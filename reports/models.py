import django
from django.db import models


class bot_ranking(models.Model):
    # Field name made lowercase..
    chat_date = models.DateField(
        db_column='chat_date', max_length=50, default=django.utils.timezone.now)
    botid = models.CharField(db_column='botid', max_length=500, default=0)
    rank1 = models.IntegerField(db_column='rank1', default=0)
    rank2 = models.IntegerField(db_column='rank2', default=0)
    rank3 = models.IntegerField(db_column='rank3', default=0)
    rank4 = models.IntegerField(db_column='rank4', default=0)
    rank5 = models.IntegerField(db_column='rank5', default=0)

    class Meta:
        verbose_name_plural = "Bot Ranking"
