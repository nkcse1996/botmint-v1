from django import forms

class GraphForm(forms.Form):
    date_text = forms.CharField(required=False)

class HrActionForm(forms.Form):
    page = forms.IntegerField(required=False)
    search_text = forms.CharField(required=False)
    records_per_page = forms.IntegerField(required=False)
    date_range = forms.CharField(required=False)


class AnalysedDataForm(forms.Form):
    date_range = forms.CharField(required=False)


class UpdateResponseForm(forms.Form):
    chat_id = forms.CharField(required=False)
    new_category_report = forms.CharField(required=False)
    question = forms.CharField(required=False)
    search_text_response = forms.CharField(required=False)
    checked_text = forms.CharField(required=False)
    checked_text_val = forms.CharField(required=False)
    question_entity_id_var = forms.CharField(required=False)
    notify_user_status = forms.CharField(required=False)


class RetrieveChatHistoryForm(forms.Form):
    obj_id = forms.CharField(required=False)