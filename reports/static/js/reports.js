    var botid = window.botId;
    var date_text;
    function showHrStatusDialog( message){
                    //$.noConflict();
                    $("#chat_alert_msg").text(message);
                    $(".ui-dialog").remove();
                    updateDialog=  $("#chat_alert_msg").dialog({
                        autoOpen: false,
                        modal:true,
                        create: function(event, ui) {
                          var widget = $(this).dialog("widget");


                          $(".ui-dialog-titlebar-close")
                            .remove();

                       }
                    });
                      $(".ui-dialog-titlebar").css('background-color','#e33044!important');
                      $(".ui-dialog-titlebar").append('<span id="msg-dialog-close-button" onclick="javascript:updateDialog.dialog(\'close\'); $(this).remove();$(\'.ui-widget-overlay.ui-front\').css(\'background\',\'#aaaaaa\');" style="color:#ffffff;float:right;font-size:20px;font-weight:bold;cursor:pointer;">×</span>');
                        $(".ui-dialog").append('<div class="ui-dialog-titlebar ui-corner-all ui-widget-header ui-helper-clearfix ui-draggable-handle" onclick="javascript:updateDialog.dialog(\'close\'); $(this).remove();" style="background-color: rgb(227, 48, 68);width: 15%;border-radius: 20%;cursor:pointer;height:30px" id="dialog_footer"></div>');
			            $("#dialog_footer").append('<span id="msg-dialog-ok-button" onclick="javascript:updateDialog.dialog(\'close\'); $(this).remove();" style="color:#ffffff;float:right;font-size:13px;font-weight:bold;cursor:pointer;">OK</span>');
                        updateDialog.dialog("open");

                        $(".ui-widget-overlay.ui-front").css("background","transparent");
			            $(".ui-dialog-title").css("color","white");
			    }

			    function showRefreshedHrStatusDialog( message){
                    //$.noConflict();
                    $("#chat_alert_msg").text(message);
                    $(".ui-dialog").remove();
                    updateDialog=  $("#chat_alert_msg").dialog({
                        autoOpen: false,
                        modal:true,
                        create: function(event, ui) {
                          var widget = $(this).dialog("widget");


                          $(".ui-dialog-titlebar-close")
                            .remove();

                       },
                       close: function( event, ui ) {
                          window.location.reload();
                       }
                    });
                      $(".ui-dialog-titlebar").css('background-color','#e33044!important');
                      $(".ui-dialog-titlebar").append('<span id="msg-dialog-close-button" onclick="javascript:updateDialog.dialog(\'close\'); $(this).remove();$(\'.ui-widget-overlay.ui-front\').css(\'background\',\'#aaaaaa\');" style="color:#ffffff;float:right;font-size:20px;font-weight:bold;cursor:pointer;">×</span>');
                        $(".ui-dialog").append('<div class="ui-dialog-titlebar ui-corner-all ui-widget-header ui-helper-clearfix ui-draggable-handle" onclick="javascript:updateDialog.dialog(\'close\'); $(this).remove();" style="background-color: rgb(227, 48, 68);width: 15%;border-radius: 20%;cursor:pointer;height:30px" id="dialog_footer"></div>');
			            $("#dialog_footer").append('<span id="msg-dialog-ok-button" onclick="javascript:updateDialog.dialog(\'close\'); $(this).remove();" style="color:#ffffff;float:right;font-size:13px;font-weight:bold;cursor:pointer;">OK</span>');
                        updateDialog.dialog("open");

                        $(".ui-widget-overlay.ui-front").css("background","transparent");
			            $(".ui-dialog-title").css("color","white");
			    }

			    function showConfirmHrStatusDialog( message){
                    //jQuery.noConflict();
                    $("#chat_alert_msg").text(message);
                    $(".ui-dialog").remove();
                    updateDialog=  $("#chat_alert_msg").dialog({
                        autoOpen: false,
                        modal:true,
                        create: function(event, ui) {
                          var widget = $(this).dialog("widgetwidget");


                          $(".ui-dialog-titlebar-close")
                            .remove();

                       }
                    });
                      $(".ui-dialog-titlebar").css('background-color','#e33044!important');
                      $(".ui-dialog-titlebar").append('<span id="msg-dialog-close-button" onclick="javascript:updateDialog.dialog(\'close\'); $(this).remove();$(\'.ui-widget-overlay.ui-front\').css(\'background\',\'#aaaaaa\');" style="color:#ffffff;float:right;font-size:20px;font-weight:bold;cursor:pointer;">×</span>');
                        $(".ui-dialog").append('<div class="ui-dialog-titlebar ui-corner-all ui-widget-header ui-helper-clearfix ui-draggable-handle" onclick="javascript:updateDialog.dialog(\'close\'); $(this).remove(); clearData();" style="background-color: rgb(227, 48, 68);width: 17%;cursor:pointer;float:left;height:30px;border-radius: 15%;" id="dialog_footer_cancel"></div>');
                        $("#dialog_footer_cancel").append('<span id="msg-dialog-ok-button" onclick="javascript:updateDialog.dialog(\'close\'); $(this).remove();" style="color:#ffffff;font-size:13px;float:right;font-weight:bold;cursor:pointer;">OK</span>');
                        $(".ui-dialog").append('<div class="ui-dialog-titlebar ui-corner-all ui-widget-header ui-helper-clearfix ui-draggable-handle" onclick="javascript:updateDialog.dialog(\'close\'); $(this).remove();" style="background-color: rgb(227, 48, 68);width: 25%;cursor:pointer;float:left;height:30px;border-radius: 10%" id="dialog_footer"></div>');
                        $("#dialog_footer").append('<span id="msg-dialog-cancel-button" onclick="javascript:updateDialog.dialog(\'close\'); $(this).remove();" style="color:#ffffff;float:right;font-size:13px;font-weight:bold;cursor:pointer;">Cancel</span>');
                        updateDialog.dialog("open");

                        $(".ui-widget-overlay.ui-front").css("background","transparent");
			            $(".ui-dialog-title").css("color","white");
			    }


                function filter_report(page)
                {
                if(page==undefined){
                    page='';
                }
                var search_text =  $("#search_text").val();
                var date_range = $('#date_text').val();

                 if((search_text.length)>=3 || page != ''){


                 $.ajax({
                        type: 'GET',
                        dataType: 'html',
                        url: "/reports/"+botid+"/hr_action/",
                        data: {'records_per_page':$('#records_per_page').val(),
                                'search_text':search_text,
                                'date_range': date_range,
                                'page': page,
                                },
                        success: function(data){
                        //check if session is timed out or not
                            if(data.indexOf('bot_error') !=-1)
                            {
                                var result=JSON.parse(data);
                                showRefreshedHrStatusDialog(result['bot_error']);
                                //window.location.replace('');
                            }
                            else{
                            <!--$('#top_questions').hide();-->
                            $('#search_data').show();
                            $('#show_report').show();
                            $('#show_report').html(data);
                            }
                        }});
                 }
             }



             function reports_display(page)
                {
                if(page==undefined){
                    page='';
                }
                if (date_range == "" || date_range == undefined){
                    var date_range = $('#date_text').val();}

                 $.ajax({
                        type: 'GET',
                        dataType: 'html',
                        url: "/reports/"+botid+"/hr_action/",
                        data: {'records_per_page':$('#records_per_page').val(),
                                'date_range': date_range,
                                'page': page,
                                },
                        success: function(data){
                            //check if session is timed out or not
                            if(data.indexOf('bot_error') !=-1)
                            {
                                var result=JSON.parse(data);
                                showRefreshedHrStatusDialog(result['bot_error']);
                                //window.location.replace('');
                            }
                            else{
                            $('#printExportRecord').prop('disabled', false);
                            <!--$('#top_questions').hide();-->
                            <!--$('#search_data').show();-->
                            $('#show_report').show();
                            $('#show_report').html(data);
                            //alert();
                            //$('a[data-action=collapse]').click();
                            if ($('.widget-body').css('display') == 'none')
                            {
                            $('#open_report').click();
                            }

                            $(document).scrollTop(100)
                            goToByScroll('report_section');
                            }
                        }});


             }

             <!--here button clear history-->
             $("#chat_history_clear").click(function(){
             alert("hi");
                showConfirmHrStatusDialog("Do you really want to delete from database")

			});
			function clearData(){
                $.ajax({
                    type: 'GET',
                    url: "/learning/"+botid+"/clear_chat_db/",
                    success: function(data){
                        //check if session is timed out or not
                        if(data['bot_error']){
                            showRefreshedHrStatusDialog(data['bot_error']);
                            //window.location.replace('');
                        }
                        else{
                        data=JSON.parse(data)
                        showRefreshedHrStatusDialog(data.message);
                        }
                    }
			    });
			}

            function filter_report_ref(page)
                {
                    if(page==undefined){
                        page='';
                    }
                     var date_range = $('#date_text').val();
                     $.ajax({
                        type: 'GET',
                        dataType: 'html',
                        url: "/reports/"+botid+"/hr_action/",
                        data: {'records_per_page':$('#records_per_page').val(),
                                'search_text':'',
                                'page': 1,
                                'date_range': date_range,
                                },
                        success: function(data){
                        //check if session is timed out or not
                            if(data.indexOf('bot_error') !=-1)
                            {
                                var result=JSON.parse(data);
                                showRefreshedHrStatusDialog(result['bot_error']);
                                //window.location.replace('');
                            }
                            else{
                            $('#search_text').val(''),
                            //$('#top_questions').hide();
                            $('#search_data').show();
                            $('#show_report').show();
                            $('#show_report').html(data);
                            }
                        }});
                 }


                function goToByScroll(id){
                                            // Remove "link" from the ID
                                            id = id.replace("link", "");
                                            // Scroll
                                            $('html,body').animate({
                                            scrollTop: $("#"+id).offset().top},'slow');
                                          }

            function printExportRecord()
                                {
                                    hostname = '/reports/'+botid+'/export_csv/';
                                    window.open(hostname, '_blank');
                                }

            function top_questions_display(){
            var date_range = $('#date_text').val();
            $.ajax({
                        type: 'GET',
                        url: "/reports/"+botid+"/top_questions/",
                        data:{"date_range":date_range},
                        success: function(data){
                            //$('#search_data').hide();-->
                            //$('#show_report').hide();-->
                            $('#top_questions').show();
                            $('#top_questions').html(data);
                            //<!--if ($('.widget-body').css('display') == 'none')-->
                            //<!--{-->
                            //<!--$('#open_report').click();-->
                            //<!--}-->
                        }
                     });
           }
           function load_histogram(){
            var date_text = $('#date_text').val();
                    $.ajax({
                            url: "/reports/"+botid+"/histogram/",
                            data: {date_text: date_text},
                            type:'GET',
                            dataType:'json',
                            success: function(result){
                            console.log(result)
                            var myNewChart = '';
                            $('#Chart').remove();
                            var histogram = document.getElementById("histogram").getContext("2d");
                            var x = document.getElementById('histogramGraph');
                            if (x.style.display === 'none') {
                                x.style.display = 'block';
                                }
                            //<!-- No data found -->
                            if(result['msg']=='No Records Found'){
                                alert(result['msg']);
                                }

                            //<!-- Generating Histogram  -->
                            if(result)
                            {

                            // Get the context of the canvas element we want to select
                            var dates = result['Start_date']+' to '+result['End_date']
                            // Instantiate a new chart using 'data' (defined below)
                            var data1 = {
                            labels: [dates],
                            datasets: [
                            {
                                label: "Rank1",
                                backgroundColor: "rgb(102, 255, 153)",

                                data: [result['Rank1']]
                            },
                            {
                                label: "Rank2",
                                backgroundColor: "rgb(255, 224, 102)",
                                strokeColor: "rgba(151,187,205,1)",
                                pointColor: "rgba(151,187,205,1)",
                                pointStrokeColor: "#fff",
                                pointHighlightFill: "#fff",
                                pointHighlightStroke: "rgba(151,187,205,1)",
                                data: [result['Rank2']]
                            },
                            {
                                label: "Rank3",
                                backgroundColor: "rgb(163, 194, 194)",
                                strokeColor: "rgba(151,187,205,1)",
                                pointColor: "rgba(151,187,205,1)",
                                pointStrokeColor: "#fff",
                                pointHighlightFill: "#fff",
                                pointHighlightStroke: "rgba(151,187,205,1)",
                                data: [result['Rank3']]
                            },
                            {
                                label: "Rank4",
                                backgroundColor: "rgb(195, 195, 136)",
                                strokeColor: "rgba(151,187,205,1)",
                                pointColor: "rgba(151,187,205,1)",
                                pointStrokeColor: "#fff",
                                pointHighlightFill: "#fff",
                                pointHighlightStroke: "rgba(151,187,205,1)",
                                data: [result['Rank4']]
                            },
                            {
                                label: "Rank5",
                                backgroundColor: 'rgba(251, 85, 85, 0.4)',
                                strokeColor: "rgba(151,187,205,1)",
                                pointColor: "rgba(151,187,205,1)",
                                pointStrokeColor: "#fff",
                                pointHighlightFill: "#fff",
                                pointHighlightStroke: "rgba(151,187,205,1)",
                                data: [result['Rank5']]
                            }
                            ]

                            };

                            var options = {
                                responsive: true,
                                title: {
                                    display: true,
                                    position: "top",
                                    text: "Bar Graph",
                                    fontSize: 18,
                                    fontColor: "#111"
                                },
                                legend: {
                                    display: true,
                                    position: "top",

                                }
                            };


                            var myNewChart = '';
                            myNewChart = new Chart(histogram , {
                                            type: "bar",
                                            data: data1,
                                            datasetFill: false,
                                            options:options,

                            });
                            }
                            }

                    });
				}
				function load_line_graph(){
				var date_text = $('#date_text').val();
                $.ajax({
                        url: "/reports/"+botid+"/line_graph/",
                        data: {date_text: date_text},
                        type:'GET',
                        dataType:'json',
                        success: function(result){
                        console.log(result)
                        var myNewChart = '';
                        $('#Chart').remove();
                        var line_graph = document.getElementById("line_graph").getContext("2d");
                        var x = document.getElementById('divGraph');
                        if (x.style.display === 'none') {
                            x.style.display = 'block';
                        }

                        <!-- No data found -->
                        if(result['msg']=='No Records Found'){
                        alert(result['msg']);
                        }

                        <!-- Generating Line graph-->
                        if(result)
                        {
                        // Instantiate a new chart using 'data' (defined below)
                        var data = {
                        labels: result['Dates'],
                        datasets: [
                          {
                            label: "Rank1",
                            backgroundColor: "rgb(0, 230, 77)",
                            borderColor: "rgb(102, 255, 153)",
                            fill: false,
                            lineTension: 0,
                            radius: 5,
                            data: result['Rank1']
                          },
                          {
                            label: "Rank2",
                            backgroundColor: "rgb(230, 184, 0)",
                            borderColor: "rgb(255, 224, 102)",
                            fill: false,
                            lineTension: 0,
                            radius: 5,
                            data: result['Rank2']
                          },
                          {
                            label: "Rank3",
                            backgroundColor: "rgb(92, 138, 138)",
                            borderColor: "rgb(163, 194, 194)",
                            fill: false,
                            lineTension: 0,
                            radius: 5,
                            data: result['Rank3']
                          },
                          {
                            label: "Rank4",
                            backgroundColor: "rgb(153, 153, 77)",
                            borderColor: "rgb(195, 195, 136",
                            fill: false,
                            lineTension: 0,
                            radius: 5,
                            data: result['Rank4']
                          },
                          {
                            label: "Rank5",
                            backgroundColor: "rgb(249, 6, 6)",
                            borderColor: "rgba(251, 85, 85, 0.4)",
                            fill: false,
                            lineTension: 0,
                            radius: 5,
                            data: result['Rank5']
                          }
                        ]
                        };

                        var options = {
                                responsive: true,
                                multiTooltipTemplate: "<%= datasetLabel %>" ,
                                title: {
                                    display: true,
                                    position: "top",
                                    text: "Line Graph",
                                    fontSize: 18,
                                    fontColor: "#111"
                                },
                                legend: {
                                    display: true,
                                    position: "top",

                                }
                            };
                            myNewChart = new Chart(line_graph , {
                                                type: "line",
                                                data: data,
                                                datasetFill: false,
                                                options:options,

                            });
                }
                }});

				}

         $(document).ready(function(){
                date_text = $('#date_text').val();
                //to translate the daterange picker, please copy the "examples/daterange-fr.js'  %}" contents here before initialization
				$('input[name=date-range-picker]').daterangepicker({
					'applyClass' : 'btn-sm btn-success',
					'cancelClass' : 'btn-sm btn-default',
					'startDate' : moment().add(-7, 'day'),
					'maxDate' : new Date(),
					<!--'startDate' : new Date(today.getTime() - (7 * 24 * 60 * 60 * 1000)),-->
					<!--'endDate' : new Date(),-->
					locale: {
						applyLabel: 'Apply',
						cancelLabel: 'Cancel',
					}
				})
				.prev().on(ace.click_event, function(){
					$(this).next().focus();
				});

              reports_display(undefined);
              top_questions_display();
              load_histogram();
              load_line_graph();
         });