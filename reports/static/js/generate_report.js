var botid = window.botId;
$(document).ready(function() {

//    // column in chat history table needed to be hidden initially
    $('.user_id_column').hide();
    $('#user_id').hide();
    $('.username_column').hide();
    $('#user_name').hide();
    $('.status_column').hide();
    $('#status').hide();
    $('.feedback_type').hide();
    $('#feedback_type').hide();
    $('.session_id_column').hide()
    $('#session_id').hide();
    $('.email_id_column').hide();
    $('#email_id').hide()

	$.ajax({
			type: 'POST',
			url: "/api/"+botid+"/get_api_list/",
			success: function(data){
			for (var key in data){
			var id = data[key]['id'];
			var name = data[key]['api_name'];
			$("#api_connect_report").append('<option value="'+ id + '">' + name + '</option>');
			}
			}});
  	});


  	 function filter_chat_histoy_table(){
     var column_selected = $('#add_columns').val();
     if (parseInt(column_selected) == 1){
        document.getElementById("user_name").style.display = "block";
        $('.username_column').css('display','block');
     }
     else if (parseInt(column_selected) == 2){
        document.getElementById("status").style.display = "block";
        $('.status_column').css('display','block');
     }
     else if (parseInt(column_selected) == 3){
        document.getElementById("feedback_type").style.display = "block";
        $('.feedback_type').css('display','block');
     }
     else if (parseInt(column_selected) == 4){
        document.getElementById("user_id").style.display = "block";
        $('.user_id_column').css('display','block');
     }
      else if (parseInt(column_selected) == 5){
        document.getElementById("session_id").style.display = "block";
        $('.session_id_column').css('display','block');
     }
      else if (parseInt(column_selected) == 6){
        document.getElementById("email_id").style.display = "block";
        $('.email_id_column').css('display','block');
     }
 }

 function add_entity_report(obj_id)
                  {
                    $.ajax({
                        type: 'GET',
                        dataType: 'json',
                        url: "/reports/"+botid+"/retrieve_chat_history/",
                        data: {'obj_id':obj_id,},
                        success: function(data){
						var Question = data.Question;
						var Answer = data.Answer;
						var Id = data.Id;
                        $('#question').val(Question);
                        $('#question_objid_chat').val(Id);
                        }
                     });
                  }

<!--START:Conversation-->
            var modal_conv = document.getElementById("ReportConversationalModal");
            function openConverseModal(){
            document.getElementById("conversation_update_msg").innerHTML = "";
            modal_conv.style.display = "block";
            }
    var conv;
    function add_conversation(data){
                conv=data;
                if(conv!=''||conv!=null && conv.length > 0)
                    conversation_build(conv);
        }

    function conversation_addStatement(id) {
        var newId = id;
        var classIdentifier = 12;
        $statement = $('<div id="' + newId + '" class="col-md-' + classIdentifier + '" style="background-color: rgb(255, 255, 255); \
                            box-shadow: rgba(0, 0, 0, 0.25) 0px 0px 5px 2px; padding: 0px; margin: 0px;">\
                                <div>\
                                    <label style="float:left">From Entellio to User:</label> \
                                </div>\
                                <div class="form-group" id="' + newId + '_formToggle"> \
                                    <input type="text" class="form-control" id="' + newId + '_statement" placeholder="From Entellio to User" readonly> \
                                </div>\
                        </div>');

        $statement.appendTo("#conversation_path_formToggle");
    }

    function conversation_addDecision(id) {
        var newId = id;
        var classIdentifier = 12;
        $decision = $('<div id="' + newId + '" class="col-md-' + classIdentifier + '" style="background-color: rgb(255, 255, 255); \
                            box-shadow: rgba(0, 0, 0, 0.25) 0px 0px 5px 2px; padding: 0px; margin: 0px;">\
                                <div  id="' + newId + '_formContrl" class="form-group"> \
                                    <div class="col-md-12" id="'+ newId + '-1" style="margin:0px;padding:2px;box-shadow: rgba(0, 0, 0, 0.25) 0px 0px 5px 2px;">\
                                        <div>\
                                            <label>Input from User:</label>\
                                        </div>\
                                        <div class="form-group" id="' + newId + '-1_formToggle"> \
                                          <input type="text" id="' + newId + '_desLabel" placeholder="Input from User" style="width:500px;" readonly></input>\
                                        </div> \
                                    </div>\
                                </div>\
                        </div>');


        $decision.appendTo("#conversation_path_formToggle");
    }

    function conversation_build(currentNode) {
        var currentNodeLength = currentNode.length;
        for(var i=0; i<currentNodeLength; i++) {
            var user = currentNode[i]['Question'];
            var bot = currentNode[i]['Answer'];
            if(user!='') {
               conversation_addDecision("conversation_path_"+parseInt(i+1)+"_user", false);
               document.getElementById("conversation_path_" + parseInt(i+1) + "_user_desLabel").value = user;
            } if(bot!='') {
                conversation_addStatement("conversation_path_"+parseInt(i+1)+"_bot", false);
                document.getElementById("conversation_path_" + parseInt(i+1) + "_bot_statement").value = bot;
            }
        }
    }

    function conversation_close(){
                    for(var i=1; i<=conv.length+1; i++) {
                    if($("#conversation_path_"+i+"_bot"))
                        $("#conversation_path_"+i+"_bot").remove();
                    if($("#conversation_path_"+i+"_user"))
                        $("#conversation_path_"+i+"_user").remove();
                }
        modal_conv.style.display = "none";
    }
<!--END:Conversation-->

function filter_report_map()
{
	$("#livesearch").hide();
	$("#new_question").hide();

	var search_text = document.getElementById("search_text_response").value;
	if((search_text.length)>=3)
		{
		var search_text = document.getElementById("search_text_response").value;
		$.ajax({
				type: 'GET',
				dataType: 'html',
				url: "/bot/"+botid+"/retrieve_match/",
				data: {'search_text':search_text},
				success: function(data){
						var json = $.parseJSON(data);
						var output = '<div class="search_item_css" id="New" onclick="selected_entity(this.id)">#New</div>';
						$.each(json['search_list'], function(index, element)
						{
							output+='<div class="search_item_css" id="'+element['obj_id']+'" onclick="selected_entity(this.id)">"'+element['obj_data']+'"</div>';
						});
						document.getElementById("livesearch").innerHTML=output;
						$("#livesearch").show();
					}
				});
		}
}
function selected_entity(id)
{
    var selected_val = $("#"+id).text()
    var selected_id = id;
    $("#question_entity_id").val(selected_id)
    if (selected_id == "New" )
    {
      $("#new_question").show();
      $("#livesearch").hide();
    }
    $("#livesearch").hide();
    $('#search_text_response').val(selected_val);
}

function updateResponse()
{
    var chat_id = document.getElementById("question_objid_chat").value;
    var question = document.getElementById("question").value;
    var search_text_response = document.getElementById("search_text_response").value;
    var question_entity_id_var = document.getElementById("question_entity_id").value;

    var new_category_report = document.getElementById("new_category_report").value;
    var checked_text = "";
    var checked_text_val = "";

    var new_answer_rad = document.getElementById("new_answer").checked;
    var api_connect_report_rad = document.getElementById("api_connect_report_check").checked;
    var cmd_connect_report_rad = document.getElementById("cmd_connect_report_check").checked;
    var web_connect_report_rad = document.getElementById("web_connect_report_check").checked;

    //Start: Ensuring at least one radio button is checked
    if (search_text_response == '#New')
    {
        if (api_connect_report_rad == false && cmd_connect_report_rad == false && web_connect_report_rad == false
            && new_answer_rad == false)
            {
            alert('Please select at least one of the option.');
            return;
            }
    }
    //End: Ensuring at least one radio button is checked
    if (api_connect_report_rad == true)
    {
        checked_text_val = document.getElementById("api_connect_report").value;
        checked_text = "api_connect";
        $('#search_text_response').val("");
        $('#web_connect_report').val("");
        $('#new_answer_report').val("");

    }
    if (cmd_connect_report_rad == true)
    {
        checked_text_val = document.getElementById("cmd_connect_report").value;
        checked_text = "cmd_connect";
        $('#api_connect_report').val("");
        $('#web_connect_report').val("");
        $('#new_answer_report').val("");
    }
    if (web_connect_report_rad == true)
    {
        checked_text_val = document.getElementById("web_connect_report").value;
        checked_text = "web_connect";
        $('#api_connect_report').val("");
        $('#cmd_connect_report').val("");
        $('#new_answer_report').val("");
    }
    if (new_answer_rad == true)
    {
         checked_text_val = document.getElementById("new_answer_report").value;
         checked_text = "new_answer";
         $('#api_connect_report').val("");
         $('#cmd_connect_report').val("");
         $('#web_connect_report').val("");
    }

    var notify_user_status = document.getElementById("notify_user").checked;

    $.ajax({
				type: 'POST',
				dataType: 'html',
				url: "/reports/"+botid+"/update_response/",
				data: {'chat_id':chat_id,'question':question,
				        'search_text_response':search_text_response,
				        'checked_text':checked_text,
				        'checked_text_val':checked_text_val,
				        'new_category_report':new_category_report,
				        'notify_user_status':notify_user_status,
				        'question_entity_id_var':question_entity_id_var},
				success: function(result){
				    alert(result)
				    $('#api_connect_report').val("");
                    $('#cmd_connect_report').val("");
                    $('#web_connect_report').val("");
                    $('#new_answer_report').val("");
                    $('#new_category_report').val("");
                    $("#histModal").modal('toggle');
					}
				});
}