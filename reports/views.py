import configparser
import csv
import datetime as dt
import json
import os
import time
from datetime import datetime, timedelta
from functools import wraps

from django.conf import settings
from django.contrib.auth.decorators import login_required
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from django.http import HttpResponse
from django.shortcuts import render
from django.utils.encoding import smart_str
from mongoengine import Q
from django.views.decorators.csrf import csrf_exempt
from fwk.settings import db

from bot_registration.models import bot_details
from chat.models import ChatHistory
from reports.forms import GraphForm
from reports.forms import HrActionForm, AnalysedDataForm, UpdateResponseForm, RetrieveChatHistoryForm
from reports.models import bot_ranking

from learning.views import ques_IE_extractor
from learning.models import Questionnaire, POS

admin_config = configparser.ConfigParser()
admin_config.read(os.path.join(settings.BASE_DIR, 'bot_config', 'config', 'config.ini'))
pjoin = os.path.join

def pre_reports_check(view_func):
    def decorator(request, *args, **kwargs):
        user_login = request.user
        if all(map(lambda v: v in [i.name.lower() for i in user_login.groups.all()],
                   ['report', kwargs['botid'].lower()])):
            pass
        else:
            bot_detail = bot_details.objects.filter(
                bot_apikey=kwargs['botid'])[0]
            bot_theme = 'chat.html'
            if bot_detail.bot_theme != None:
                bot_theme = bot_detail.bot_theme
            return render(request, bot_theme,
                          {'botid': kwargs['botid'], 'bot_name': bot_detail.botname, 'bot_username': 'User name', })
        return view_func(request, *args, **kwargs)
    return wraps(view_func)(decorator)


@login_required(login_url='login/')
@pre_reports_check
# @pre_authorisation_check
def hr_report(request,botid = None):
    print("reached report")
    if not botid : botid = request.session['bot_details']['botid']
    bot_detail = bot_details.objects.filter(bot_apikey=botid)[0]
    return render(request, 'reports.html' , {'botid': botid,'bot_name':bot_detail.botname,'bot_username':bot_detail.bot_username} )


@login_required(login_url='login/')
# @pre_authorisation_check
@pre_reports_check
def hr_action(request,botid = None):
    form = HrActionForm(request.GET)
    if form.is_valid():
        page = form.cleaned_data['page']
        search_text = form.cleaned_data['search_text']
        records_per_page = form.cleaned_data['records_per_page']
        if not botid : botid = request.session['bot_details']['botid']
        start_end_date = [i.strip() for i in (form.cleaned_data['date_range']).split('-')]
        startDate_fm = datetime.strptime(start_end_date[0], '%m/%d/%Y')
        endDate_fm = datetime.strptime(start_end_date[1], '%m/%d/%Y')
        endDate_fm = endDate_fm + timedelta(hours=23, minutes=59, seconds=59)

        if records_per_page != None and records_per_page != '':
            processed_data = process_request(request, startDate_fm, endDate_fm, botid)
            if processed_data :
                analysed_data = analysed_data_fn(request ,processed_data)
                bot_list = analysed_data['bot_list']
                try:
                    for i in bot_list:
                        obj = bot_ranking.objects.update_or_create(
                            botid=i['botid'],
                            chat_date=i['date'],
                            defaults = {
                                'rank1': i['Rank1'],
                                'rank2': i['Rank2'],
                                'rank3': i['Rank3'],
                                'rank4': i['Rank4'],
                                'rank5': i['Rank5'],
                            }
                        )
                except Exception as e:
                    print(str(e))
                    pass


            return render(request, 'report_hr.html', {'result_list': listing(request,
                                                                        botid=botid,startDate=startDate_fm,
                                                                        endDate=endDate_fm,page=page,
                                                                        search_text=search_text,
                                                                        records_per_page=records_per_page),
                                                        'bot_list': bot_list,
                                                        'chat_history_data': 'show',
                                                        })
        else:
            return render(request, 'report_hr.html', {'result_list': '', 'bot_list': ''})
    else:
        d = json.dumps({'error': 'Failed to validate form'})
        return HttpResponse(d)



@login_required(login_url='login/')
def process_request(request,startDate=None,endDate=None,botid=None):
    if not botid : botid = request.session['bot_details']['botid']
    dData = ChatHistory.objects((Q(botid=botid) & Q(datetime__gte=startDate)) & Q(datetime__lte=endDate))
    try:
        if dData :
            final_list = [[i['botid'],  i['chats'][0]['request'],  i['chats'][0]['response'],
                      i['rank'].capitalize(),  i['datetime'].strftime('%m/%d/%Y')] for i in dData]
            return sorted(final_list)
        else:
            return []
    except:
        return []

@login_required(login_url='login/')
def analysed_data_fn(request,data = None):
    try:
        result_dict= {}
        error_list , bot_list = [],[]
        bot_r1,bot_r2,bot_r3,bot_r4,bot_r5 = 0,0,0,0,0
        form = AnalysedDataForm(request.GET)
        if form.is_valid():
            start_end_date = [i.strip() for i in (form.cleaned_data['date_range']).split('-')]
            startDate = datetime.strptime(start_end_date[0], '%m/%d/%Y')
            endDate = datetime.strptime(start_end_date[1], '%m/%d/%Y')
            request.session['date_range'] = form.cleaned_data['date_range']

            for n in range(int((endDate - startDate).days) + 1):
                try:
                    for i in data:
                        botid = i[0]
                        str_date=i[4]
                        obj_date = datetime.strptime(str_date, '%m/%d/%Y')
                        if obj_date == startDate + dt.timedelta(n):
                            if i[3] == '1':    bot_r1 += 1
                            if i[3] == '2':    bot_r2 += 1
                            if i[3] == '3':    bot_r3 += 1
                            if i[3] == '4':    bot_r4 += 1
                            if i[3] == '5':    bot_r5 += 1
                    bot_list.append({'Rank1': bot_r1,'Rank2': bot_r2,'Rank3': bot_r3,'Rank4': bot_r4,
                                         'Rank5': bot_r5, 'date': startDate + dt.timedelta(n),'botid':botid})
                except Exception as e:
                    error_list.append(e)

            result = {'bot_list': bot_list}
        else:
            result = {'bot_list': {}}
        return result
    except Exception:
        result = {'bot_list': {}}
        return result

@login_required(login_url='login/')
def export_csv(request,botid=None):
    try:
        data_list = []
        if not botid : botid = request.session['bot_details']['botid']
        date_range = request.session['date_range']
        start_end_date = [i.strip() for i in (date_range).split('-')]
        startDate_fm = datetime.strptime(start_end_date[0], '%m/%d/%Y')
        endDate_fm = datetime.strptime(start_end_date[1], '%m/%d/%Y')
        endDate_fm = endDate_fm + timedelta(hours=23, minutes=59, seconds=59)
        tot_data = ChatHistory.objects((Q(botid=botid) & Q(datetime__gte=startDate_fm)) & Q(datetime__lte=endDate_fm))
        response = HttpResponse(content_type='text/csv')
        # response['Content-Disposition'] = 'attachment ; filename="a.csv"'
        response['Content-Disposition'] = 'attachment ; filename={}.csv'.format(botid + time.strftime("__%x"))
        writer = csv.writer(response)
        writer.writerow([smart_str(u"User Id"),smart_str(u"Bot Id"),smart_str(u"Request"),smart_str(u"Response"),
                         smart_str(u"Rank"),smart_str(u"DateTime")])

        for i in tot_data:
            chat_date = '%s-%s-%s,%s:%s:%s' % (i.datetime.year, i.datetime.month,i.datetime.day,i.datetime.hour,i.datetime.minute,i.datetime.second)
            sub_list=[i.username,i.botid,i.chats[0]['request'],i.chats[0]['response'],i.rank,chat_date]
            data_list.append(sub_list)

        for i in data_list:
            writer.writerow([i[0],i[1],i[2],i[3],i[4],i[5]])
        print(response)
        return response
    except:
        pass
    #return response

@login_required(login_url='login/')
def listing(request,page=1,search_text ='',records_per_page=10,botid=None,startDate=None,endDate=None):
    if not botid : botid = request.session['bot_details']['botid']
    if page == '' or page == None:  page = 1
    dictlist,chat_list,final_list = [],[],[]
    user_list = ChatHistory.objects(Q(botid=botid) & Q(datetime__gte=startDate) & Q(datetime__lte=endDate)).order_by("-id")
    if search_text != None:
        for i in user_list:
            if i['feedback_msg'] != None:
                sub_list = [i['botid'], i['session_id'], i['user_id'], i['username'], i['emailid'], i['chats'][0]['request'],
                            i['chats'][0]['response'], i['rank'], i['datetime'].strftime('%m/%d/%Y'),
                            i['feedback_type'], i['feedback_msg']]
            else:
                sub_list = [i['botid'], i['session_id'], i['user_id'], i['username'], i['emailid'], i['chats'][0]['request'],
                            i['chats'][0]['response'], i['rank'], i['datetime'].strftime('%m/%d/%Y'),
                            i['feedback_type']]
            if search_text.lower() in ''.join(sub_list).lower():
                dictlist.append(i)
    else:
        for i in user_list: dictlist.append(i)
    paginator = Paginator(dictlist, records_per_page)
    try:
        users = paginator.page(page)
    except PageNotAnInteger:
        users = paginator.page(1)
    except EmptyPage:
        users = paginator.page(paginator.num_pages)
    return users

def line_graph(request, botid = None):
    data, startDate ,endDate = date_picker(request, botid)
    if len(data) == 0 : return HttpResponse(json.dumps({'msg':'No Records Found'}))
    else :
        line_graph_data, hist_data = graph_listing(data, startDate, endDate)
        return HttpResponse(json.dumps(line_graph_data))

def histogram(request, botid = None):
    data, startDate, endDate = date_picker(request, botid)
    if len(data) == 0 : return HttpResponse(json.dumps({'msg':'No Records Found'}))
    else :
        line_graph_data, hist_data = graph_listing(data, startDate, endDate)
        return HttpResponse(json.dumps(hist_data))

def graph_listing(data, startDate, endDate):
    Rank1, Rank2, Rank3, Rank4, Rank5, Dates = [], [], [], [], [], []
    for d in data:
        Rank1.append(d.rank1)
        Rank2.append(d.rank2)
        Rank3.append(d.rank3)
        Rank4.append(d.rank4)
        Rank5.append(d.rank5)

        date_fm = '%s-%s-%s' % (d.chat_date.year, d.chat_date.month, d.chat_date.day)
        Dates.append(date_fm)

    line_graph_data = {'Rank1': Rank1, 'Rank2': Rank2, 'Rank3': Rank3, 'Rank4': Rank4, 'Rank5': Rank5, 'Dates': Dates}
    hist_data = {'Rank1': sum(Rank1), 'Rank2': sum(Rank2), 'Rank3': sum(Rank3), 'Rank4': sum(Rank4),
                 'Rank5': sum(Rank5), 'Start_date': startDate, 'End_date': endDate}

    return line_graph_data, hist_data


def date_picker(request, botid = None):

    if not botid : botid = request.session['bot_details']['botid']
    form = GraphForm(request.GET)
    if form.is_valid():
        start_end_date = [i.strip() for i in (form.cleaned_data['date_text']).split('-')]
        startDate_fm = datetime.strptime(start_end_date[0], '%m/%d/%Y')
        endDate_fm = datetime.strptime(start_end_date[1], '%m/%d/%Y')
        startDate = '%s-%s-%s' % (startDate_fm.year, startDate_fm.month, startDate_fm.day)
        endDate = '%s-%s-%s' % (endDate_fm.year, endDate_fm.month, endDate_fm.day)
        data = bot_ranking.objects.filter(botid=botid, chat_date__range=(startDate, endDate)).order_by('chat_date')
        return data, startDate, endDate


def question_insert_helper(botid, pos, new_question, search_text_response, checked_text, checked_text_val, question_entity_id_var, category):
    print("BotID:" + botid
          + "  Question ID for update::  " + question_entity_id_var
          + "  New Question to update::  " + new_question + "  Entities to map with::  " + search_text_response +
          "checked_text::  " + checked_text + "checked_text_val  " + checked_text_val + "--------")
    return_val = False
    # For inserting new question
    try:
        botdomain = bot_details.objects.filter(bot_apikey=botid)[0].bot_domain
    except:
        botdomain = ""
    if search_text_response != "" and search_text_response == "#New":
        try:
            ques = ""
            if checked_text == "api_connect":
                ques = Questionnaire(domain=botdomain, category=category, answer="", questions=[new_question], bot_id=botid,
                                     created_date=datetime.now(), last_updated_by="user", questions_pos=pos, api_connect=['True', checked_text_val],
                                     cmd_connect=['False', ''], web_connect=['False', ''], is_conversational=['False', ''], conversation_id='None')
            elif checked_text == "cmd_connect":
                ques = Questionnaire(domain=botdomain, category=category, answer="", questions=[new_question], bot_id=botid,
                                     created_date=datetime.now(), last_updated_by="user", questions_pos=pos, api_connect=['False', ''],
                                     cmd_connect=['True', checked_text_val], web_connect=['False', ''], is_conversational=['False', ''], conversation_id='None')
            elif checked_text == "web_connect":
                ques = Questionnaire(domain=botdomain, category=category, answer="", questions=[new_question], bot_id=botid,
                                     created_date=datetime.now(), last_updated_by="user", questions_pos=pos, api_connect=['False', ''],
                                     cmd_connect=['False', ''], web_connect=['True', checked_text_val], is_conversational=['False', ''], conversation_id='None')
            elif checked_text == "new_answer":
                ques = Questionnaire(domain=botdomain, category=category, answer=checked_text_val, questions=[new_question], bot_id=botid,
                                     created_date=datetime.now(), last_updated_by="user", questions_pos=pos, api_connect=['False', ''],
                                     cmd_connect=['False', ''], web_connect=['False', ''], is_conversational=['False', ''], conversation_id='None')

            ques.save()
            return_val = True

            # if checked_text == 'api_connect':
            #     insert_question_in_db(int(checked_text_val), new_question, str(ques.id))

        except:
            return_val = False
    # For updating question
    elif search_text_response != "" and search_text_response != "#New":
        try:
            question_list = []
            # ques_obj = Questionnaire.objects.filter(bot_id=botid,id=question_entity_id_var)
            ques_obj = Questionnaire.objects.get(id=question_entity_id_var)

            # Fetch POS object details of existing question when new question
            # to be mapped
            entities_ext = ques_obj.questions_pos.entities
            intents_ext = ques_obj.questions_pos.intents
            identifier_ext = ques_obj.questions_pos.identifier
            ablative_ext = ques_obj.questions_pos.ablative

            # Finding intent and entity detail for new question
            entities_new, intents_new, ablative_new, identifier_new = ques_IE_extractor(
                question=new_question)

            # Adding/updating existing intent and entity
            entities_new.extend(entities_ext)
            identifier_new.extend(identifier_ext)
            ablative_new.extend(ablative_ext)
            intents_new.update(intents_ext)

            # Creating new POS object
            pos_obj = POS(entities=set(entities_new), intents=intents_new, identifier=set(
                identifier_new), ablative=set(ablative_new))

            # Updating existing question with above details
            question_list.extend(ques_obj.questions)
            question_list.append(str(new_question.strip().lower()))
            if len(question_list) != 0:
                # ques_obj.update(questions=question_list)
                ques_obj.questions = question_list
                ques_obj.questions_pos = pos_obj
                ques_obj.save()
                return_val = True
            else:
                return_val = False
        except:
            return_val = False

    return return_val


@csrf_exempt
def updateResponse(request, botid=None):
    if not botid:
        botid = request.session['bot_details']['botid']
    if request.POST:
        try:
            form = UpdateResponseForm(request.POST)
            if form.is_valid():
                return_msg = ""
                chat_id = form.cleaned_data['chat_id']
                category = form.cleaned_data['new_category_report']
                question = form.cleaned_data['question']
                search_text_response = form.cleaned_data[
                    'search_text_response']
                checked_text = form.cleaned_data['checked_text']
                checked_text_val = form.cleaned_data['checked_text_val']
                question_entity_id_var = form.cleaned_data[
                    'question_entity_id_var']
                notify_user_status = form.cleaned_data['notify_user_status']

                if notify_user_status == 'true':
                    email_status = notify_user(chat_id)
                    print(email_status)
                # Extracting intents and entiry and POS tagging
                nouns_ext, verbs_ext, adj_ext, pronoun_ext = ques_IE_extractor(
                    question=question)
                pos = POS(entities=nouns_ext, intents=verbs_ext,
                          identifier=adj_ext, ablative=pronoun_ext)

                # Question insertion and updation
                insert_update_status = question_insert_helper(
                    botid, pos, question, search_text_response, checked_text, checked_text_val, question_entity_id_var, category)
                if insert_update_status == True:
                    # Updating chat history status
                    chat_obj = ChatHistory.objects.filter(id=chat_id)
                    if len(chat_obj) == 1:
                        chat_obj.update(status="Added")
                    return_msg = "Question inserted/updated successfully."
                else:
                    return_msg = "Unable to inserted/updated question.Please try again!"
            else:
                return_msg = "Unable to inserted/updated question.Please try again!"
        except Exception as e:
            print(str(e))
            return_msg = "Unable to inserted/updated question.Please try again!"
    return HttpResponse(return_msg)

def retrieve_chat_history(request, botid=None):
    form = RetrieveChatHistoryForm(request.GET)
    if form.is_valid():
        id = form.cleaned_data['obj_id']
        ques_list = ChatHistory.objects.get(id=id)
        Question = ques_list.request
        Answer = ques_list.response
        entity_data = {'Question': Question, 'Answer': Answer, 'Id': id}
        d = json.dumps(entity_data)
        return HttpResponse(d)
    else:
        return HttpResponse(0)


@csrf_exempt
def top_questions(request, botid=None):
    form = HrActionForm(request.GET)
    if form.is_valid():
        start_end_date = [i.strip()for i in (form.cleaned_data['date_range']).split('-')]
        startDate_fm = datetime.strptime(start_end_date[0], '%m/%d/%Y')
        endDate_fm = datetime.strptime(start_end_date[1], '%m/%d/%Y')
        endDate_fm = endDate_fm + timedelta(hours=23, minutes=59, seconds=59)
        db_cursor = db.chat_history.aggregate(
            [
                {"$match": {"datetime": {"$gte": startDate_fm, "$lte": endDate_fm}}},
                {"$group": {"_id": {"botid": "$botid", "chats": "$chats"}, "total": {"$sum": 1}}},
                {'$sort': {'total': -1}}
            ])
        top_questions_list = []
        c = 0
        for item in db_cursor:
            if ((item['_id']['botid'] == request.session['bot_details']['botid']) and (c <= 19)):
                c = c + 1
                top_questions_list.append({"bot_id": item['_id']['botid'],
                                           "input": item['_id']['chats'][0]['request'],
                                           "output": item['_id']['chats'][0]['response'],
                                           "total": item['total']})
    return render(request, 'top_questions.html', {"top_questions_list": top_questions_list, "botid": botid})

def notify_user(chat_id=None,):

    chat_obj = ChatHistory.objects.get(id=chat_id)
    id = chat_obj.id
    username = chat_obj.username
    emailid = chat_obj.emailid
    question = chat_obj.request
    email(id, username, emailid, question)

    return 'Email sent successfully'


def email(id, user_name, toaddrs, question):

    fromaddr = 'entellio@techmahindra.com'
    msg = ("From:{}\nTo:{}\n\nHi {},\n\nHere are the questions that we have updated in Entellio based on your request:\n ".format(
        fromaddr, toaddrs, user_name))
    msg = ("{}\n\t{}".format(msg, question))
    signature = ("WITH REGARDS,\nENTELLIO HELPDESK")
    msg = (msg + "\n\n{}".format(signature))
    #server = smtplib.SMTP('PUNEXCHMBX001.Techmahindra.com')
    # server.set_debuglevel(1)
    #server.sendmail(fromaddr, toaddrs, msg)
    # server.quit()

    print(msg)