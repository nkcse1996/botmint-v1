from django.conf.urls import url
from . import views

urlpatterns = [
    url(r'line_graph/', views.line_graph, name='line_graph'),
    url(r'histogram/', views.histogram, name='histogram'),
    url(r'report/(?P<environment>\w{0,50})/(?P<client>\w{0,50})/$', views.hr_report, name='HR'),
    url(r'report/$', views.hr_report, name='HR'),
    url(r'hr_action/$', views.hr_action, name='HR'),
    url(r'export_csv/$', views.export_csv, name='HR Export'),
    url(r'^update_response/$', views.updateResponse, name='updateResponse'),
    url(r'^retrieve_chat_history/$', views.retrieve_chat_history, name='retrieve_chat_history'),
    url(r'^top_questions/$', views.top_questions, name='top_questions'),
]