from django.conf.urls import url
from userprofiles.views import ProfileHomeView, ProfileIdentite

app_name = "profile"
urlpatterns = [
    url(r'^$', ProfileHomeView.as_view(), name='home'),
    url(r'^identity/(?P<pk>[0-9]+)/$', ProfileIdentite.as_view(), name='identity-form'),
]
