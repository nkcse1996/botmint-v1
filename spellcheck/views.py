from django.shortcuts import render
# Create your views here.
from django.contrib.auth.decorators import login_required
from bson.json_util import dumps
from django.http import HttpResponse
from django.core.paginator import EmptyPage, PageNotAnInteger, Paginator
from registration.models import Bot
from config.utils import get_bot_db
from django.conf import settings
from django.http import JsonResponse
import json


@login_required()
def spell_index(request):
     return render(request, 'spell_index.html')


@login_required
def search_text(request, bot_id):
    try:
        bot = Bot.objects.get(pk=bot_id)
    except Exception as e:
        print (str(e))
    db_name = get_bot_db(bot)
    spell = settings.CON[db_name].SpellChecker
    #client = MongoClient('localhost:27017')
    #db = client.search
    resp={}
    d = json.loads(request.body.decode('utf-8'))
    if d['action'] == "search":
        try:
            data_list = []
            msg = "word found"
            if len(list(spell.find( {'spell': d['word'] })))!=0:
                for data in spell.find():
                    data = dict(data)
                    data['_id'] = str(data['_id'])
                    data_list.append(data)
                    resp['data'] = data_list
                # paginator = Paginator(data_list, 2)
                # page = request.GET.get('page')
                # data = paginator.get_page(page)
                # return render(request, 'spell_index.html', {'data': data, 'msg':msg })
            else:
                resp = {'status': True, 'msg': "Not Found "}
        except Exception as e:
            print (str(e))
            resp = {'status': False, 'msg': str(e)}
        return JsonResponse(resp)
    else:
            data = {}
            msg = "New word added successfully"
            data['spell'] = d['word']
            try:
                spell(data).save()
                #spell_checker.insert_one(data)
            except Exception as e:
                print (str(e))
            return JsonResponse({'status': True, 'msg': msg })
