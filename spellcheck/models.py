from mongokit import Document
from django.conf import settings

# Create your models here.


@settings.CON.register
class SpellChecker(Document):
    __database__ = 'botmint'
    __collection__ = 'spell'
    use_schemaless = True
    structure = {
        'spell': str
    }
    required_fields = ['spell']
