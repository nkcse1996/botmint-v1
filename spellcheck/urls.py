from django.conf.urls import url, include
from .views import spell_index, search_text
app_name="spellcheck"
urlpatterns = [
    url(r'^$', spell_index, name='spell_page'),
    url(r'^(?P<bot_id>\d+)/spellsearch/$',search_text, name='search_text'),
]
