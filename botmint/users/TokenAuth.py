# ###################################################################################################
# Marker::

# (adding request parameter in authenticate method):abhishek)
# ###################################################################################################

import json

from rest_framework_jwt.views import obtain_jwt_token, refresh_jwt_token, \
    verify_jwt_token
from django.contrib.auth import authenticate, login
from django.http import JsonResponse
from django.views.decorators.csrf import csrf_exempt
from datetime import timedelta as delta
import jwt
from datetime import datetime
from django.contrib.auth import get_user_model
from django.utils.encoding import smart_text
from django.utils.translation import ugettext as _
from rest_framework import exceptions
from rest_framework.authentication import (
    BaseAuthentication, get_authorization_header
)
from rest_framework import authentication
from rest_framework_jwt.settings import api_settings
from rest_framework_jwt.utils import jwt_encode_handler
from uuid import uuid4
from brain.utils import set_bot_details_in_session, \
    set_user_details_in_session
jwt_decode_handler = api_settings.JWT_DECODE_HANDLER
jwt_get_username_from_payload = api_settings.JWT_PAYLOAD_GET_USERNAME_HANDLER
JWT_EXP_DELTA_SECONDS = 36000000


@csrf_exempt
def login_user_api(request, botid=None):

    body_unicode = request.body.decode('utf-8')
    body = json.loads(body_unicode)
    username = body['username']
    password = body['password']
    try:
        if not request.user.is_authenticated():
            user = authenticate(username=username,
                            password=password, request=request)
            if user.is_active:
                login(request, user)
            set_bot_details_in_session(request, botid)
            request.session['user_context'] = {}
    except:
        return JsonResponse({'failure': "login failure"})

    if not body['userinfo'].get('user_id'):
        body['userinfo']['user_id'] = str(uuid4())

    set_user_details_in_session(request, body['userinfo'])
    payload = {
        'session_key': request.session.session_key,
        'user': json.dumps(body['userinfo']),
        'exp': datetime.utcnow() + delta(seconds=JWT_EXP_DELTA_SECONDS)
    }
    jwt_token = jwt_encode_handler(payload)

    return JsonResponse({'token': jwt_token, 'user_id': body['userinfo']['user_id']})


class BaseJSONWebTokenAuthentication(authentication.BaseAuthentication):
    """
    Token based authentication using the JSON Web Token standard.
    """

    def authenticate(self, request):
        """
        Returns a two-tuple of `User` and token if a valid signature has been
        supplied using JWT-based authentication.  Otherwise returns `None`.
        """
        jwt_value = request.META.get('HTTP_TOKEN')
        if not jwt_value:
            payload = {'TokenError': 'Empty Token'}
        else:
            try:
                payload = jwt_decode_handler(jwt_value)
            except jwt.ExpiredSignature as e:
                payload = {
                    'TokenError': 'Signature has expired {} --> {}'.format(jwt_value, str(e))}
            except jwt.DecodeError as e:
                payload = {
                    'TokenError': 'Error decoding signature {} --> {}'.format(jwt_value, str(e))}
            except jwt.InvalidTokenError as e:
                payload = {
                    'TokenError': 'Invalid Token {} --> {}'.format(jwt_value, str(e))}
            except Exception as e:
                payload = {
                    'TokenError': 'Invalid Token {} --> {}'.format(jwt_value, str(e))}

        return payload

    def authenticate_credentials(self, payload):
        """
        Returns an active user that matches the payload's user id and email.
        """
        User = get_user_model()
        username = jwt_get_username_from_payload(payload)

        if not username:
            msg = _('Invalid payload.')
            raise exceptions.AuthenticationFailed(msg)

        try:
            user = User.objects.get_by_natural_key(username)
        except User.DoesNotExist:
            msg = _('Invalid signature.')
            raise exceptions.AuthenticationFailed(msg)

        if not user.is_active:
            msg = _('User account is disabled.')
            raise exceptions.AuthenticationFailed(msg)

        return user


class JSONWebTokenAuthentication(BaseJSONWebTokenAuthentication):
    """
    Clients should authenticate by passing the token key in the "Authorization"
    HTTP header, prepended with the string specified in the setting
    `JWT_AUTH_HEADER_PREFIX`. For example:
        Authorization: JWT eyJhbGciOiAiSFMyNTYiLCAidHlwIj
    """
    www_authenticate_realm = 'api'

    def get_jwt_value(self, request):
        auth = get_authorization_header(request).split()
        auth_header_prefix = api_settings.JWT_AUTH_HEADER_PREFIX.lower()

        if not auth:
            if api_settings.JWT_AUTH_COOKIE:
                return request.COOKIES.get(api_settings.JWT_AUTH_COOKIE)
            return None

        if smart_text(auth[0].lower()) != auth_header_prefix:
            return None

        if len(auth) == 1:
            msg = _('Invalid Authorization header. No credentials provided.')
            raise exceptions.AuthenticationFailed(msg)
        elif len(auth) > 2:
            msg = _('Invalid Authorization header. Credentials string '
                    'should not contain spaces.')
            raise exceptions.AuthenticationFailed(msg)

        return auth[1]

    def authenticate_header(self, request):
        """
        Return a string to be used as the value of the `WWW-Authenticate`
        header in a `401 Unauthenticated` response, or `None` if the
        authentication scheme should return `403 Permission Denied` responses.
        """
        return '{0} realm="{1}"'.format(
            api_settings.JWT_AUTH_HEADER_PREFIX, self.www_authenticate_realm)
