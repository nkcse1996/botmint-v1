from importlib import import_module
from django.core import serializers
from django.http import JsonResponse
from django.shortcuts import get_object_or_404, render
from django.views.decorators.csrf import csrf_exempt
from nlp.process import process
from .utils import save_chat
from registration.models import Bot
from django.contrib.auth.decorators import login_required
from datetime import datetime
import json

from django.contrib.auth import authenticate, login
from uuid import uuid4
from config.utils import get_bot_db
from django.conf import settings


@login_required()
def chat(request, bot_id=None):
    if bot_id:
        bot_obj = get_object_or_404(Bot, pk=bot_id)
        bot = serializers.serialize('json', [bot_obj, ])
    else:
        bot_obj = None
        bot = None
    bots = Bot.objects.filter(admin=request.user)
    dummy_cells = (3 - (bots.count() + 1) % 3) % 3
    return render(request, 'chat.html',  {'bot_c': bot, 'bot_obj': bot_obj,
                                          'bots': bots,
                                          'dummy_cells': range(dummy_cells)})


# @csrf_exempt
# def message(request, bot_id):
#     ans = ""
#     msg = request.POST.get("message")
#     context = request.session.pop("context", "message")
#     if msg:
#         if request.session.get("context", {}).get('spell'):
#             msg = request.session["last_message"]
#         ans, suggestions = get_answer(bot_id, msg)

#         if not ans:
#             try:
#                 ans = wikipedia.summary(msg, sentences=1)
#             except wikipedia.exceptions.DisambiguationError:
#                 pass
#             except wikipedia.exceptions.PageError:
#                 pass

#         if not ans and context != "spell":
#             correction = TextBlob(msg).correct()
#             # print("Norvig correction")
#             # print(correction)
#             # if not settings.SPELL_OBJ:
#             #     SPELL_OBJ = SpellCheck()
#             #
#             # correction = SPELL_OBJ.correct(msg)
#             # print("Ola coorection")
#             # print(correction)
#             if msg != correction:
#                 ans = "Did You mean ? <br>{} <br>YES/NO".format(
#                     str(correction))
#                 request.session["context"] = "spell"
#                 request.session["last_message"] = msg

#         if not ans:
#             ans = "Please repharase your sentence"
#     return JsonResponse({'msg': ans})

@login_required
@csrf_exempt
def message(request, bot_id):
    error = "Only Post request allowed"
    response = ''
    if request.method == 'POST':
        error = ''
        try:
            bot = Bot.objects.get(id=bot_id, status=True)
            print(bot.expiry)
            # expiry__gte=datetime.now())
            bot_session(request, bot)
        except Exception as e:
            error = str(e)

        if not error:
            user_msg = request.POST.get("message")

            context = request.session.get("context", {})
            bot_info = request.session.get("bot")
            user_info = request.session.get("userinfo", {})
            user_id = user_info.get('user_id')
            if not user_id:
                user_id = request.POST.get('user_id', str(uuid4()))
                db_name = get_bot_db(bot)
                bot_user = settings.CON[db_name].BotUser
                user = bot_user.find_one({'user_info.username':
                                         request.POST.get("userinfo", {}).get("username")})
                if not user:
                    try:
                        user = bot_user({'user_id': user_id, 'user_info': {}})
                        user.save()
                    except Exception as e:
                        error = str(e)
                user_info = user['user_info']
                user_info['user_id'] = user['user_id']
                user_session(request, user_info)
            user_info['session_id'] = request.session.session_key
            req_info = {'base_url': request.build_absolute_uri("/")}
            counter = request.session.get('counter', 0)
            response = process(user_msg, bot_info,
                               user_info, context, req_info, counter)

            chat_id = save_chat(request.session.get('chat_id'),
                                user_msg,
                                user_info,
                                response,
                                bot_info,
                                )

            if response.get("context"):
                request.session['chat_id'] = chat_id
            else:
                request.session.pop("chat_id", False)

            request.session['context'] = response.get("context", {})
    return JsonResponse({'error': error, 'resp': response})


@csrf_exempt
def message_api(request, bot_id):

    error = "Only Post request allowed"
    response = ''
    if request.method == 'POST':
        input_data = json.loads(request.body.decode('utf-8'))
        session_key = input_data.get('token')
        if session_key:
            try:
                import_module(settings.SESSION_ENGINE).SessionStore(
                    session_key)
            except Exception as e:
                error = str(e)
            else:
                user_msg = request.POST.get("message")
                context = request.session.get("context", {})
                bot_info = request.session.get("bot")
                user_info = request.session.get("userinfo")
                user_info['session_id'] = request.session.session_key
                req_info = {'base_url': request.build_absolute_uri("/")}
                response = process(user_msg, bot_info,
                                   user_info, context, req_info)

                chat_id = save_chat(request.session.get('chat_id'),
                                    user_msg,
                                    user_info,
                                    response.get("answer"),
                                    bot_info,
                                    )

                if response.get("context"):
                    request.session['chat_id'] = chat_id
                else:
                    request.session.pop("chat_id", False)

                request.session['context'] = response.get("context", {})
                error = ''
        else:
            error = 'token is missing'

    return JsonResponse({'error': error, 'resp': response})


@csrf_exempt
def login_api(request, botid):
    error = ''
    try:
        bot = Bot.objects.get(id=botid, status=True,
                              expiry__gte=datetime.now())
    except Exception as e:
        error = str(e)

    if not error:
        body_unicode = request.body.decode('utf-8')
        body = json.loads(body_unicode)
        username = body['username']
        password = body['password']
        try:
            if not request.user.is_authenticated():
                user = authenticate(username=username,
                                    password=password, request=request)
                if user.is_active:
                    login(request, user)
                bot_session(request, bot)
                request.session['context'] = {}
        except Exception as e:
            error = str(e)

    if not error:
        user_id = body['userinfo'].get('user_id', str(uuid4()))
        db_name = get_bot_db(bot)
        bot_user = settings.CON[db_name].BotUser

        if not body['userinfo'].get('user_id'):
            try:
                users = bot_user.find(user_info__username=body.get(
                    "userinfo", {}).get("username"))
                if users:
                    user_id = users[0].user_id
                else:
                    bot_user(user_id=user_id,
                             user_info=body.get("userinfo", {})).save()
                body['userinfo']['user_id'] = user_id
                user_session(request, body['userinfo'])
            except Exception as e:
                error = str(e)

    return JsonResponse(
        {'token': request.session.session_key,
         'user_id': user_id,
         'error': error
         })


def bot_session(request, bot):
    request.session['bot'] = json.loads(bot.attrs)
    request.session['bot']['botid'] = bot.id
    request.session['bot']['botname'] = bot.name


def user_session(request, userinfo):
    userinfo.update({'username': request.user.get_username()})
    request.session['userinfo'] = userinfo
