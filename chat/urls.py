from django.conf.urls import url
from .views import chat, message, login_api, message_api

app_name = "chat"

urlpatterns = [
    url(r'^$', chat, name='chat'),
    url(r'^(?P<bot_id>\d+)/$', chat, name='chat-bot'),
    url(r'^message/(?P<bot_id>\d+)/$', message, name='message'),
    url(r'^login/(?P<bot_id>\d+)/$', login_api, name='login_api'),
    url(r'^message_api/(?P<bot_id>\d+)/$', message_api, name='message_api')
]
