from django.conf import settings
from mongokit import Document
import datetime


@settings.CON.register
class ChatHistory(Document):
    __database__ = 'botmint'
    __collection__ = 'chat_history'
    use_schemaless = True
    structure = {
        'botid': str,
        'sn_id': str,
        'u_id': str,
        'u_nm': str,
        'u_email': str,
        'chats': [dict],
        'fb_type': str,
        'fb_msg': str,
        'dt': datetime.datetime,
        'scr': str
    }

    required_fields = ['botid', 'sn_id', 'u_id']
    default_values = {'chats': [],
                      'fb_type': 'thumbs',
                      'scr': '1'
                      }
