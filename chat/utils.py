from datetime import datetime
from config.utils import log_error_msg, get_bot_db_from_session
from django.conf import settings


def save_chat(chat_id, usr_msg, user_info, bot_msg, bot_info):
    db_name = get_bot_db_from_session(bot_info)
    chathistory = settings.CON[db_name].ChatHistory

    if not chat_id:
        try:
            data = {
                'botid': str(bot_info['botid']),
                'sn_id': user_info['session_id'],
                'u_id': user_info['user_id'],
                'u_nm': user_info.get('username', 'NA'),
                'u_email': user_info.get('email', 'NA'),
                'chats': [{'user': usr_msg, 'bot': bot_msg.get("answer", ''),
                           'dt': datetime.now()}],
                'fb_type': bot_info.get('feedback_type', 'thumbs'),
                'fb_msg': '',
                'dt': datetime.now(),
                'scr': str(bot_msg.get("score", "1"))
            }
            chat_history = chathistory(data)
            chat_history.save()
            chat_id = str(chat_history['_id'])
        except Exception as e:
            log_error_msg("save_chat", str(e))
    else:
        try:
            chat = {'user': usr_msg, 'bot': bot_msg.get("answer", ''),
                    'dt': datetime.now()}
            chathistory.find_and_modify(
                {'_id': chat_id}, {'$push': {'chats': chat}})
        except Exception as e:
            log_error_msg("save_chat else part", str(e))

    return chat_id
