import zipfile
import os
import shutil
import fileinput

from django.conf import settings

def zipdir(path, ziph):
    # ziph is zipfile handle
    lenDirPath  = len(path)
    for root, dirs, files in os.walk(path):
        for file in files:
            file_path = os.path.join(root, file)
            ziph.write(file_path, file_path[lenDirPath :])
    return


def copy_rename(bot_name, customer_name, info=None):

    try:
        if info is not None:
            dest_name = bot_name+"_"+customer_name
            src_dir = os.path.join(settings.MEDIA_ROOT, "Entellio_client")
            dst_dir = os.path.join(settings.MEDIA_ROOT, "Customer_Folders")
            src_file = os.path.join(src_dir, "entellio.zip")
            shutil.copy(src_file,dst_dir)
            zip_ref = zipfile.ZipFile(src_file, 'r')
            zip_ref.extractall(dst_dir)
            zip_ref.close()
            if os.path.exists(os.path.join(dst_dir,dest_name)):
                shutil.rmtree(os.path.join(dst_dir, dest_name))
            os.remove(os.path.join(dst_dir,"entellio.zip"))
            dst_file = os.path.join(dst_dir, "entellio")
            new_dst_file_name = os.path.join(dst_dir, dest_name)
            os.rename(dst_file, new_dst_file_name)
            # info = {"CUSTOMER_ENTELLIO_URL": "'http://127.0.0.1:8000/'","CUSTOMER_BOT_ID": "'sdfsdfdsfgfdgdfgfd'", "CUSTOMER_BOT_NAME": "'sreelekha'","CUSTOMER_PASSWORD":"'Sree@123'"}
            if info is not None:
                for path_file in ["entellio_static/js/entellio_poc.js","entellio_static/js/entellio.js"]:
                    with fileinput.FileInput(os.path.join(new_dst_file_name,path_file), inplace=True) as file:
                        for line in file:
                            for d in info.keys():
                                if d in line:
                                    line = line.replace(d,info[d])
                                    print(line,end='')
                                    break
                            else:
                                print(line,end='')

                zipf = zipfile.ZipFile(new_dst_file_name+".zip", 'w', zipfile.ZIP_DEFLATED)
                zipdir(new_dst_file_name, zipf)
                zipf.close()
            return bot_name+"_"+customer_name+".zip"
        else:
            return "Fail"
    except Exception as e:
        print(e)
        return "Fail"
