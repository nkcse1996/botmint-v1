var botid = window.botId;
$(document).ready(function() {
    $(".settings").click(function() {
    if (this.getAttribute("data-channel-Model") =="webconnectModal"){
    status_update();

        }
        else{
        $("#" + this.getAttribute("data-channel-Model")).modal('toggle');}
    });

    $('#webconnect_form').validate({ // initialize the plugin
        errorClass: "my-error-class",
        rules: {
            bot_user_name: {
                required: true,
            },
            bot_user_password: {
                required: true,
                minlength: 5
            },
             bot_customer_name: {
                required: true
            }

        },
        message:{
        bot_user_name:"Please enter the username",
        bot_user_password :"Please enter the password",
        bot_customer_name:"Please enter the customer_name"
        },

        submitHandler:function submit(){
            $.ajax({
                url: "/channels/" + botid + "/webconnect/listen/",
                type: 'POST',
                data: $("#webconnect_form").serialize(),
                success: function(result) {
                //alert(JSON.stringify(result));
                if(result["status"] == false){
                    alert(result["message"])
                    $('#webconnect_switch' ).prop('checked', result["status"]);
                    $("#webconnectModal").modal('hide');
                    $("#webconnect_form")[0].reset();

                }else{
                    alert("The user is added successfully");
                    $("#webconnect_file").attr("href","../../../media/Customer_Folders/"+result["message"]);
                    $("#webconnectModal").modal('hide');
                    }
                }
            });
        }
    });

     $('#webconnect_edit_form').validate({ // initialize the plugin
        errorClass: "my-error-class",
        rules: {
            bot_user_name: {
                required: true,
            },
            bot_user_password: {
                required: true,
                minlength: 5
            },


        },
        message:{
        bot_user_name:"Please enter the username",
        bot_user_password :"Please enter the password",
        },

        submitHandler:function submit(){
            var myform = $('#webconnect_edit_form');
            var disabled = myform.find(':input:disabled').removeAttr('disabled');
            form_data = myform.serialize();
            disabled.attr('disabled','disabled');
            $.ajax({
                url: "/channels/" + botid + "/webconnect/edit_webconnect/",
                type: 'POST',
                data: form_data,
                success: function(result) {
                if(result["status"] == false){
                    alert(result["message"])
                    $("#webconnectModal").modal('hide');
                }else{
                    alert("The user is modified successfully");

                    $("#webconnect_file").attr("href","../../../media/Customer_Folders/"+result["message"]);
                    $("#webconnectModal").modal('hide');
                    }
                }
            });
        }
    });

    $("#webform_close").click(function(){
        $("#webconnectModal").modal('hide');
        $("#webconnect_switch").prop('checked',false);
        $("#webconnect_form")[0].reset();

    });

    $("#webform_close1").click(function(){
        $("#webconnectModal").modal('hide');
        //$("#webconnect_switch").prop('checked',false);

    });

    $("input.switch").change(function() {
        var status = this.checked;
        var channel = this.getAttribute("data-channel");
        $.ajax({
            url: "/channels/" + botid + "/" + channel + "/status/",
            type: 'POST',
            data: {
                "status": status
            },
            switch_id: this.id,
            success: function(result) {
                    console.log(result)
                    if(channel =="webconnect"){
                        if(result.count == 0){
                              $("#webconnectModal").modal('toggle');
                              $("#Content_id").hide();
                              $("#Download_Button").hide();
                              $("#Edit_Form").hide();
                        }
                        else{

                                $('#' + this.switch_id).prop('checked', result.status);
                                $("#Content_id").show();
                                $("#Download_Button").show();
                                $("#webconnect_file").attr("href","../../../media/Customer_Folders/"+result["file_name"]);
                                $("#User_Form").hide();

                         }
                    }
                    else{
                        alert(result["message"]);
                        $('#' + this.switch_id).prop('checked', result.status);
                    }

            }
        });
    });


   function status_update(){
//   $("#webconnectModal").modal('toggle');
//   $("#User_Form").show();
        $.ajax({
            url: "/channels/" + botid + "/webconnect/status/",
            type: 'POST',
            data: {
                "status": $('#webconnect_switch').prop('checked')
            },
            success: function(result) {
                    if(result.count == 0){
                          $("#webconnectModal").modal('toggle');
                          $("#Content_id").hide();
                          $("#Download_Button").hide();
                          $("#User_Form").show();
                          $("#Edit_Form").hide();
                    }
                    else{
                            $('#webconnect_switch').prop('checked', result.status);
                            $("#Content_id").show();
                            $("#Download_Button").show();
                            $("#webconnect_file").attr("href","../../../media/Customer_Folders/"+result["file_name"]);
                            $("#User_Form").hide();
                             $("#webconnectModal").modal('toggle');
                             $("#edit_user_name").val(result["user_name"]);
                             $("#edit_bot_user_password").val(result["password"]);
                             $("#edit_customer_name").val(result["customer_name"]);
                             $("#Edit_Form").show();
                     }

            }
        });
   }


    (function load_channels_status_fn() {
        $.ajax({
            url: "/channels/" + botid + "/channels/status/",
            type: 'POST',
            success: function(result) {
                for (var key in result) {
                    if (result.hasOwnProperty(key)) {
                        var switch_id = key.replace("_status", "_switch");
                        var file_id = key.replace("_filename", "_file");
                        $('#' + switch_id).prop('checked', result[key]);
                        $('#' + file_id).attr("href","../../../media/Customer_Folders/"+result[key])
                    }
                }
            }
        });
    })();
});
