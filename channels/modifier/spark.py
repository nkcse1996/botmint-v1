from bs4 import BeautifulSoup
from brain.process_data import prcoess_question



def prcoess_question_spark(request, input_question=None, botid=None):
    html_resp = prcoess_question(request, input_question =input_question, botid= botid)
    if isinstance(html_resp, list): html_resp = " ".join(html_resp)
    soup = BeautifulSoup(html_resp, "html.parser")
    resp= soup.get_text()
    if resp.startswith("Did you mean") and resp.endswith("Yes No"):
        word = resp.strip("Did you mean").rstrip("Yes No").strip()
        resp = "Did You Mean : {} ? Yes No".format(word)
    return resp

