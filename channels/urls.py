from django.conf.urls import url

from .views import slack, spark, skype, channels, facebook, line, twitter, viber, kik, telegram, webconnect

urlpatterns = [
    url(r'^channels/$', channels.install, name='channel_install'),
    url(r'^channels/status/$', channels.status, name='channels_status'),
    # url(r'^channels/Customer_Folders/$', channels.status, name='channels_status')

    url(r'^slack/install$', slack.install, name='slack_install'),
    url(r'^slack/listen/$', slack.listen, name='slack_listen'),
    url(r'^slack/status/$', slack.status, name='slack_status'),

    url(r'^spark/install$', spark.install, name='spark_install'),
    url(r'^spark/listen/$', spark.listen, name='spark_listen'),
    url(r'^spark/status/$', spark.status, name='spark_status'),

    url(r'^skype/install$', skype.install, name='skype_install'),
    url(r'^skype/listen/$', skype.listen, name='skype_listen'),
    url(r'^skype/status/$', skype.status, name='skype_status'),

    url(r'^facebook/install$', facebook.install, name='facebook_install'),
    url(r'^facebook/listen/$', facebook.listen, name='facebook_listen'),
    url(r'^facebook/status/$', facebook.status, name='facebook_status'),

    url(r'^line/install$', line.install, name='line_install'),
    url(r'^line/listen/$', line.listen, name='line_listen'),
    url(r'^line/status/$', line.status, name='line_status'),
    #url(r'^postinstall/$', views.channel_postinstall, name='channel_postinstall'),

    url(r'^twitter/install$', twitter.install, name='twitter_install'),
    url(r'^twitter/listen/$', twitter.listen, name='twitter_listen'),
    url(r'^twitter/status/$', twitter.status, name='twitter_status'),

    # url(r'^viber/install$', viber.install, name='viber_install'),
    # url(r'^viber/listen/$', viber.listen, name='viber_listen'),
    # url(r'^viber/status/$', viber.status, name='viber_status'),

    url(r'^kik/install$', kik.install, name='kik_install'),
    url(r'^kik/listen/$', kik.listen, name='kik_listen'),
    url(r'^kik/status/$', kik.status, name='kik_status'),

    url(r'^telegram/install$', telegram.install, name='telegram_install'),
    url(r'^telegram/listen/$', telegram.listen, name='telegram_listen'),
    url(r'^telegram/status/$', telegram.status, name='telegram_status'),

    url(r'^webconnect/listen/$', webconnect.add_to_customer_db, name='add_to_customer_db'),
    url(r'^webconnect/edit_webconnect/$', webconnect.edit_to_customer_db, name='edit_to_customer_db'),
    url(r'^webconnect/status/$', webconnect.status, name='webconnect_status'),




]
