from django import forms
from django.forms import ModelForm
from .models import SlackAuth, SparkAuth, SkypeAuth, FacebookAuth, LineAuth, \
     TwitterAuth, ViberAuth, KikAuth, TelegramAuth, WebConnectAuth


class SlackAuthForm(ModelForm):

    class Meta:
        model = SlackAuth
        fields = ['bot_name', 'client_id',
                  'client_secret', 'verification_code', 'access_token']

    def __init__(self, *args, **kwargs):
        super(SlackAuthForm, self).__init__(*args, **kwargs)

class SkypeAuthForm(ModelForm):

    class Meta:
        model = SkypeAuth
        fields = ['bot_name', 'app_id', 'app_password']

    def __init__(self, *args, **kwargs):
        super(SkypeAuthForm, self).__init__(*args, **kwargs)

class SparkAuthForm(ModelForm):

    class Meta:
        model = SparkAuth
        fields = ['bot_name', 'bot_email', 'admin_token', 'bot_token']

    def __init__(self, *args, **kwargs):
        super(SparkAuthForm, self).__init__(*args, **kwargs)

class FacebookAuthForm(ModelForm):

    class Meta:
        model = FacebookAuth
        fields = ['bot_name', 'verify_token', 'page_access_token']

    def __init__(self, *args, **kwargs):
        super(FacebookAuthForm, self).__init__(*args, **kwargs)

class LineAuthForm(ModelForm):

    class Meta:
        model = LineAuth
        fields = ['bot_name', 'channel_id', 'channel_secret', 'channel_access_token']

    def __init__(self, *args, **kwargs):
        super(LineAuthForm, self).__init__(*args, **kwargs)

class TwitterAuthForm(ModelForm):

    class Meta:
        model = TwitterAuth
        fields = ['bot_name', 'consumer_key', 'consumer_secret', 'access_token', 'access_token_secret']

    def __init__(self, *args, **kwargs):
        super(TwitterAuthForm, self).__init__(*args, **kwargs)

class ViberAuthForm(ModelForm):

    class Meta:
        model = ViberAuth
        fields = ['bot_name', 'app_key', 'avatar_url']

    def __init__(self, *args, **kwargs):
        super(ViberAuthForm, self).__init__(*args, **kwargs)

class KikAuthForm(ModelForm):

    class Meta:
        model = KikAuth
        fields = ['bot_name', 'app_key']

    def __init__(self, *args, **kwargs):
        super(KikAuthForm, self).__init__(*args, **kwargs)

class TelegramAuthForm(ModelForm):

    class Meta:
        model = TelegramAuth
        fields = ['bot_name', 'token']

    def __init__(self, *args, **kwargs):
        super(TelegramAuthForm, self).__init__(*args, **kwargs)


class WebConnectAuthForm(ModelForm):

    class Meta:
        model = WebConnectAuth
        fields = ['bot_user_name', 'bot_user_password','bot_customer_name']

    def __init__(self, *args, **kwargs):
        super(WebConnectAuthForm, self).__init__(*args, **kwargs)

    # def clean(self):
    #     cleaned_data = super(WebConnectAuthForm, self).clean()
    #     bot_user_name = cleaned_data.get("bot_user_name",None),
    #     bot_user_password =cleaned_data.get("bot_user_password",None),
    #     bot_customer_name =cleaned_data.get("bot_customer_name")
    #     if bot_user_name is None or bot_user_password is None or  bot_customer_name is None:
    #         raise forms.ValidationError("No data")
    def as_json(self):
        cleaned_data = super(WebConnectAuthForm, self).clean()
        return dict(
            # CUSTOMER_BOT_ID = "'{}'".format(self.bot.bot_apikey),
            CUSTOMER_BOT_NAME="'{}'".format(cleaned_data.get("bot_user_name")),
            CUSTOMER_PASSWORD="'{}'".format(cleaned_data.get("bot_user_password")),
            BOT_CUSTOMER_NAME="'{}'".format(cleaned_data.get("bot_customer_name")))
