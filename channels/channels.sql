D:\entellio\Entellio\trunk\entellioReportPortal
BEGIN;
--
-- Create model SkypeAuth
--
CREATE TABLE `SkypeAuth` (`id` integer AUTO_INCREMENT NOT NULL PRIMARY KEY, `bot_name` varchar(128) NOT NULL, `app_id` varchar(128) NOT NULL, `app_password` varchar(256) NOT NULL, `status` bool NOT NULL, `bot_id` integer NOT NULL);
--
-- Create model SlackAuth
--
CREATE TABLE `SlackAuth` (`id` integer AUTO_INCREMENT NOT NULL PRIMARY KEY, `bot_name` varchar(128) NOT NULL, `client_id` varchar(128) NOT NULL, `client_secret` varchar(256) NOT NULL, `verification_code` varchar(128) NOT NULL, `access_token` varchar(256) NOT NULL, `status` bool NOT NULL, `bot_id` integer NOT NULL);
--
-- Create model SparkAuth
--
CREATE TABLE `SparkAuth` (`id` integer AUTO_INCREMENT NOT NULL PRIMARY KEY, `bot_name` varchar(128) NOT NULL, `bot_email` varchar(128) NOT NULL, `admin_token` varchar(256) NOT NULL, `bot_token` varchar(256) NOT NULL, `status` bool NOT NULL, `bot_id` integer NOT NULL);
--
-- Create model FacebookAuth
--
CREATE TABLE `FacebookAuth` (`id` integer AUTO_INCREMENT NOT NULL PRIMARY KEY, `bot_name` varchar(128) NOT NULL, `verify_token` varchar(128) NOT NULL, `page_access_token` varchar(256) NOT NULL, `status` bool NOT NULL, `bot_id` integer NOT NULL);
--
-- Create model LineAuth
--
CREATE TABLE `LineAuth` (`id` integer AUTO_INCREMENT NOT NULL PRIMARY KEY, `bot_name` varchar(128) NOT NULL, `channel_id` varchar(128) NOT NULL, `channel_secret` varchar(128) NOT NULL, `channel_access_token` varchar(256) NOT NULL, `status` bool NOT NULL, `bot_id` integer NOT NULL);
--
-- Create model KikAuth
--
CREATE TABLE `KikAuth` (`id` integer AUTO_INCREMENT NOT NULL PRIMARY KEY, `bot_name` varchar(128) NOT NULL, `app_key` varchar(128) NOT NULL, `status` bool NOT NULL, `bot_id` integer NOT NULL);
--
-- Create model TelegramAuth
--
CREATE TABLE `TelegramAuth` (`id` integer AUTO_INCREMENT NOT NULL PRIMARY KEY, `bot_name` varchar(128) NOT NULL, `token` varchar(128) NOT NULL, `status` bool NOT NULL, `bot_id` integer NOT NULL);
--
-- Create model TwitterAuth
--
CREATE TABLE `TwitterAuth` (`id` integer AUTO_INCREMENT NOT NULL PRIMARY KEY, `bot_name` varchar(128) NOT NULL, `consumer_key` varchar(128) NOT NULL, `consumer_secret` varchar(128) NOT NULL, `access_token` varchar(128) NOT NULL, `access_token_secret` varchar(129) NOT NULL, `status` bool NOT NULL, `bot_id` integer NOT NULL);
--
-- Create model ViberAuth
--
CREATE TABLE `ViberAuth` (`id` integer AUTO_INCREMENT NOT NULL PRIMARY KEY, `bot_name` varchar(128) NOT NULL, `app_key` varchar(128) NOT NULL, `avatar_url` varchar(256) NOT NULL, `status` bool NOT NULL, `bot_id` integer NOT NULL);
ALTER TABLE `SkypeAuth` ADD CONSTRAINT `SkypeAuth_bot_id_15e7bb59_fk_bot_details_botid` FOREIGN KEY (`bot_id`) REFERENCES `bot_details` (`botid`);
ALTER TABLE `SlackAuth` ADD CONSTRAINT `SlackAuth_bot_id_779e7ee8_fk_bot_details_botid` FOREIGN KEY (`bot_id`) REFERENCES `bot_details` (`botid`);
ALTER TABLE `SparkAuth` ADD CONSTRAINT `SparkAuth_bot_id_7a4e32de_fk_bot_details_botid` FOREIGN KEY (`bot_id`) REFERENCES `bot_details` (`botid`);
ALTER TABLE `FacebookAuth` ADD CONSTRAINT `FacebookAuth_bot_id_f35aa1f6_fk_bot_details_botid` FOREIGN KEY (`bot_id`) REFERENCES `bot_details` (`botid`);
ALTER TABLE `LineAuth` ADD CONSTRAINT `LineAuth_bot_id_5fe724af_fk_bot_details_botid` FOREIGN KEY (`bot_id`) REFERENCES `bot_details` (`botid`);
ALTER TABLE `KikAuth` ADD CONSTRAINT `KikAuth_bot_id_6fb8c1c4_fk_bot_details_botid` FOREIGN KEY (`bot_id`) REFERENCES `bot_details` (`botid`);
ALTER TABLE `TelegramAuth` ADD CONSTRAINT `TelegramAuth_bot_id_ea7028d5_fk_bot_details_botid` FOREIGN KEY (`bot_id`) REFERENCES `bot_details` (`botid`);
ALTER TABLE `TwitterAuth` ADD CONSTRAINT `TwitterAuth_bot_id_59caac25_fk_bot_details_botid` FOREIGN KEY (`bot_id`) REFERENCES `bot_details` (`botid`);
ALTER TABLE `ViberAuth` ADD CONSTRAINT `ViberAuth_bot_id_d9dc9aa5_fk_bot_details_botid` FOREIGN KEY (`bot_id`) REFERENCES `bot_details` (`botid`);
COMMIT;
