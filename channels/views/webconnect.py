import os
from urllib.parse import urlparse

from django.conf import settings
from django.contrib.auth.models import Group
from django.contrib.auth.models import User
from django.http import JsonResponse
from django.views.decorators.csrf import csrf_exempt

from bot_registration.models import bot_details
from channels.GeneratePOC import copy_rename
from channels.forms import WebConnectAuthForm
from channels.models import WebConnectAuth

your_media_root = settings.MEDIA_ROOT

@csrf_exempt
def add_to_customer_db(request, botid):
    try:
        if request.POST:
            parsed_uri = urlparse(request.build_absolute_uri())
            domain = '{uri.scheme}://{uri.netloc}'.format(uri=parsed_uri)
            details = bot_details.objects.get(bot_apikey=botid)
            form = WebConnectAuthForm(request.POST)
            if form.is_valid():
                try:
                    user = User.objects.filter(username=form.cleaned_data.get("bot_user_name"),first_name=botid[-6:]+"_"+form.cleaned_data.get("bot_customer_name"))
                    if user:
                        return JsonResponse({"message": "User is already defined", "status": False})
                except User.DoesNotExist:
                    pass
                customer_name = form.cleaned_data.get("bot_customer_name",None)
                if customer_name is None:
                    return JsonResponse({'message': "Failed", 'status': False})
                info = form.as_json()
                info["CUSTOMER_BOT_ID"] = "'{}'".format(botid)
                info["CUSTOMER_ENTELLIO_URL"] = "'{}'".format(domain)
                dst_dir = your_media_root+"/"+"Customer_Folders"
                onlyfiles = [f for f in os.listdir(dst_dir) if (botid[-6:]+"_"+customer_name) in f]
                if len(onlyfiles) > 0:
                        return JsonResponse({"message": "Filename already exists, Please change the name and submit it","status": False})
                else:
                    results_copy = copy_rename(botid[-6:], customer_name, info)
                    if results_copy != "Fail":
                        _auth = form.save(commit=False)
                        _auth.bot = bot_details.objects.get(bot_apikey=botid)
                        _auth.status = True
                        _auth.save()
                        obj = WebConnectAuth.objects.filter(bot=details)
                        bot_group, created = Group.objects.get_or_create(name=botid)
                        # bot_group1, created = Group.objects.get_or_create(name="Learning")
                        # bot_group2, created = Group.objects.get_or_create(name="Report")
                        # bot_group3, created = Group.objects.get_or_create(name="channels")
                        # bot_group4, created = Group.objects.get_or_create(name="api")
                        user1 = User.objects.create_user(username=obj[0].bot_user_name,
                                                         email='techm@test.com',
                                                         first_name=botid[-6:]+"_"+obj[0].bot_customer_name,
                                                         password=obj[0].bot_user_password)

                        bot_group.user_set.add(user1)
                        # bot_group1.user_set.add(user1)
                        # bot_group2.user_set.add(user1)
                        # bot_group3.user_set.add(user1)
                        # bot_group4.user_set.add(user1)
                        return JsonResponse({'message': results_copy, 'status': True})
                    else:
                        return JsonResponse({'message': "Failed", 'status': False})
            else:
                return JsonResponse({'message': "Failed", 'status': False})
    except Exception as e:
        print(e)
        return JsonResponse({'message': "Failed", 'status': False})


@csrf_exempt
def edit_to_customer_db(request, botid):
    try:
        if request.POST:
            parsed_uri = urlparse(request.build_absolute_uri())
            domain = '{uri.scheme}://{uri.netloc}'.format(uri=parsed_uri)
            bot = bot_details.objects.filter(bot_apikey=botid)
            if bot:
                bot = bot[0]
                _bot = WebConnectAuth.objects.filter(bot=bot)
                if _bot:
                    form = WebConnectAuthForm(request.POST)
                    if form.is_valid():
                        try:
                            user = User.objects.filter(username=form.cleaned_data.get("bot_user_name"),first_name=botid[-6:]+"_"+form.cleaned_data.get("bot_customer_name"))
                            if user:
                                return JsonResponse({"message": "User is already defined", "status": False})
                        except User.DoesNotExist:
                            pass
                        customer_name = form.cleaned_data.get("bot_customer_name",None)
                        info = form.as_json()
                        info["CUSTOMER_BOT_ID"] = "'{}'".format(botid)
                        info["CUSTOMER_ENTELLIO_URL"] = "'{}'".format(domain)
                        results_copy = copy_rename(botid[-6:], customer_name, info)
                        if results_copy != "Fail":
                            _bot[0].bot_user_name = form.cleaned_data.get("bot_user_name")
                            _bot[0].bot_user_password = form.cleaned_data.get("bot_user_password")
                            _bot[0].bot_customer_name = form.cleaned_data.get("bot_customer_name")
                            _bot[0].status = True
                            _bot[0].save()
                            obj = WebConnectAuth.objects.filter(bot=bot)
                            bot_group, created = Group.objects.get_or_create(name=botid)
                            # bot_group1, created = Group.objects.get_or_create(name="Learning")
                            # bot_group2, created = Group.objects.get_or_create(name="Report")
                            # bot_group3, created = Group.objects.get_or_create(name="channels")
                            # bot_group4, created = Group.objects.get_or_create(name="api")
                            user1 = User.objects.filter(first_name=botid[-6:]+"_"+obj[0].bot_customer_name)
                            if user1:
                                user1.delete()
                            user2 = User.objects.create_user(username=obj[0].bot_user_name,
                                                             email='techm@test.com',
                                                             first_name=botid[-6:]+"_"+obj[0].bot_customer_name,
                                                             password=obj[0].bot_user_password)


                            bot_group.user_set.add(user2)
                            # bot_group1.user_set.add(user2)
                            # bot_group2.user_set.add(user2)
                            # bot_group3.user_set.add(user2)
                            # bot_group4.user_set.add(user2)
                            return JsonResponse({'message': results_copy, 'status': True})
                        else:
                            return JsonResponse({'message': "Failed", 'status': False})
                else:
                    return JsonResponse({'message': "Failed", 'status': False})
    except Exception as e:
        print(e)
        return JsonResponse({'message': "Failed", 'status': False})

@csrf_exempt
def status(request, botid=None):
    status = {'message': "Unable to find Web Channel for bot.", 'status': False,'count': 0}
    s_status = True if request.POST.get('status') in ["True", "true"] else False
    bot = bot_details.objects.filter(bot_apikey=botid)
    if bot:
        bot = bot[0]

        _bot = WebConnectAuth.objects.filter(bot=bot)

        if _bot:
            file_name = str(botid[-6:])+"_"+str(_bot[0].bot_customer_name)+".zip"
            _bot[0].status = s_status
            _bot[0].save()
            status['message'] = "Web Channel status changed successfully."
            status['file_name'] = file_name
            status['status'] = s_status
            status['count'] = 1
            status["user_name"] = _bot[0].bot_user_name
            status["password"] = _bot[0].bot_user_password
            status["customer_name"] = _bot[0].bot_customer_name
    return JsonResponse(status)
