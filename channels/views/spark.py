from django.http import HttpResponse, JsonResponse
from django.shortcuts import render, redirect
from django.views.decorators.csrf import csrf_exempt
import json
from channels.bots import spark_bot
from django.urls import reverse
from channels.models import SparkAuth
from channels.forms import SparkAuthForm
from channels.utils import get_absolute_https_url
from bot_registration.models import bot_details


def install(request, botid):
    queryset = SparkAuth.objects.filter(bot__bot_apikey=botid)
    if queryset:
        queryset = queryset[0]
    if request.POST:
        form_data = request.POST
        if queryset:
            form = SparkAuthForm(request.POST, instance=queryset)
        else:
            form = SparkAuthForm(request.POST)

        if form.is_valid():
            spark_auth = form.save(commit=False)
            spark_auth.bot = bot_details.objects.get(bot_apikey=botid)
            spark_auth.status = True
            spark_auth.save()
            sparkBot = spark_bot.Bot.get_bot(botid)
            targetUrl = get_absolute_https_url(request, 'spark_listen', botid)
            sparkBot.set_webhook(botid, targetUrl)
            return redirect(reverse('channel_install', args=[botid]))

@csrf_exempt
def listen(request, botid, *args, **kwargs):
    response = 'Only Post Allowed'
    if request.method == 'POST':
        request.session['bot_details']['botid'] = botid
        request.session['user_context'] = request.session.get(
            'user_context', {})
        data = json.loads(request.body.decode('utf-8'))
        room_id = data.get('data', {}).get('roomId')
        room_type = data.get('data', {}).get('roomType')
        person_email = data.get('data', {}).get('personEmail')
        mentioned_people = data.get('data', {}).get('mentionedPeople')
        message_id = data.get('data', {}).get('id', '')
        sparkBot = spark_bot.Bot.get_bot(botid)
        if sparkBot:
            message = sparkBot.client.messages.get(message_id)
            text = message.text
            response = "Posted"
            if room_type == 'group':
                sparkBot.chat_message(request, room_id, text)
            elif person_email != sparkBot.email:
                sparkBot.chat_message(request, room_id, text)
        else:
            response = "Either Cisco spark is not integrated or enabled to entellio"
    return JsonResponse(response, safe=False)

@csrf_exempt
def status(request, botid=None):
    status = {'message': "Unable to find Spark Channel for bot.", 'status': False}
    s_status = True if request.POST.get('status') in ["True", "true"] else False
    bot = bot_details.objects.filter(bot_apikey=botid)
    if bot:
        bot = bot[0]
        sparkbot = SparkAuth.objects.filter(bot=bot)
        if sparkbot:
            sparkbot[0].status = s_status
            sparkbot[0].save()
            status['message'] = "Spark Channel status changed successfully."
            status['status'] = s_status
        if not s_status and botid in spark_bot.Bot.BOTS:
            del spark_bot.Bot.BOTS[botid]
    return JsonResponse(status)
