from django.http import HttpResponse, JsonResponse
from django.shortcuts import render, redirect
from django.views.decorators.csrf import csrf_exempt
import json
from channels.bots import skype_bot
from django.urls import reverse
from channels.models import SkypeAuth
from channels.forms import SkypeAuthForm
from bot_registration.models import bot_details
from fwk.utils import log_error_msg


def install(request, botid):
    queryset = SkypeAuth.objects.filter(bot__bot_apikey=botid)
    if queryset:
        queryset = queryset[0]
    if request.POST:
        form_data = request.POST
        if queryset:
            form = SkypeAuthForm(request.POST, instance=queryset)
        else:
            form = SkypeAuthForm(request.POST)

        if form.is_valid():
            _auth = form.save(commit=False)
            _auth.bot = bot_details.objects.get(bot_apikey=botid)
            _auth.status = True
            _auth.save()
            return redirect(reverse('channel_install', args=[botid]))

@csrf_exempt  # TO DO
def listen(request, botid, *args, **kwargs):
    response = 'Only Post Allowed'
    if request.method == 'POST':
        request.session['bot_details']['botid'] = botid
        request.session['user_context'] = request.session.get(
            'user_context', {})
        try:
            data = json.loads(request.body.decode('utf-8'))
        except Exception as e:
            log_error_msg("listen", str(e))
            data = {}
        act_id = data.get('id')
        conv_id = data.get('conversation', {}).get('id')
        service_url = data.get('serviceUrl')
        text = data.get('text', '')
        sender = data.get('recipient', {}).get('id')

        _bot = skype_bot.Bot.get_bot(botid)
        if _bot:
            if sender != _bot.name:
                _bot.chat_message(
                    request, service_url, conv_id, act_id, sender, text)
            response = "ok"
        else:
            response = "Either Skype is not integrated or enabled to entellio"
    return JsonResponse(response, safe=False)

@csrf_exempt
def status(request, botid=None):
    status = {'message': "Unable to find Skype Channel for bot.", 'status': False}
    s_status = True if request.POST.get('status') in ["True", "true"] else False
    bot = bot_details.objects.filter(bot_apikey=botid)
    if bot:
        bot = bot[0]
        _bot = SkypeAuth.objects.filter(bot=bot)
        if _bot:
            _bot[0].status = s_status
            _bot[0].save()
            status['message'] = "Skype Channel status changed successfully."
            status['status'] = s_status
    if not s_status and botid in skype_bot.Bot.BOTS:
        del skype_bot.Bot.BOTS[botid]
    return JsonResponse(status)
