from django.http import HttpResponse, JsonResponse
from django.shortcuts import render, redirect
from django.views.decorators.csrf import csrf_exempt
import json
from channels.bots import viber_bot
from django.urls import reverse
from channels.models import ViberAuth
from channels.forms import ViberAuthForm
from bot_registration.models import bot_details


def install(request, botid):
    queryset = ViberAuth.objects.filter(bot__bot_apikey=botid)
    if queryset:
        queryset = queryset[0]
    if request.POST:
        form_data = request.POST
        if queryset:
            form = ViberAuthForm(request.POST, instance=queryset)
        else:
            form = ViberAuthForm(request.POST)

        if form.is_valid():
            _auth = form.save(commit=False)
            _auth.bot = bot_details.objects.get(bot_apikey=botid)
            _auth.status = True
            _auth.save()
            return redirect(reverse('channel_install', args=[botid]))

@csrf_exempt
def listen(request, botid):
    _bot = viber_bot.Bot.get_bot(botid)
    if request.method == 'GET':
        _event = request.GET
        if _event.get("hub.mode") == "subscribe" and _event.get("hub.challenge") and \
            _event.get('hub.verify_token') == _bot.verify_token:
                return HttpResponse(_event.get("hub.challenge"))
        else:
            return HttpResponse("Verification token mismatch or invalid request", status=403)
    elif request.method == 'POST':
        _event = json.loads(request.body.decode())
        request.session['bot_details']['botid'] = botid
        request.session['user_context'] = request.session.get(
            'user_context', {})
        for entry in _event["entry"]:
            for messaging_event in entry["messaging"]:
                if messaging_event.get("message"):  # someone sent us a message
                    sender_id = messaging_event["sender"]["id"]        # the viber ID of the person sending you the message
                    recipient_id = messaging_event["recipient"]["id"]  # the recipient's ID, which should be your page's viber ID
                    _text = messaging_event["message"]["text"]  # the message's text
        _bot.chat_message(request, sender_id, _text)
        return HttpResponse(status=200)
    else:
        return HttpResponse("{} method not valid".format(request.method), status=403)

@csrf_exempt
def status(request, botid=None):
    status = {'message': "Unable to find Viber Channel for bot.", 'status': False}
    s_status = True if request.POST.get('status') in ["True", "true"] else False
    bot = bot_details.objects.filter(bot_apikey=botid)
    if bot:
        bot = bot[0]
        _bot = ViberAuth.objects.filter(bot=bot)
        if _bot:
            _bot[0].status = s_status
            _bot[0].save()
            status['message'] = "Viber Channel status changed successfully."
            status['status'] = s_status
        if not s_status and botid in viber_bot.Bot.BOTS:
            del viber_bot.Bot.BOTS[botid]
    return JsonResponse(status)
