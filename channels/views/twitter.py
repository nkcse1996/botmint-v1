from django.http import HttpResponse, JsonResponse
from django.shortcuts import render, redirect
from django.views.decorators.csrf import csrf_exempt
import json
from channels.bots import twitter_bot
from django.urls import reverse
from channels.models import TwitterAuth
from channels.forms import TwitterAuthForm
from bot_registration.models import bot_details


def install(request, botid):
    queryset = TwitterAuth.objects.filter(bot__bot_apikey=botid)
    if queryset:
        queryset = queryset[0]
    if request.POST:
        form_data = request.POST
        if queryset:
            form = TwitterAuthForm(request.POST, instance=queryset)
        else:
            form = TwitterAuthForm(request.POST)

        if form.is_valid():
            _auth = form.save(commit=False)
            _auth.bot = bot_details.objects.get(bot_apikey=botid)
            _auth.status = True
            _auth.save()
            return redirect(reverse('channel_install', args=[botid]))

@csrf_exempt
def listen(request, botid):
    _bot = twitter_bot.Bot.get_bot(botid)
    _bot.chat_message(request)
    return HttpResponse("Twitter Bot started listening", status=200)

@csrf_exempt
def status(request, botid=None):
    status = {'message': "Unable to find Twitter Channel for bot.", 'status': False}
    s_status = True if request.POST.get('status') in ["True", "true"] else False
    bot = bot_details.objects.filter(bot_apikey=botid)
    if bot:
        bot = bot[0]
        _bot = TwitterAuth.objects.filter(bot=bot)
        if _bot:
            _bot[0].status = s_status
            _bot[0].save()
            status['message'] = "Twitter Channel status changed successfully."
            status['status'] = s_status
        if not s_status and botid in twitter_bot.Bot.BOTS:
            twitter_bot.Bot.BOTS[botid].disconnect()
            del twitter_bot.Bot.BOTS[botid]
    return JsonResponse(status)
