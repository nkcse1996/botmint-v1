from django.http import HttpResponse, JsonResponse, HttpResponseBadRequest, HttpResponseForbidden
from django.shortcuts import render, redirect
from django.views.decorators.csrf import csrf_exempt
import json
from channels.bots import line_bot
from django.urls import reverse
from channels.models import LineAuth
from channels.forms import LineAuthForm
from bot_registration.models import bot_details


def install(request, botid):
    queryset = LineAuth.objects.filter(bot__bot_apikey=botid)
    if queryset:
        queryset = queryset[0]
    if request.POST:
        form_data = request.POST
        if queryset:
            form = LineAuthForm(request.POST, instance=queryset)
        else:
            form = LineAuthForm(request.POST)

        if form.is_valid():
            _auth = form.save(commit=False)
            _auth.bot = bot_details.objects.get(bot_apikey=botid)
            _auth.status = True
            _auth.save()
            return redirect(reverse('channel_install', args=[botid]))

@csrf_exempt
def listen(request, botid):
    _bot = line_bot.Bot.get_bot(botid)
    if request.method == 'POST':
        signature = request.META['HTTP_X_LINE_SIGNATURE']
        body = request.body.decode('utf-8')
        request.session['bot_details']['botid'] = botid
        request.session['user_context'] = request.session.get(
            'user_context', {})
        result = _bot.chat_message(request, body, signature)
        if result == 'forbidden':
            return HttpResponseForbidden()
        elif result == "badrequest":
            return HttpResponseBadRequest()
        else:
            return HttpResponse()
    else:
        return HttpResponse("{} method not valid".format(request.method), status=403)

@csrf_exempt
def status(request, botid=None):
    status = {'message': "Unable to find Line Channel for bot.", 'status': False}
    s_status = True if request.POST.get('status') in ["True", "true"] else False
    bot = bot_details.objects.filter(bot_apikey=botid)
    if bot:
        bot = bot[0]
        _bot = LineAuth.objects.filter(bot=bot)
        if _bot:
            _bot[0].status = s_status
            _bot[0].save()
            status['message'] = "Line Channel status changed successfully."
            status['status'] = s_status
        if not s_status and botid in line_bot.Bot.BOTS:
            del line_bot.Bot.BOTS[botid]
    return JsonResponse(status)
