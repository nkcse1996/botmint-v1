from django.http import HttpResponse, JsonResponse
from django.shortcuts import render, redirect
from django.views.decorators.csrf import csrf_exempt
import json
from channels.bots import slack_bot
from channels.bots import spark_bot
from channels.bots import skype_bot
from channels.bots import facebook_bot
from channels.bots import line_bot
from channels.bots import twitter_bot
from channels.bots import viber_bot
from channels.bots import kik_bot
from channels.bots import telegram_bot
from channels.bots import webconnect_bot
from channels.utils import get_absolute_https_url

from channels.forms import SlackAuthForm, SparkAuthForm, SkypeAuthForm, \
    FacebookAuthForm, LineAuthForm, TwitterAuthForm, ViberAuthForm, \
    KikAuthForm, TelegramAuthForm

from bot_registration.models import bot_details
# Create your views here.


def install(request, botid):
    bot_detail = bot_details.objects.filter(bot_apikey=botid)[0]
    slacktargetUrl = get_absolute_https_url(request, 'slack_listen', botid)
    skypetargetUrl = get_absolute_https_url(request, 'skype_listen', botid)
    facebooktargetUrl = get_absolute_https_url(request, 'facebook_listen', botid)
    linetargetUrl = get_absolute_https_url(request, 'line_listen', botid)
    queryset = slack_bot.Bot.get_bot_queryset(botid)
    if queryset:
        slack_form = SlackAuthForm(instance=queryset)
    else:
        slack_form = SlackAuthForm()

    queryset = spark_bot.Bot.get_bot_queryset(botid)
    if queryset:
        spark_form = SparkAuthForm(instance=queryset)
    else:
        spark_form = SparkAuthForm()

    queryset = skype_bot.Bot.get_bot_queryset(botid)
    if queryset:
        skype_form = SkypeAuthForm(instance=queryset)
    else:
        skype_form = SkypeAuthForm()

    queryset = facebook_bot.Bot.get_bot_queryset(botid)
    if queryset:
        facebook_form = FacebookAuthForm(instance=queryset)
    else:
        facebook_form = FacebookAuthForm()

    queryset = line_bot.Bot.get_bot_queryset(botid)
    if queryset:
        line_form = LineAuthForm(instance=queryset)
    else:
        line_form = LineAuthForm()

    queryset = twitter_bot.Bot.get_bot_queryset(botid)
    if queryset:
        twitter_form = TwitterAuthForm(instance=queryset)
    else:
        twitter_form = TwitterAuthForm()


    queryset = viber_bot.Bot.get_bot_queryset(botid)
    if queryset:
        viber_form = ViberAuthForm(instance=queryset)
    else:
        viber_form = ViberAuthForm()

    queryset = kik_bot.Bot.get_bot_queryset(botid)
    if queryset:
        kik_form = KikAuthForm(instance=queryset)
    else:
        kik_form = KikAuthForm()

    queryset = telegram_bot.Bot.get_bot_queryset(botid)
    if queryset:
        telegram_form = TelegramAuthForm(instance=queryset)
    else:
        telegram_form = TelegramAuthForm()

    return render(request, "channels.html", {'botid': botid,
                                             'bot_name': bot_detail.botname,
                                             'bot_username': bot_detail.bot_username,
                                             'slack_targetUrl': slacktargetUrl,
                                             'skype_targetUrl': skypetargetUrl,
                                             'facebook_targetUrl': facebooktargetUrl,
                                             'line_targetUrl': linetargetUrl,
                                             'slack_form': slack_form,
                                             'spark_form': spark_form,
                                             'skype_form': skype_form,
                                             'facebook_form': facebook_form,
                                             'line_form': line_form,
                                             'twitter_form': twitter_form,
                                             'viber_form': viber_form,
                                             'kik_form': kik_form,
                                             'telegram_form': telegram_form
                                             })


def pretty_request(request):
    """
        This function is for debugging purpose only
    """
    headers = ''
    for header, value in request.META.items():
        if not header.startswith('HTTP'):
            continue
        header = '-'.join([h.capitalize()
                           for h in header[5:].lower().split('_')])
        headers += '{}: {}\n'.format(header, value)

    return (
        '{method} HTTP/1.1\n'
        'Content-Length: {content_length}\n'
        'Content-Type: {content_type}\n'
        '{headers}\n\n'
        '{body}'
    ).format(
        method=request.method,
        content_length=request.META['CONTENT_LENGTH'],
        content_type=request.META['CONTENT_TYPE'],
        headers=headers,
        body=request.body,
    )


@csrf_exempt
def status(request, botid=None):
    status_dict = {}
    status_dict['slack_status'] = True if slack_bot.Bot.get_bot(
        botid) else False
    status_dict['skype_status'] = True if skype_bot.Bot.get_bot(
        botid) else False
    status_dict['spark_status'] = True if spark_bot.Bot.get_bot(
        botid) else False
    status_dict['facebook_status'] = True if facebook_bot.Bot.get_bot(
        botid) else False
    status_dict['line_status'] = True if line_bot.Bot.get_bot(
        botid) else False
    status_dict['twitter_status'] = True if twitter_bot.Bot.get_bot(
        botid) else False
    status_dict['viber_status'] = True if viber_bot.Bot.get_bot(
        botid) else False
    status_dict['kik_status'] = True if kik_bot.Bot.get_bot(
        botid) else False
    status_dict['telegram_status'] = True if telegram_bot.Bot.get_bot(
        botid) else False
    status_dict['webconnect_status'] = True if webconnect_bot.Bot.get_bot(
        botid) else False
    bot_folder_name = webconnect_bot.Bot.get_bot_folder(botid)
    status_dict['webconnect_filename'] = bot_folder_name.bot_file_name if bot_folder_name else False
    print(status_dict)
    return JsonResponse(status_dict, safe=False)
