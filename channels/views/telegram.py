from django.http import HttpResponse, JsonResponse
from django.shortcuts import render, redirect
from django.views.decorators.csrf import csrf_exempt
import json
from channels.bots import telegram_bot
from django.urls import reverse
from channels.models import TelegramAuth
from channels.forms import TelegramAuthForm
from bot_registration.models import bot_details


def install(request, botid):
    queryset = TelegramAuth.objects.filter(bot__bot_apikey=botid)
    if queryset:
        queryset = queryset[0]
    if request.POST:
        form_data = request.POST
        if queryset:
            form = TelegramAuthForm(request.POST, instance=queryset)
        else:
            form = TelegramAuthForm(request.POST)

        if form.is_valid():
            _auth = form.save(commit=False)
            _auth.bot = bot_details.objects.get(bot_apikey=botid)
            _auth.status = True
            _auth.save()
            return redirect(reverse('channel_install', args=[botid]))

@csrf_exempt
def listen(request, botid):
    _bot = telegram_bot.Bot.get_bot(botid)
    _bot.chat_message(request)
    return HttpResponse("telegram bot started listening", status=200)

@csrf_exempt
def status(request, botid=None):
    status = {'message': "Unable to find Telegram Channel for bot.", 'status': False}
    s_status = True if request.POST.get('status') in ["True", "true"] else False
    bot = bot_details.objects.filter(bot_apikey=botid)
    if bot:
        bot = bot[0]
        _bot = TelegramAuth.objects.filter(bot=bot)
        if _bot:
            _bot[0].status = s_status
            _bot[0].save()
            status['message'] = "Telegram Channel status changed successfully."
            status['status'] = s_status
        if not s_status and botid in telegram_bot.Bot.BOTS:
            telegram_bot.Bot.BOTS[botid].disconnect()
            del telegram_bot.Bot.BOTS[botid]
    return JsonResponse(status)
