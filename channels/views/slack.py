from django.http import HttpResponse, JsonResponse
from django.shortcuts import render, redirect
from django.views.decorators.csrf import csrf_exempt
import json
from channels.bots import slack_bot
from django.urls import reverse
from channels.models import SlackAuth
from channels.forms import SlackAuthForm
from bot_registration.models import bot_details


def install(request, botid):
    queryset = SlackAuth.objects.filter(bot__bot_apikey=botid)
    if queryset:
        queryset = queryset[0]
    if request.POST:
        form_data = request.POST
        if queryset:
            form = SlackAuthForm(request.POST, instance=queryset)
        else:
            form = SlackAuthForm(request.POST)

        if form.is_valid():
            slack_auth = form.save(commit=False)
            slack_auth.bot = bot_details.objects.get(bot_apikey=botid)
            slack_auth.status = True
            slack_auth.save()
            return redirect(reverse('channel_install', args=[botid]))

@csrf_exempt
def listen(request, botid):
    slack_event = json.loads(request.body.decode())

    if "challenge" in slack_event:
        return HttpResponse(slack_event["challenge"])
    slackBot = slack_bot.Bot.get_bot(botid)
    if slackBot.verification != slack_event.get("token"):
        message = {'msg': "Invalid Slack verification token: %s \nslackBot has: \
                   %s\n\n" % (slack_event["token"], slackBot.verification)}
        resp = JsonResponse(message, status=403)
        resp["X-Slack-No-Retry"] = 1
        return resp

    if "event" in slack_event:
        event_type = slack_event["event"]["type"]
        if slack_event["event"].get("username") != slackBot.name:
            request.session['bot_details']['botid'] = botid
            request.session['user_context'] = request.session.get(
                'user_context', {})
            return _event_handler(request, event_type, slack_event, botid)

    message = {'msg': "[NO EVENT IN SLACK REQUEST] These are not the droids\
                         you're looking for."}
    resp = HttpResponse(message, status=404)
    resp["X-Slack-No-Retry"] = 1
    return resp

@csrf_exempt
def status(request, botid):
    status = {'message': "Unable to find Slack Channel for bot.", 'status': False}
    s_status = True if request.POST.get('status') in ["True", "true"] else False
    bot = bot_details.objects.filter(bot_apikey=botid)
    if bot:
        bot = bot[0]
        slackbot = SlackAuth.objects.filter(bot=bot)
        if slackbot:
            slackbot[0].status = s_status
            slackbot[0].save()
            status['message'] = "Slack Channel status changed successfully."
            status['status'] = s_status
    if not s_status and botid in slack_bot.Bot.BOTS:
        del slack_bot.Bot.BOTS[botid]
    return JsonResponse(status)

def _event_handler(request, event_type, slack_event, botid=None):
    """
    A helper function that routes events from Slack to our Bot
    by event type and subtype.

    Parameters
    ----------
    event_type : str
        type of event recieved from Slack
    slack_event : dict
        JSON response from a Slack reaction event

    Returns
    ----------
    obj
        Response object with 200 - ok or 500 - No Event Handler error

    """
    team_id = slack_event["team_id"]
    if event_type == "message":
        text = slack_event["event"].get("text", "test")
        channel_id = slack_event["event"]["channel"]
        # Send the onboarding message
        slackBot = slack_bot.Bot.get_bot(botid)
        slackBot.chat_message(request, channel_id, team_id, text)
        resp = HttpResponse("copy message Sent")
        resp["X-Slack-No-Retry"] = 1
        return resp

    message = {
        'msg': "You have not added an event handler for the %s" % event_type}
    # Return a helpful error message
    resp = HttpResponse(message)
    resp["X-Slack-No-Retry"] = 1
    return resp
