from django.http import HttpResponse, JsonResponse
from django.shortcuts import render, redirect
from django.views.decorators.csrf import csrf_exempt
import json
from channels.bots import kik_bot
from django.urls import reverse
from channels.models import KikAuth
from channels.forms import KikAuthForm
from channels.utils import get_absolute_https_url
from bot_registration.models import bot_details


def install(request, botid):
    queryset = KikAuth.objects.filter(bot__bot_apikey=botid)
    if queryset:
        queryset = queryset[0]
    if request.POST:
        form_data = request.POST
        if queryset:
            form = KikAuthForm(request.POST, instance=queryset)
        else:
            form = KikAuthForm(request.POST)

        if form.is_valid():
            _auth = form.save(commit=False)
            _auth.bot = bot_details.objects.get(bot_apikey=botid)
            _auth.status = True
            _auth.save()
            _bot = kik_bot.Bot.get_bot(botid)
            targetUrl = get_absolute_https_url(request, 'kik_listen', botid)
            _bot.set_webhook(targetUrl)
            return redirect(reverse('channel_install', args=[botid]))

@csrf_exempt
def listen(request, botid):
    if request.method == 'POST':
        _bot = kik_bot.Bot.get_bot(botid)
        if not _bot.bot.verify_signature(
            request.META.get("HTTP_X_KIK_SIGNATURE"), request.body):
            return HttpResponse("Verification token mismatch or invalid request", status=403)

        request.session['bot_details']['botid'] = botid
        request.session['user_context'] = request.session.get(
            'user_context', {})
        _bot.chat_message(request, json.loads(request.body.decode()))
        return HttpResponse(status=200)
    else:
        return HttpResponse("{} method not valid".format(request.method), status=403)

@csrf_exempt
def status(request, botid=None):
    status = {'message': "Unable to find Kik Channel for bot.", 'status': False}
    s_status = True if request.POST.get('status') in ["True", "true"] else False
    bot = bot_details.objects.filter(bot_apikey=botid)
    if bot:
        bot = bot[0]
        _bot = KikAuth.objects.filter(bot=bot)
        if _bot:
            _bot[0].status = s_status
            _bot[0].save()
            status['message'] = "Kik Channel status changed successfully."
            status['status'] = s_status
        if not s_status and botid in kik_bot.Bot.BOTS:
            del kik_bot.Bot.BOTS[botid]
    return JsonResponse(status)
