# -*- coding: utf-8 -*-
"""
Python Kik Bot class for use with the Entellio app
"""
from brain.process_data import prcoess_question_text
from kik import KikApi, Configuration
from kik.messages import messages_from_json, TextMessage, TextResponse, \
    StartChattingMessage, SuggestedResponseKeyboard
from channels.models import KikAuth

class Bot(object):
    """ Instanciates a Bot object to handle Kik onboarding interactions."""
    BOTS = {}

    def __init__(self, name, botid, app_key):
        super(Bot, self).__init__()
        self.name = name
        self.botid = botid
        if app_key:
            try:
                self.bot = KikApi(name, app_key)
            except: # To do have to figure out exception
                self.bot = None

    @classmethod
    def get_bot(cls, botid):
        bot = cls.BOTS.get(botid)
        if bot: return bot
        # TRY to fetch from database
        try:
            bot_obj = KikAuth.objects.get(bot__bot_apikey=botid, status=True)
            bot = Bot(bot_obj.bot_name, botid, bot_obj.app_key)
            cls.BOTS[botid] = bot
            return bot
        except KikAuth.DoesNotExist:
            return None

    @classmethod
    def get_bot_queryset(cls, botid):
        queryset = KikAuth.objects.filter(bot__bot_apikey=botid)
        if queryset:
            queryset = queryset[0]
        else:
            queryset = None
        return queryset

    def chat_message(self, request, request_body):
        messages = messages_from_json(request_body["messages"])
        response_messages = []

        for message in messages:
            # Check if its the user's first message. Start Chatting messages are sent only once.
            if isinstance(message, StartChattingMessage):
                response_messages.append(TextMessage(
                    to=message.from_user,
                    chat_id=message.chat_id,
                    body="Hi, I am {} bot ".format(self.name),
                    keyboards=[SuggestedResponseKeyboard(responses=[TextResponse("Good"), TextResponse("Bad")])]))

            # Check if the user has sent a text message.
            elif isinstance(message, TextMessage):
                text = message.body.lower()
                userinfo = {} # to do
                set_bot_details_in_session(request, self.botid, text)
                set_user_details_in_session(request, userinfo)
                resp = prcoess_question_text(request, text)
                response_messages.append(TextMessage(
                    to=message.from_user,
                    chat_id=message.chat_id,
                    body=resp,
                    # keyboards are a great way to provide a menu of options for a user to respond with!
                    keyboards=[SuggestedResponseKeyboard(responses=[TextResponse("Good"), TextResponse("Bad")])]))

        resp_post = self.bot.send_messages(response_messages)
        return resp_post

    def set_webhook(self, url):
        if self.bot.get_configuration().webhook != url:
            self.bot.set_configuration(Configuration(webhook=url))
