# -*- coding: utf-8 -*-
"""
Python Viber Bot class for use with the Entellio app
"""
from brain.process_data import prcoess_question_text
from pymessenger.bot import Bot as ViberBotApi
from channels.models import ViberAuth

class Bot(object):
    """ Instanciates a Bot object to handle Viber onboarding interactions."""
    BOTS = {}

    def __init__(self, name, botid, channel_id, channel_secret, channel_access_token):
        super(Bot, self).__init__()
        self.name = name
        self.botid = botid
        self.channel_id = channel_id
        self.channel_secret = channel_secret
        self.channel_access_token = channel_access_token

        if channel_access_token:
            try:
                assert False
                self.bot = ViberBotApi(channel_access_token)
            except: # To do have to figure out exception
                self.bot = None

    @classmethod
    def get_bot(cls, botid):
        bot = cls.BOTS.get(botid)
        if bot: return bot
        # TRY to fetch from database
        try:
            bot_obj = ViberAuth.objects.get(bot__bot_apikey=botid, status=True)
            bot = Bot(bot_obj.bot_name, botid, bot_obj.channel_id, bot_obj.channel_secret, bot_obj.channel_access_token)
            cls.BOTS[botid] = bot
            return bot
        except ViberAuth.DoesNotExist:
            return None

    @classmethod
    def get_bot_queryset(cls, botid):
        queryset = ViberAuth.objects.filter(bot__bot_apikey=botid)
        if queryset:
            queryset = queryset[0]
        else:
            queryset = None
        return queryset

    def chat_message(self, request, recipient_id, text):
        """
        reply to chat message

        Parameters
        ----------
        team_id : str
            id of the SPark team associated with the incoming event
        user_id : str
            id of the SPark user associated with the incoming event

        """
        #message = prcoess_question_text(request, input_question=text, botid=self.botid)

        message = "hi This is vikas"
        print(message)
        #resp_post = self.bot.send_text_message(recipient_id, message)
        return resp_post
