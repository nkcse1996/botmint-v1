# -*- coding: utf-8 -*-
"""
Python Slack Bot class for use with the Entellio app
"""
from brain.process_data import prcoess_question_text
from slackclient import SlackClient
from channels.models import SlackAuth

class Bot(object):
    """ Instanciates a Bot object to handle Slack  message interactions."""
    BOTS = {}

    def __init__(self, name, botid, c_id, c_secret, verification, access_token=""):
        super(Bot, self).__init__()
        self.name = name
        self.botid = botid
        self.emoji = ":robot_face:"
        # When we instantiate a new bot object, we can access the app
        # credentials we set earlier in our local development environment.
        self.oauth = {"client_id": c_id,
                      "client_secret": c_secret,
                      # Scopes provide and limit permissions to what our app
                      # can access. It's important to use the most restricted
                      # scope that your app will need.
                      "scope": "bot"}
        self.verification = verification
        try:
            self.client = SlackClient(access_token)
        except: # to do  have to figure out exceptions
            self.client = None

    @classmethod
    def get_bot(cls, botid):
        bot = cls.BOTS.get(botid)
        if bot:
            return bot
        # TRY to fetch from database
        try:
            bot_obj = SlackAuth.objects.get(bot__bot_apikey=botid, status=True)
            bot = Bot(bot_obj.bot_name, botid, bot_obj.client_id,
                      bot_obj.client_secret,
                      bot_obj.verification_code, bot_obj.access_token, )
            cls.BOTS[botid] = bot
            return bot
        except SlackAuth.DoesNotExist:
            return None

    @classmethod
    def get_bot_queryset(cls, botid):
        queryset = SlackAuth.objects.filter(bot__bot_apikey=botid)
        if queryset:
            queryset = queryset[0]
        else:
            queryset = None
        return queryset

    def auth(self, code, botid):
        """
        Authenticate with OAuth and assign correct scopes.
        Save a dictionary of authed team information in memory on the bot
        object.

        Parameters
        ----------
        code : str
            temporary authorization code sent by Slack to be exchanged for an
            OAuth token

        """
        # After the user has authorized this app for use in their Slack team,
        # Slack returns a temporary authorization code that we'll exchange for
        # an OAuth token using the oauth.access endpoint
        auth_response = self.client.api_call(
            "oauth.access",
            client_id=self.oauth["client_id"],
            client_secret=self.oauth["client_secret"],
            code=code
        )
        # Then we'll reconnect to the Slack Client with the correct team's
        # bot token
        try:
            bot_obj = SlackAuth.objects.get(bot__bot_apikey=botid)
            bot_obj.access_token = auth_response["bot"]["bot_access_token"]
            bot_obj.save()
        except SlackAuth.DoesNotExist:
            pass

        self.client = SlackClient(auth_response["bot"]["bot_access_token"])

    def open_dm(self, user_id):
        """
        Open a DM to send a welcome message when a 'team_join' event is
        recieved from Slack.

        Parameters
        ----------
        user_id : str
            id of the Slack user associated with the 'team_join' event

        Returns
        ----------
        dm_id : str
            id of the DM channel opened by this method
        """
        new_dm = self.client.api_call("im.open",
                                      user=user_id)
        dm_id = new_dm["channel"]["id"]
        return dm_id

    def chat_message(self, request, channel_id, team_id, text):
        text = text[len(self.name):].strip() if text.lower().startswith(self.name.lower()) else text.strip()
        text = prcoess_question_text(request, input_question=text, botid=self.botid)
        post_message = self.client.api_call("chat.postMessage",
                                            channel=channel_id,
                                            text=text,
                                            )
