# -*- coding: utf-8 -*-
"""
Python Line Bot class for use with the Entellio app
"""
from brain.process_data import prcoess_question_text
from channels.models import LineAuth
from linebot import LineBotApi, WebhookParser
from linebot.exceptions import InvalidSignatureError, LineBotApiError
from linebot.models import MessageEvent, TextMessage, TextSendMessage


class Bot(object):
    """ Instanciates a Bot object to handle Line onboarding interactions."""
    BOTS = {}

    def __init__(self, name, botid, channel_id, channel_secret, channel_access_token):
        super(Bot, self).__init__()
        self.name = name
        self.botid = botid
        self.channel_id = channel_id
        if channel_access_token:
            try:
                self.bot = LineBotApi(channel_access_token)
            except:  # To do have to figure out exception
                self.bot = None

        if channel_secret:
            try:
                self.parser = WebhookParser(channel_secret)
            except:
                self.parser = None

    @classmethod
    def get_bot(cls, botid):
        bot = cls.BOTS.get(botid)
        if bot:
            return bot
        # TRY to fetch from database
        try:
            bot_obj = LineAuth.objects.get(bot__bot_apikey=botid, status=True)
            bot = Bot(bot_obj.bot_name, botid, bot_obj.channel_id,
                      bot_obj.channel_secret, bot_obj.channel_access_token)
            cls.BOTS[botid] = bot
            return bot
        except LineAuth.DoesNotExist:
            return None

    @classmethod
    def get_bot_queryset(cls, botid):
        queryset = LineAuth.objects.filter(bot__bot_apikey=botid)
        if queryset:
            queryset = queryset[0]
        else:
            queryset = None
        return queryset

    def chat_message(self, request, body, signature):
        """
        reply to chat message

        Parameters
        ----------
        body : str
            Message sent by Line Bot
        signature : str
            Signature validity to verify if message was sent from line only
        """
        try:
            events = self.parser.parse(body, signature)
        except InvalidSignatureError:
            return "forbidden"
        except LineBotApiError:
            return "badrequest"

        for event in events:
            if isinstance(event, MessageEvent):
                if isinstance(event.message, TextMessage):
                    userinfo = {} # to do
                    set_bot_details_in_session(request, self.botid, text)
                    set_user_details_in_session(request, userinfo)
                    message = prcoess_question_text(request, event.message.text)
                    self.bot.reply_message(
                        event.reply_token,
                        TextSendMessage(text=message)
                    )

        return "ok"
