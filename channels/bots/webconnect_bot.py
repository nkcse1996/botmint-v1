# -*- coding: utf-8 -*-
"""
Python Facebook Bot class for use with the Entellio app
"""
from brain.process_data import prcoess_question_text
from channels.models import WebConnectAuth

class Bot(object):
    """ Instanciates a Bot object to handle Facebook onboarding interactions."""
    BOTS = {}

    def __init__(self, botid,bot_user_name, bot_user_password, bot_customer_name,bot_file_name):
        super(Bot, self).__init__()
        self.bot = botid
        self.bot_user_name = bot_user_name
        self.bot_user_password = bot_user_password
        self.bot_customer_name = bot_customer_name
        self.bot_file_name = bot_file_name


    @classmethod
    def get_bot(cls, botid):
        print(botid)
        bot = cls.BOTS.get(botid)
        if bot: return bot
        # TRY to fetch from database
        try:
            bot_obj = WebConnectAuth.objects.get(bot__bot_apikey=botid, status=True)
            print(bot_obj)
            bot_file_name = str(botid[-6:])+"_"+str(bot_obj.bot_customer_name)+".zip"
            bot = Bot(botid,bot_obj.bot_user_name, bot_obj.bot_user_password, bot_obj.bot_customer_name,bot_file_name)
            cls.BOTS[botid] = bot
            print("***************:{}********************".format(bot.__dict__))
            return bot
        except WebConnectAuth.DoesNotExist:
            return None

    @classmethod
    def get_bot_folder(cls, botid):
        print(botid)
        bot = cls.BOTS.get(botid)
        if bot: return bot
        # TRY to fetch from database
        try:
            bot_obj = WebConnectAuth.objects.get(bot__bot_apikey=botid)
            print(bot_obj)
            bot_file_name = str(botid[-6:])+"_"+str(bot_obj.bot_customer_name)+".zip"
            bot = Bot(botid,bot_obj.bot_user_name, bot_obj.bot_user_password, bot_obj.bot_customer_name,bot_file_name)
            cls.BOTS[botid] = bot
            print("***************sdfsdf:{}********************".format(bot.__dict__))
            return bot
        except WebConnectAuth.DoesNotExist:
            return None

    @classmethod
    def get_bot_queryset(cls, botid):
        queryset = WebConnectAuth.objects.filter(bot__bot_apikey=botid)
        if queryset:
            queryset = queryset[0]
        else:
            queryset = None
        return queryset

    def chat_message(self, request, recipient_id, text):
        """
        reply to chat message

        Parameters
        ----------
        recipient_id : str
            id of the webconnect associated with the incoming event
        text : str
            text message from user

        """
        message = prcoess_question_text(request, input_question=text, botid=self.botid)
        resp_post = self.bot.send_text_message(recipient_id, message)
        return resp_post
