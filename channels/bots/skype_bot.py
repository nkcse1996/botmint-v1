# -*- coding: utf-8 -*-
"""
Python Skype Bot class for use with the Entellio app
"""
from brain.process_data import prcoess_question_text
from channels.api.skypeapi import SkypeBot
from channels.models import SkypeAuth

class Bot(object):
    """ Instanciates a Bot object to handle skype onboarding interactions."""
    BOTS = {}

    def __init__(self, name='entellio', botid=None, client_id=None, client_secret=None):
            super(Bot, self).__init__()
            self.name = name
            self.botid = botid
            try:
                self.bot = SkypeBot(client_id, client_secret)
            except: # To do have to figure out exception
                self.bot = None


    @classmethod
    def get_bot(cls, botid):
        bot = cls.BOTS.get(botid)
        if bot: return bot
        # TRY to fetch from database
        try:
            bot_obj = SkypeAuth.objects.get(bot__bot_apikey=botid, status=True)
            bot = Bot(bot_obj.bot_name, botid, bot_obj.app_id, bot_obj.app_password)
            cls.BOTS[botid] = bot
            return bot
        except SkypeAuth.DoesNotExist:
            return None

    def chat_message(self, request, service_url, conv_id, act_id, sender, text):
        """
        reply to chat message

        Parameters
        ----------
        team_id : str
            id of the skype team associated with the incoming event
        user_id : str
            id of the skype user associated with the incoming event
        """
        text = prcoess_question_text(request, input_question=text, botid=self.botid)
        resp_post = self.bot.send_message(service_url=service_url, conv_id=conv_id, act_id=act_id, sender=sender, text=text)
        return resp_post

    @classmethod
    def get_bot_queryset(cls, botid):
        queryset = SkypeAuth.objects.filter(bot__bot_apikey=botid)
        if queryset:
            queryset = queryset[0]
        else:
            queryset = None
        return queryset
