# -*- coding: utf-8 -*-
"""
Python SPark Bot class for use with the Entellio app
"""
from channels.modifier.spark import prcoess_question_spark
from ciscosparkapi import CiscoSparkAPI
from channels.models import SparkAuth

class Bot(object):
    """ Instanciates a Bot object to handle SPark onboarding interactions."""
    BOTS = {}

    def __init__(self, name, botid, email, client_token, bot_token):
        super(Bot, self).__init__()
        self.name = name
        self.botid = botid
        self.emoji = ":robot_face:"
        self.email = email
        self.client = None
        if client_token:
            self.client = CiscoSparkAPI(client_token)
        self.bot = None
        if bot_token:
            try:
                self.bot = CiscoSparkAPI(bot_token)
            except: # To do have to figure out exception
                self.bot = None

    @classmethod
    def get_bot(cls, botid):
        bot = cls.BOTS.get(botid)
        if bot: return bot
        # TRY to fetch from database
        try:
            bot_obj = SparkAuth.objects.get(bot__bot_apikey=botid, status=True)
            bot = Bot(bot_obj.bot_name, botid, bot_obj.bot_email,
                      bot_obj.admin_token, bot_obj.bot_token)
            cls.BOTS[botid] = bot
            return bot
        except SparkAuth.DoesNotExist:
            return None

    @classmethod
    def get_bot_queryset(cls, botid):
        queryset = SparkAuth.objects.filter(bot__bot_apikey=botid)
        if queryset:
            queryset = queryset[0]
        else:
            queryset = None
        return queryset

    def chat_message(self, request, room_id, text):
        """
        reply to chat message

        Parameters
        ----------
        team_id : str
            id of the SPark team associated with the incoming event
        user_id : str
            id of the SPark user associated with the incoming event

        """
        text = text[len(self.name):].strip() if text.lower().startswith(self.name.lower()) else text.strip()
        text = prcoess_question_spark(request, input_question=text, botid=self.botid)
        resp_post = self.bot.messages.create(roomId=room_id, text=text)
        return resp_post

    def set_webhook(self, botid, targetUrl):
        bot_query = list(self.client.people.list(email=self.email))
        if bot_query:
            bot = bot_query[0]
        else:
            return "Please enter Bot in cisco spark first"

        clean_webhooks(self, botid)

        resp = self.client.webhooks.create(
        name=botid + " entellio direct",
        targetUrl=targetUrl,
        resource="messages",
        event="created",
        filter="roomType=direct"
        )
        print("Direct message webhook added")
        print(resp)

        resp = self.client.webhooks.create(
        name=botid + " entellio group",
        targetUrl=targetUrl,
        resource="messages",
        event="created",
        filter="mentionedPeople={}".format(bot.id)
        )
        print("group message webhook added")
        return "Succesfully created webhooks"

def clean_webhooks(self, botid):
    webhooks = self.client.webhooks.list()
    for hook in webhooks:
        if hook.name in [botid + " entellio direct", botid + " entellio group"]:
            try:
                self.client.webhooks.delete(webhookId=hook.id)
            except:
                pass # TO DO return 204 on success
