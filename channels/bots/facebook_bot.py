# -*- coding: utf-8 -*-
"""
Python Facebook Bot class for use with the Entellio app
"""
from brain.process_data import prcoess_question_text
from pymessenger.bot import Bot as FacebookBotApi
from channels.models import FacebookAuth
from brain.utils import set_bot_details_in_session, \
set_user_details_in_session

class Bot(object):
    """ Instanciates a Bot object to handle Facebook onboarding interactions."""
    BOTS = {}

    def __init__(self, name, botid, verify_token, page_access_token):
        super(Bot, self).__init__()
        self.name = name
        self.botid = botid
        self.verify_token = verify_token
        if page_access_token:
            try:
                self.bot = FacebookBotApi(page_access_token)
            except: # To do have to figure out exception
                self.bot = None

    @classmethod
    def get_bot(cls, botid):
        bot = cls.BOTS.get(botid)
        if bot: return bot
        # TRY to fetch from database
        try:
            bot_obj = FacebookAuth.objects.get(bot__bot_apikey=botid, status=True)
            bot = Bot(bot_obj.bot_name, botid, bot_obj.verify_token, bot_obj.page_access_token)
            cls.BOTS[botid] = bot
            return bot
        except FacebookAuth.DoesNotExist:
            return None

    @classmethod
    def get_bot_queryset(cls, botid):
        queryset = FacebookAuth.objects.filter(bot__bot_apikey=botid)
        if queryset:
            queryset = queryset[0]
        else:
            queryset = None
        return queryset

    def chat_message(self, request, recipient_id, text):
        """
        reply to chat message

        Parameters
        ----------
        recipient_id : str
            id of the Facebook associated with the incoming event
        text : str
            text message from user

        """
        userinfo = {} # to do
        set_bot_details_in_session(request, self.botid, text)
        set_user_details_in_session(request, userinfo)
        message = prcoess_question_text(request, input_question=text)
        resp_post = self.bot.send_text_message(recipient_id, message)
        return resp_post
