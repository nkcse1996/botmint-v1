# -*- coding: utf-8 -*-
"""
Python Twitter Bot class for use with the Entellio app
"""
from brain.process_data import prcoess_question_text
import tweepy as TwitterBotApi
from channels.models import TwitterAuth
import json


class Bot(TwitterBotApi.StreamListener):
    """ Instanciates a Bot object to handle Twitter onboarding interactions."""
    BOTS = {}

    def __init__(self, name, botid, consumer_key,
                 consumer_secret, access_token, access_token_secret, *args, **kwargs):
        super(Bot, self).__init__(*args, **kwargs)
        self.name = name
        self.botid = botid
        self.stream = None
        if consumer_key and consumer_secret and access_token and access_token_secret:
            try:
                self.auth = TwitterBotApi.OAuthHandler(consumer_key, consumer_secret)
                self.auth.set_access_token(access_token, access_token_secret)
                self.bot = TwitterBotApi.API(self.auth)
                self.id = self.bot.me().id_str
            except:  # To do have to figure out exception
                self.bot = None
                self.id = None
                self.auth= None

    @classmethod
    def get_bot(cls, botid):
        bot = cls.BOTS.get(botid)
        if bot:
            return bot
        # TRY to fetch from database
        try:
            bot_obj = TwitterAuth.objects.get(
                bot__bot_apikey=botid, status=True)
            bot = Bot(bot_obj.bot_name, botid, bot_obj.consumer_key,
                      bot_obj.consumer_secret,
                      bot_obj.access_token,
                      bot_obj.access_token_secret)
            cls.BOTS[botid] = bot
            return bot
        except TwitterAuth.DoesNotExist:
            return None

    @classmethod
    def get_bot_queryset(cls, botid):
        queryset = TwitterAuth.objects.filter(bot__bot_apikey=botid)
        if queryset:
            queryset = queryset[0]
        else:
            queryset = None
        return queryset

    def chat_message(self, request):
        self.request = request
        self.stream = TwitterBotApi.Stream(self.auth, self)
        self.stream.userstream(_with='user')

    def disconnect(self):
        self.stream.disconnect()

    def on_data(self, data):
        print(data)
        tweet = json.loads(data.strip())

        retweeted = tweet.get('retweeted')
        from_self = tweet.get('user',{}).get('id_str', '') == self.id

        if retweeted is not None and not retweeted and not from_self:

            tweetId = tweet.get('id_str')
            screenName = tweet.get('user',{}).get('screen_name')
            text = tweet.get('text')

            text = text[len(self.name):].strip() if text.lower().startswith(self.name.lower()) else text.strip()
            response = prcoess_question_text(self.request, input_question=text, botid=self.botid)

            replyText = '@' + screenName + ' ' + response

            #check if repsonse is over 140 char
            if len(replyText) > 140:
                replyText = replyText[0:139] + '…'

            # If rate limited, the status posts should be queued up and sent on an interval
            self.bot.update_status(status=replyText, in_reply_to_status_id=tweetId)

    def on_error(self, status):
        print(status)
