# -*- coding: utf-8 -*-
"""
Python Telegram Bot class for use with the Entellio app
"""
from brain.process_data import prcoess_question_text
from channels.models import TelegramAuth
from telegram.ext import CommandHandler
from telegram.ext import MessageHandler, Filters
from telegram.ext import Updater as TelegramBotApi

class Bot(object):
    """ Instanciates a Bot object to handle Telegram onboarding interactions."""
    BOTS = {}

    def __init__(self, name, botid, token):
        super(Bot, self).__init__()
        self.name = name
        self.botid = botid
        if token:
            try:
                self.bot = TelegramBotApi(token=token)
            except: # To do have to figure out exception
                self.bot = None

    @classmethod
    def get_bot(cls, botid):
        bot = cls.BOTS.get(botid)
        if bot: return bot
        # TRY to fetch from database
        try:
            bot_obj = TelegramAuth.objects.get(bot__bot_apikey=botid, status=True)
            bot = Bot(bot_obj.bot_name, botid, bot_obj.token)
            cls.BOTS[botid] = bot
            return bot
        except TelegramAuth.DoesNotExist:
            return None

    @classmethod
    def get_bot_queryset(cls, botid):
        queryset = TelegramAuth.objects.filter(bot__bot_apikey=botid)
        if queryset:
            queryset = queryset[0]
        else:
            queryset = None
        return queryset

    def chat_message(self, request):
        self.request = request
        intro_handler = CommandHandler('start', self.intro)
        reply_handler = MessageHandler([Filters.text], self.reply)
        self.bot.dispatcher.add_handler(intro_handler)
        self.bot.dispatcher.add_handler(reply_handler)
        self.bot.start_polling()

    def disconnect(self):
        self.bot.stop()

    def intro(self, bot, update):
        bot.sendMessage(chat_id=update.message.chat_id,
                    text="Hi. I am bot and my name is {}".format(self.name))

    def reply(self, bot, update):
        try:
            text = update.message.text
            response = prcoess_question_text(self.request, input_question=text, botid=self.botid)
            bot.sendMessage(chat_id=update.message.chat_id, text=response)
        except UnicodeEncodeError:
            bot.sendMessage(chat_id=update.message.chat_id,
                        text="Sorry, but I can't understand your text.")
