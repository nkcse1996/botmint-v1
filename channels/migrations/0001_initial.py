# -*- coding: utf-8 -*-
# Generated by Django 1.11.2 on 2018-03-15 07:16
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        ('bot_registration', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='FacebookAuth',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('bot_name', models.CharField(default='', max_length=128)),
                ('verify_token', models.CharField(default='', max_length=128)),
                ('page_access_token', models.CharField(default='', max_length=256)),
                ('status', models.BooleanField(default=False)),
                ('bot', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='bot_registration.bot_details')),
            ],
            options={
                'db_table': 'FacebookAuth',
            },
        ),
        migrations.CreateModel(
            name='KikAuth',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('bot_name', models.CharField(default='', max_length=128)),
                ('app_key', models.CharField(default='', max_length=128)),
                ('status', models.BooleanField(default=False)),
                ('bot', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='bot_registration.bot_details')),
            ],
            options={
                'db_table': 'KikAuth',
            },
        ),
        migrations.CreateModel(
            name='LineAuth',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('bot_name', models.CharField(default='', max_length=128)),
                ('channel_id', models.CharField(default='', max_length=128)),
                ('channel_secret', models.CharField(default='', max_length=128)),
                ('channel_access_token', models.CharField(default='', max_length=256)),
                ('status', models.BooleanField(default=False)),
                ('bot', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='bot_registration.bot_details')),
            ],
            options={
                'db_table': 'LineAuth',
            },
        ),
        migrations.CreateModel(
            name='SkypeAuth',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('bot_name', models.CharField(default='', max_length=128)),
                ('app_id', models.CharField(default='', max_length=128)),
                ('app_password', models.CharField(default='', max_length=256)),
                ('status', models.BooleanField(default=False)),
                ('bot', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='bot_registration.bot_details')),
            ],
            options={
                'db_table': 'SkypeAuth',
            },
        ),
        migrations.CreateModel(
            name='SlackAuth',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('bot_name', models.CharField(default='', max_length=128)),
                ('client_id', models.CharField(default='', max_length=128)),
                ('client_secret', models.CharField(default='', max_length=256)),
                ('verification_code', models.CharField(default='', max_length=128)),
                ('access_token', models.CharField(default='', max_length=256)),
                ('status', models.BooleanField(default=False)),
                ('bot', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='bot_registration.bot_details')),
            ],
            options={
                'db_table': 'SlackAuth',
            },
        ),
        migrations.CreateModel(
            name='SparkAuth',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('bot_name', models.CharField(default='', max_length=128)),
                ('bot_email', models.CharField(default='', max_length=128)),
                ('admin_token', models.CharField(default='', max_length=256)),
                ('bot_token', models.CharField(default='', max_length=256)),
                ('status', models.BooleanField(default=False)),
                ('bot', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='bot_registration.bot_details')),
            ],
            options={
                'db_table': 'SparkAuth',
            },
        ),
        migrations.CreateModel(
            name='TelegramAuth',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('bot_name', models.CharField(default='', max_length=128)),
                ('token', models.CharField(default='', max_length=128)),
                ('status', models.BooleanField(default=False)),
                ('bot', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='bot_registration.bot_details')),
            ],
            options={
                'db_table': 'TelegramAuth',
            },
        ),
        migrations.CreateModel(
            name='TwitterAuth',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('bot_name', models.CharField(default='', max_length=128)),
                ('consumer_key', models.CharField(default='', max_length=128)),
                ('consumer_secret', models.CharField(default='', max_length=128)),
                ('access_token', models.CharField(default='', max_length=128)),
                ('access_token_secret', models.CharField(default='', max_length=129)),
                ('status', models.BooleanField(default=False)),
                ('bot', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='bot_registration.bot_details')),
            ],
            options={
                'db_table': 'TwitterAuth',
            },
        ),
        migrations.CreateModel(
            name='ViberAuth',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('bot_name', models.CharField(default='', max_length=128)),
                ('app_key', models.CharField(default='', max_length=128)),
                ('avatar_url', models.CharField(default='', max_length=256)),
                ('status', models.BooleanField(default=False)),
                ('bot', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='bot_registration.bot_details')),
            ],
            options={
                'db_table': 'ViberAuth',
            },
        ),
    ]
