from django.contrib import admin

from .models import SlackAuth, SparkAuth, SkypeAuth, FacebookAuth, LineAuth, \
        TwitterAuth, ViberAuth, KikAuth, TelegramAuth, WebConnectAuth
admin.site.register(SlackAuth)
admin.site.register(SparkAuth)
admin.site.register(SkypeAuth)
admin.site.register(FacebookAuth)
admin.site.register(LineAuth)
admin.site.register(TwitterAuth)
admin.site.register(ViberAuth)
admin.site.register(KikAuth)
admin.site.register(TelegramAuth)
admin.site.register(WebConnectAuth)
