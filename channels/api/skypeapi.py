# -*- coding: utf-8 -*
import requests
import urllib
import time
from fwk.utils import log_error_msg
BASE_URL = 'https://login.microsoftonline.com/botframework.com/oauth2/v2.0/token'


class SkypeBot:
    def __init__(self, client_id, client_secret):
        self.client_id = client_id
        self.client_secret = client_secret
        self.time = 0
        self.expires_in = 0
        self.token = None
        self.set_token()

    def set_token(self):
        if time.time() - self.time > self.expires_in:
            payload = {"grant_type": "client_credentials",
                       "client_id": self.client_id,
                       "client_secret": self.client_secret,
                       "scope": "https://api.botframework.com/.default"
                       }
            header = {"Content-Type": "application/x-www-form-urlencoded"}
            payload = urllib.parse.urlencode(payload)
            response = requests.post(BASE_URL, data=payload, headers=header)
            data = response.json()
            self.token = data["access_token"]
            self.expires_in = data["expires_in"]
            self.time = time.time()
        # we can add async call here to set the tokem in background

    def send_message(self, service_url, conv_id, act_id, sender, text):
        # This can be send using worker to improve site performance
        self.set_token()
        try:
            payload = {'type': 'message',
                       'from': {'id': sender},
                       'text': text
                       }
            post_url = '{service_url}/v3/conversations/{conv_id}/activities/'.format(
                service_url=service_url, conv_id=conv_id)
            if act_id:
                post_url += act_id
            headers = {"Authorization": "Bearer " +
                       self.token, "Content-Type": "application/json"}
            return requests.post(post_url, headers=headers, json=payload)

        except Exception as e:
            log_error_msg("send_message", str(e))
            return {'status': str(e)}
