from django.urls import reverse
from django.conf import settings

def get_absolute_https_url(request, url_name, botid):
    url = request.build_absolute_uri(
        reverse(url_name, args=(botid,)))
    if settings.CHANNELS_FORCED_HTTPS:
        url = url.replace("http://", "https://")
    return url
