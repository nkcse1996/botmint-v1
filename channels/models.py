from django.db import models
from bot_registration.models import bot_details
# Create your models here.

class SlackAuth(models.Model):
    bot=models.ForeignKey(bot_details, on_delete=models.CASCADE)
    bot_name=models.CharField(max_length=128, default="")
    client_id =models.CharField(max_length=128, default="")
    client_secret=models.CharField(max_length=256, default="")
    verification_code=models.CharField(max_length=128, default="")
    access_token = models.CharField(max_length=256, default="")
    status = models.BooleanField(default=False)
    class Meta:
        db_table = u'SlackAuth'


class SkypeAuth(models.Model):
    bot=models.ForeignKey(bot_details, on_delete=models.CASCADE)
    bot_name=models.CharField(max_length=128, default="")
    app_id =models.CharField(max_length=128, default="")
    app_password=models.CharField(max_length=256, default="")
    status = models.BooleanField(default=False)
    class Meta:
        db_table = u'SkypeAuth'


class SparkAuth(models.Model):
    bot=models.ForeignKey(bot_details, on_delete=models.CASCADE)
    bot_name=models.CharField(max_length=128, default="")
    bot_email=models.CharField(max_length=128, default="")
    admin_token =models.CharField(max_length=256, default="")
    bot_token=models.CharField(max_length=256, default="")
    status = models.BooleanField(default=False)

    class Meta:
        db_table = u'SparkAuth'


class FacebookAuth(models.Model):
    bot=models.ForeignKey(bot_details, on_delete=models.CASCADE)
    bot_name=models.CharField(max_length=128, default="")
    verify_token =models.CharField(max_length=128, default="")
    page_access_token=models.CharField(max_length=256, default="")
    status = models.BooleanField(default=False)
    class Meta:
        db_table = u'FacebookAuth'

class LineAuth(models.Model):
    bot=models.ForeignKey(bot_details, on_delete=models.CASCADE)
    bot_name=models.CharField(max_length=128, default="")
    channel_id=models.CharField(max_length=128, default="")
    channel_secret=models.CharField(max_length=128, default="")
    channel_access_token=models.CharField(max_length=256, default="")
    status = models.BooleanField(default=False)
    class Meta:
        db_table = u'LineAuth'

class TwitterAuth(models.Model):
    bot=models.ForeignKey(bot_details, on_delete=models.CASCADE)
    bot_name=models.CharField(max_length=128, default="")
    consumer_key=models.CharField(max_length=128, default="")
    consumer_secret=models.CharField(max_length=128, default="")
    access_token=models.CharField(max_length=128, default="")
    access_token_secret=models.CharField(max_length=129, default="")
    status = models.BooleanField(default=False)
    class Meta:
        db_table = u'TwitterAuth'

class ViberAuth(models.Model):
    bot=models.ForeignKey(bot_details, on_delete=models.CASCADE)
    bot_name=models.CharField(max_length=128, default="")
    app_key=models.CharField(max_length=128, default="")
    avatar_url=models.CharField(max_length=256, default="")
    status=models.BooleanField(default=False)
    class Meta:
        db_table = u'ViberAuth'

class KikAuth(models.Model):
    bot=models.ForeignKey(bot_details, on_delete=models.CASCADE)
    bot_name=models.CharField(max_length=128, default="")
    app_key=models.CharField(max_length=128, default="")
    status = models.BooleanField(default=False)
    class Meta:
        db_table = u'KikAuth'

class TelegramAuth(models.Model):
    bot=models.ForeignKey(bot_details, on_delete=models.CASCADE)
    bot_name=models.CharField(max_length=128, default="")
    token=models.CharField(max_length=128, default="")
    status = models.BooleanField(default=False)
    class Meta:
        db_table = u'TelegramAuth'


class WebConnectAuth(models.Model):
    bot = models.ForeignKey(bot_details, on_delete=models.CASCADE)
    bot_user_name = models.CharField(max_length=128)
    bot_user_password = models.CharField(max_length=128)
    bot_customer_name = models.CharField(max_length=128)
    status = models.BooleanField(default=False)

    def as_json(self):
        return dict(
            CUSTOMER_BOT_ID = "'{}'".format(self.bot.bot_apikey),
            CUSTOMER_BOT_NAME="'{}'".format(self.bot_user_name),
            CUSTOMER_PASSWORD="'{}'".format(self.bot_user_password),
            BOT_CUSTOMER_NAME="'{}'".format(self.bot_customer_name))


    class Meta:
        db_table = u'WebConnectAuth'
