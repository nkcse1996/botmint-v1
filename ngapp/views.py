from django.shortcuts import render
from registration.models import Bot

# Create your views here.


def home(request):
    bots = Bot.objects.filter(admin=request.user)
    bots_data = []
    for bot in bots:
        b = {
            "id": bot.id,
            "displayName": bot.name,
            "avatar": request.build_absolute_uri("/")[:-1] + bot.image.url,
            "status": "Online" if bot.status else "Offline"
        }
        bots_data.append(b)

    return render(request, "ngchat.html", {'bots': bots_data})
