from django.conf.urls import url
from .views import home

app_name = "ngapp"
urlpatterns = [
    url(r'^chat/$', home, name='home'),
    ]
