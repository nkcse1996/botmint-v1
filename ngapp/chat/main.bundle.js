webpackJsonp(["main"],{

/***/ "./src/$$_lazy_route_resource lazy recursive":
/***/ (function(module, exports) {

function webpackEmptyAsyncContext(req) {
	// Here Promise.resolve().then() is used instead of new Promise() to prevent
	// uncatched exception popping up in devtools
	return Promise.resolve().then(function() {
		throw new Error("Cannot find module '" + req + "'.");
	});
}
webpackEmptyAsyncContext.keys = function() { return []; };
webpackEmptyAsyncContext.resolve = webpackEmptyAsyncContext;
module.exports = webpackEmptyAsyncContext;
webpackEmptyAsyncContext.id = "./src/$$_lazy_route_resource lazy recursive";

/***/ }),

/***/ "./src/app/app.component.css":
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/app.component.html":
/***/ (function(module, exports) {

module.exports = "<app-chat [bots]=\"bots\" [user_image]=\"user_image\"></app-chat>\n"

/***/ }),

/***/ "./src/app/app.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AppComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};

var AppComponent = /** @class */ (function () {
    function AppComponent() {
    }
    AppComponent.prototype.ngOnInit = function () {
        this.bots = window['bot_info'] ? window['bot_info']['data'] : [];
        this.user_image = window['user_image'];
    };
    AppComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
            selector: 'app-root',
            template: __webpack_require__("./src/app/app.component.html"),
            styles: [__webpack_require__("./src/app/app.component.css")]
        })
    ], AppComponent);
    return AppComponent;
}());



/***/ }),

/***/ "./src/app/app.module.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AppModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser__ = __webpack_require__("./node_modules/@angular/platform-browser/esm5/platform-browser.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__app_component__ = __webpack_require__("./src/app/app.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__details_form_details_form_component__ = __webpack_require__("./src/app/details-form/details-form.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__chat_chat_component__ = __webpack_require__("./src/app/chat/chat.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__angular_router__ = __webpack_require__("./node_modules/@angular/router/esm5/router.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__angular_forms__ = __webpack_require__("./node_modules/@angular/forms/esm5/forms.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__angular_common_http__ = __webpack_require__("./node_modules/@angular/common/esm5/http.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__myservice__ = __webpack_require__("./src/app/myservice.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__full_chat_full_chat_component__ = __webpack_require__("./src/app/full-chat/full-chat.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__html_message_html_message_component__ = __webpack_require__("./src/app/html-message/html-message.component.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};











var AppModule = /** @class */ (function () {
    function AppModule() {
    }
    AppModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["I" /* NgModule */])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__app_component__["a" /* AppComponent */],
                __WEBPACK_IMPORTED_MODULE_3__details_form_details_form_component__["a" /* DetailsFormComponent */],
                __WEBPACK_IMPORTED_MODULE_4__chat_chat_component__["a" /* ChatComponent */],
                __WEBPACK_IMPORTED_MODULE_9__full_chat_full_chat_component__["a" /* FullChatComponent */],
                __WEBPACK_IMPORTED_MODULE_10__html_message_html_message_component__["a" /* HtmlMessageComponent */]
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser__["a" /* BrowserModule */],
                __WEBPACK_IMPORTED_MODULE_6__angular_forms__["a" /* FormsModule */],
                __WEBPACK_IMPORTED_MODULE_7__angular_common_http__["b" /* HttpClientModule */],
                __WEBPACK_IMPORTED_MODULE_5__angular_router__["b" /* RouterModule */].forRoot([
                    {
                        path: 'detailsform',
                        component: __WEBPACK_IMPORTED_MODULE_3__details_form_details_form_component__["a" /* DetailsFormComponent */]
                    },
                    {
                        path: 'fullchat',
                        component: __WEBPACK_IMPORTED_MODULE_9__full_chat_full_chat_component__["a" /* FullChatComponent */]
                    },
                    {
                        path: '',
                        component: __WEBPACK_IMPORTED_MODULE_4__chat_chat_component__["a" /* ChatComponent */]
                    }
                ])
            ],
            providers: [__WEBPACK_IMPORTED_MODULE_8__myservice__["a" /* ConfigService */]],
            bootstrap: [__WEBPACK_IMPORTED_MODULE_2__app_component__["a" /* AppComponent */]]
        })
    ], AppModule);
    return AppModule;
}());



/***/ }),

/***/ "./src/app/botsData/bot-message.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return BotMessage; });
var BotMessage = /** @class */ (function () {
    function BotMessage() {
    }
    return BotMessage;
}());



/***/ }),

/***/ "./src/app/botsData/data.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return botsData; });
var botsData = [
    {
        id: 1,
        displayName: "Arya Stark",
        avatar: "https://pbs.twimg.com/profile_images/894833370299084800/dXWuVSIb.jpg",
        status: "Online"
    },
    {
        id: 2,
        displayName: "Daenerys Targaryen",
        avatar: "https://68.media.tumblr.com/avatar_d28d7149f567_128.png",
        status: "Busy"
    },
    {
        id: 3,
        displayName: "Eddard Stark",
        avatar: "https://pbs.twimg.com/profile_images/600707945911844864/MNogF757_400x400.jpg",
        status: "Offline"
    },
    {
        id: 4,
        displayName: "Hodor",
        avatar: "https://pbs.twimg.com/profile_images/378800000449071678/27f2e27edd119a7133110f8635f2c130.jpeg",
        status: "Offline"
    },
    {
        id: 5,
        displayName: "Jaime Lannister",
        avatar: "https://pbs.twimg.com/profile_images/378800000243930208/4fa8efadb63777ead29046d822606a57.jpeg",
        status: "Busy"
    },
    {
        id: 6,
        displayName: "John Snow",
        avatar: "https://pbs.twimg.com/profile_images/3456602315/aad436e6fab77ef4098c7a5b86cac8e3.jpeg",
        status: " Busy"
    },
    {
        id: 7,
        displayName: "Lorde Petyr 'Littlefinger' Baelish",
        avatar: "http://68.media.tumblr.com/avatar_ba75cbb26da7_128.png",
        status: "Offline"
    },
    {
        id: 8,
        displayName: "Sansa Stark",
        avatar: "http://pm1.narvii.com/6201/dfe7ad75cd32130a5c844d58315cbca02fe5b804_128.jpg",
        status: "Online"
    },
    {
        id: 9,
        displayName: "Theon Greyjoy",
        avatar: "https://thumbnail.myheritageimages.com/502/323/78502323/000/000114_884889c3n33qfe004v5024_C_64x64C.jpg",
        status: "Away"
    }
];


/***/ }),

/***/ "./src/app/botsData/message.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return Message; });
var Message = /** @class */ (function () {
    function Message() {
    }
    return Message;
}());



/***/ }),

/***/ "./src/app/botsData/user-status.enum.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return UserStatus; });
var UserStatus;
(function (UserStatus) {
    UserStatus[UserStatus["Online"] = 0] = "Online";
    UserStatus[UserStatus["Busy"] = 1] = "Busy";
    UserStatus[UserStatus["Away"] = 2] = "Away";
    UserStatus[UserStatus["Offline"] = 3] = "Offline";
})(UserStatus || (UserStatus = {}));


/***/ }),

/***/ "./src/app/chat/chat.component.css":
/***/ (function(module, exports) {

module.exports = "#ng-chat {\n  position: fixed;\n  z-index: 999;\n  right: 0;\n  bottom: 0;\n  color: #5C5C5C;\n  font-family: Arial, Helvetica, sans-serif;\n  -webkit-box-sizing: initial;\n          box-sizing: initial;\n  font-size: 11pt;\n  text-align: left;\n}\n\n.ng-chat-loading-wrapper {\n  height: 30px;\n  text-align: center;\n  font-size: 0.9em;\n}\n\n#ng-chat-people {\n    position: absolute;\n    width: 240px;\n    float:right;\n    height: 360px;\n    bottom: 0px;\n    right: 0px;\n    border: 2px solid #38414c;\n    margin-right: 20px;\n    border-top-right-radius: 25px;\n\n    border-top-left-radius: 12px;\n    background-color: #B0BEC5;\n    -webkit-box-shadow: 0 4px 8px rgba(0,0,0,.25);\n    box-shadow: 0 4px 8px rgba(0,0,0,.25);\n    border-bottom: 0;\n    border-bottom-width: 0px;\n    border-bottom-style: initial;\n    border-bottom-color: initial;\n    margin-bottom: auto;\n}\n\n#ng-chat-people-content{\n  position: absolute;\nbottom: 0;\nleft: 0;\n}\n\n#ng-chat-people.ng-chat-people-collapsed {\n  height: 30px;\n\n}\n\n.ng-chat-close {\n  color: #5C5C5C;\n  text-decoration: none;\n  float: right;\n}\n\n.ng-chat-title, .ng-chat-title:hover {\n  position: relative;\n  z-index: 2;\n  height: 38px;\n  line-height: 30px;\n  font-size: 1.13em;\n  padding: 0 10px;\n  border-radius:5px;\n  border-bottom-left-radius: 20px;\n  border-top-right-radius: 20px;\n  background-color: #38414c;\n  display: block;\n  text-decoration: none;\n  color: inherit;\n  font-weight: 400;\n  cursor: pointer;\n  /* background-color:#FFF; */\n}\n\n.ng-chat-title.shadowed {\n  -webkit-box-shadow: 0 4px 8px rgba(0, 0, 0, .25);\n          box-shadow: 0 4px 8px rgba(0, 0, 0, .25);\n}\n\n.ng-chat-title>strong {\n  font-weight: 600;\n  display: block;\n  overflow: hidden;\n  height: 30px;\n  text-overflow: ellipsis;\n  white-space: nowrap;\n  max-width: 85%;\n  float: left;\n}\n\n.ng-chat-title>.ng-chat-user-status {\n  float: left;\n  margin-left: 5px;\n}\n\n#ng-chat-search_friend {\n  display: block;\n  padding: 7px 10px;\n  margin: 0 auto;\n  width: calc(100% - 20px);\n  margin-top: 10px;\n  font-size: 0.9em;\n  -webkit-appearance: searchfield;\n}\n\n#ng-chat-users {\n  padding: 0 10px;\n  list-style: none;\n  margin: 0;\n  overflow: auto;\n  position: absolute;\n  top: 50px;\n  bottom: 0;\n  width: 100%;\n  -webkit-box-sizing: border-box;\n          box-sizing: border-box;\n}\n\n#ng-chat-users li {\n  clear: both;\n  margin-bottom: 10px;\n  overflow: hidden;\n  cursor: pointer;\n  max-height: 30px;\n}\n\n#ng-chat-users li>img, #ng-chat-users li>.icon-wrapper {\n  float: left;\n  margin-right: 5px;\n  border-radius: 25px;\n}\n\n#ng-chat-users li>.icon-wrapper {\n  background-color: #BABABA;\n  overflow: hidden;\n  width: 30px;\n  height: 30px;\n}\n\n#ng-chat-users li>.icon-wrapper>i {\n  color: #FFF;\n  -webkit-transform: scale(0.7);\n          transform: scale(0.7);\n}\n\n#ng-chat-users li>strong {\n  float: left;\n  line-height: 30px;\n  font-size: 0.8em;\n  max-width: 57%;\n  max-height: 30px;\n  overflow: hidden;\n  white-space: nowrap;\n  text-overflow: ellipsis;\n  margin-left: 40px;\n}\n\n#ng-chat-users li>.ng-chat-user-status {\n  float: right;\n}\n\n.ng-chat-user-status {\n  border-radius: 25px;\n  width: 8px;\n  height: 8px;\n  margin-top: 10px;\n}\n\n.ng-chat-user-status.online {\n  background-color: #92A400;\n}\n\n.ng-chat-user-status.busy {\n  background-color: #F91C1E;\n}\n\n.ng-chat-user-status.away {\n  background-color: #F7D21B;\n}\n\n.ng-chat-user-status.offline {\n  background-color: #BABABA;\n}\n\n.ng-chat-unread-messages-count {\n  background-color: #E3E3E3;\n  margin-left: 5px;\n  padding: 0 5px;\n  border-radius: 25px;\n  font-size: 0.9em;\n  color: #5C5C5C;\n  line-height: 30px;\n}\n\n.ng-chat-window {\n  right: 260px;\n  height: 360px;\n  z-index: 999;\n  bottom: 0;\n  position: fixed;\n  width: 300px;\n  border: 1.5px solid #38414c;\n  border-bottom: 0;\n  border-top-right-radius: 25px;\n\n  border-top-left-radius: 12px;\n  -webkit-box-shadow: 0 4px 8px rgba(0, 0, 0, .25);\n          box-shadow: 0 4px 8px rgba(0, 0, 0, .25);\n}\n\n.ng-chat-window-collapsed {\n  height: 30px !important;\n}\n\n.ng-chat-window>input {\n  font-size: 1.2em;\n  -webkit-box-sizing: border-box;\n          box-sizing: border-box;\n  padding: 0 5px;\n  display: block;\n\n  height: calc(10%);\n  width: 100%;\n  border: none;\n  border-top: 1px solid #A3A3A3;\n}\n\n.ng-chat-window .ng-chat-messages {\n  padding: 10px;\n  height: calc(90% - 40px);\n  -webkit-box-sizing: border-box;\n          box-sizing: border-box;\n  position: relative;\n  overflow: auto;\n  background-color: #FFF;\n}\n\n.ng-chat-window .ng-chat-messages .ng-chat-message {\n  clear: both;\n}\n\nimg.avatar, .ng-chat-window .ng-chat-messages .ng-chat-message>.icon-wrapper {\n  position: absolute;\n  left: 10px;\n  border-radius: 25px;\n}\n\nimg#user_base_avatar {\n  position: absolute;\n  right: 10px;\n  border-radius: 25px;\n}\n\n.ng-chat-window .ng-chat-messages .ng-chat-message>.icon-wrapper,\n.ng-chat-window .ng-chat-messages .ng-chat-message>.user-icon-wrapper {\n  background-color: #80CBC4;\n  overflow: hidden;\n  width: 30px;\n  height: 30px;\n\n}\n\n.ng-chat-window .ng-chat-messages .ng-chat-message>.icon-wrapper>i {\n  color: #FFF;\n  -webkit-transform: scale(0.7);\n          transform: scale(0.7);\n}\n\n/*\n/////////////cards/////////// */\n\n.card {\n    /* Add shadows to create the \"card\" effect */\n    -webkit-box-shadow: 0 4px 8px 0 rgba(0,0,0,0.2);\n            box-shadow: 0 4px 8px 0 rgba(0,0,0,0.2);\n    -webkit-transition: 0.3s;\n    transition: 0.3s;\n    position:absolute;\n\n}\n\n/* On mouse-over, add a deeper shadow */\n\n.card:hover {\n    -webkit-box-shadow: 0 8px 16px 0 rgba(0,0,0,0.2);\n            box-shadow: 0 8px 16px 0 rgba(0,0,0,0.2);\n}\n\n/* Add some padding inside the card container */\n\n.container {\n    padding: 2px 16px;\n    width: auto;\n}\n\n.center {\n    margin: auto;\n    border: 3px solid green;\n    padding: 10px;\n}\n\n.titleIcon{\n  height: \"15px\";\n  float: right;\n  width: auto ;\n}\n\n.titleIconSpeaker{\n  height: \"15px\";\n  width: 15px ;\n  margin-right: 10px;\n\n}\n\n.titleIconCancle{\n  font-size:15;\n  margin-right: 15px;\n  color: #EDF4ED;\n}\n\n/*\n/////////////////form/////////////// */\n\ninput[type=text], select, textarea {\n    width: 30%; /* Full width */\n    padding: 12px; /* Some padding */\n    border: 1px solid #ccc; /* Gray border */\n    border-radius: 4px; /* Rounded borders */\n    -webkit-box-sizing: border-box;\n            box-sizing: border-box; /* Make sure that padding and width stays in place */\n    margin-top: 6px; /* Add a top margin */\n    margin-bottom: 16px; /* Bottom margin */\n    resize: vertical /* Allow the user to vertically resize the textarea (not horizontally) */\n}\n\ninput[type=password], select, textarea {\n    width: 30%; /* Full width */\n    padding: 12px; /* Some padding */\n    border: 1px solid #ccc; /* Gray border */\n    border-radius: 4px; /* Rounded borders */\n    -webkit-box-sizing: border-box;\n            box-sizing: border-box; /* Make sure that padding and width stays in place */\n    margin-top: 6px; /* Add a top margin */\n    margin-bottom: 16px; /* Bottom margin */\n    resize: vertical /* Allow the user to vertically resize the textarea (not horizontally) */\n}\n\n/* Style the submit button with a specific background color etc */\n\ninput[type=submit] {\n    background-color: #4CAF50;\n    color: white;\n    padding: 12px 20px;\n    border: none;\n    border-radius: 4px;\n    cursor: pointer;\n}\n\n/* When moving the mouse over the submit button, add a darker green color */\n\ninput[type=submit]:hover {\n    background-color: #45a049;\n}\n\n/* Add a background color and some padding around the form */\n\n.container {\n    border-radius: 5px;\n    background-color: #f2f2f2;\n    padding: 20px;\n}\n\n#chat-top-left-resize { top: 0px; left: 0px; }\n\n.spn_hol {\n    position: relative;\n    top: 0;\n    left: 0;\n    right: 0;\n    bottom: 0;\n    background: #fff;\n    z-index: 50000;\n    opacity: 1;\n     -webkit-transition: all 1s;\n    transition: all 1s;\n}\n\n.spinner {\n    position: absolute;\n    top: 50%;\n    margin-top: 0;\n    left: 50%;\n    margin-left: -35px;\n    height: 24px;\n    width: 70px;\n    text-align: center;\n    display: block;\n}\n\n.spinner > div {\n  width: 18px;\n  height: 18px;\n  background-color: #333;\n\n  border-radius: 100%;\n  display: inline-block;\n  -webkit-animation: sk-bouncedelay 1.4s infinite ease-in-out both;\n  animation: sk-bouncedelay 1.4s infinite ease-in-out both;\n}\n\n.spinner .bounce1 {\n  -webkit-animation-delay: -0.32s;\n  animation-delay: -0.32s;\n}\n\n.spinner .bounce2 {\n  -webkit-animation-delay: -0.16s;\n  animation-delay: -0.16s;\n}\n\n@-webkit-keyframes sk-bouncedelay {\n  0%, 80%, 100% { -webkit-transform: scale(0) }\n  40% { -webkit-transform: scale(1.0) }\n}\n\n@keyframes sk-bouncedelay {\n  0%, 80%, 100% {\n    -webkit-transform: scale(0);\n    transform: scale(0);\n  } 40% {\n    -webkit-transform: scale(1.0);\n    transform: scale(1.0);\n  }\n}\n\n.usermessage{\n  float: right;\n  width: 65%;\n  padding: 0;\n  margin-top: 0;\n  background-color: #81D4FA;\n  border-radius: 5px;\n  padding: 10px;\n  border-top-left-radius: 20px;\n  border-bottom-right-radius: 20px;\n  margin-top: 0;\n  margin-bottom: 5px;\n  font-size: 0.9em;\n  word-wrap: break-word;\n  margin-right: 40px;\n}\n"

/***/ }),

/***/ "./src/app/chat/chat.component.html":
/***/ (function(module, exports) {

module.exports = "<button type=\"button\" class=\"btn btn-primary\" (click)=\"openForm($event)\">config</button>\n\n<hr>\n\n<div class=\"container\" *ngIf=\"formToggle\">\n  <form action=\"\">\n\n    <label for=\"uname\">User Name</label><br />\n    <input type=\"text\" id=\"fname\" name=\"firstname\" placeholder=\"Your user name is..\"><br />\n\n    <label for=\"password\">Password</label><br />\n    <input type=\"password\" id=\"lname\" name=\"lastname\" placeholder=\"Your password is..\"><br />\n\n    <label for=\"url\">URL</label><br />\n    <input type=\"text\" id=\"lname\" name=\"lastname\" placeholder=\"Url\"><br />\n\n    <input type=\"submit\" value=\"Submit\">\n\n  </form>\n</div>\n\n\n<div *ngIf=\"show\" id=\"ng-chat-people\" [ngClass]=\"{'ng-chat-people-collapsed':isCollapsed}\">\n  <div>\n    <a href=\"javascript:void(0);\" class=\"ng-chat-title shadowed\"\n    (click)=\"onChatTitleClicked($event)\">\n    <span style=\"color: white;\"><b>Your Bots</b></span>\n    </a>\n  </div>\n  <ul id=\"ng-chat-users\" *ngIf=\"!isCollapsed\">\n    <li *ngFor=\"let user of bots\" (click)=\"openChatWindow(user, true)\">\n      <div *ngIf=\"!user.avatar\" class=\"icon-wrapper\">\n        <i class=\"user-icon\"></i>\n      </div>\n\n      <img alt=\"\" class=\"avatar\" height=\"30\" width=\"30\" [src]=\"user.avatar\">\n      <strong [title]=\"user.displayName\">{{ user.displayName}}</strong>\n      <span class=\"ng-chat-user-status online\" title=\"{{ user.status }}\"></span>\n\n    </li>\n  </ul>\n</div>\n\n<div [style.width.px]='width' [style.height.px]='height'\n*ngFor=\"let window of windows; let i = index\"\n[ngClass]=\"{'ng-chat-window': true, 'ng-chat-window-collapsed': window.isCollapsed}\"\n  [ngStyle]=\"{'right': friendsListWidth + 20 + windowSizeFactor * i + 'px'}\">\n  <div *ngIf=\"window.isCollapsed\">\n    <div class=\"ng-chat-title\" (click)=\"onChatWindowClicked(window)\">\n      <strong style=\"color: white;\" title=\"{{window.chattingTo.displayName}}\">\n                {{window.chattingTo.displayName}}\n            </strong>\n\n      <span *ngIf=\"unreadMessagesTotal(window).length > 0\"\n        class=\"ng-chat-unread-messages-count\">{{unreadMessagesTotal(window)}}</span>\n      <div class=\"titleIcon\">\n        <img src=\"assets/notification.png\" (click)=\"onNotificationChatWindow()\"\n        alt=\"\" class=\"titleIconSpeaker\">\n        <img src=\"assets/speaker.png\" (click)=\"onSpeakerChatWindow()\" alt=\"\"\n        class=\"titleIconSpeaker\">\n        <a href=\"javascript:void(0);\" class=\"ng-chat-close\"\n        (click)=\"onCloseChatWindow(window)\" class=\"titleIconCancle\">X</a>\n      </div>\n    </div>\n  </div>\n\n\n  <ng-container *ngIf=\"!window.isCollapsed\">\n    <div (mousedown)='onCornerClick($event, topLeftResize)'\n    id=\"chat-corner-resize\" class=\"ng-chat-title\" (click)=\"onChatWindowClicked(window)\">\n\n\n      <strong style=\"color: white;\" title=\"{{window.chattingTo.displayName}}\">\n                {{window.chattingTo.displayName}}\n            </strong>\n\n      <span *ngIf=\"unreadMessagesTotal(window).length > 0\"\n        class=\"ng-chat-unread-messages-count\">{{unreadMessagesTotal(window)}}</span>\n      <div class=\"titleIcon\">\n\n        <img src=\"assets/notification.png\" alt=\"\"\n        class=\"titleIconSpeaker\" (click)=\"onNotificationChatWindow()\">\n        <img src=\"assets/speaker.png\" alt=\"\"\n        class=\"titleIconSpeaker\" (click)=\"onSpeakerChatWindow()\">\n        <a href=\"javascript:void(0);\" class=\"ng-chat-close\"\n        (click)=\"onCloseChatWindow(window)\" class=\"titleIconCancle\">X</a>\n      </div>\n    </div>\n    <div #chatMessages class=\"ng-chat-messages\">\n      <div *ngIf=\"window.isLoadingHistory\" class=\"ng-chat-loading-wrapper\">\n        <div class=\"loader\">Loading history...</div>\n      </div>\n\n      <div *ngFor=\"let message of window.messages; let i = index; let last=last;\"\n      [ngClass]=\"{'ng-chat-message': true, 'ng-chat-message-received': message.fromId != userId}\">\n\n        <div *ngIf=\"message.fromId == userId\">\n          <img [src]=\"user_image\" width=\"30\" height=\"30\" class=\"img-circle\" id=\"user_base_avatar\" />\n          <span class=\"usermessage\" [innerHtml]=\"message.message\"></span>\n          <div *ngIf=\"last\" class='spn_hol'>\n            <div class='spinner'>\n                <div class='bounce1'></div>\n                <div class='bounce2'></div>\n                <div class='bounce3'></div>\n            </div>\n          </div>\n        </div>\n\n        <div *ngIf=\"window.chattingTo.avatar && isAvatarVisible(window, message, i)\">\n        <img alt=\"\" class=\"avatar\" height=\"30\" width=\"30\" [src]=\"window.chattingTo.avatar\" />\n        <app-html-message [bot_message]=\"message.message\"></app-html-message>\n        </div>\n\n      </div>\n    </div>\n    <input #chatWindowInput [ngModel]=\"window.newMessage \"\n    (ngModelChange)=\"window.newMessage=$event\" type=\"text\"\n    (keydown)=\"onChatInputTyped($event, window)\"\n     (blur)=\"toggleWindowFocus(window)\" (focus)=\"toggleWindowFocus(window)\" />\n  </ng-container>\n\n</div>\n"

/***/ }),

/***/ "./src/app/chat/chat.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ChatComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__botsData_data__ = __webpack_require__("./src/app/botsData/data.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__botsData_user_status_enum__ = __webpack_require__("./src/app/botsData/user-status.enum.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__botsData_message__ = __webpack_require__("./src/app/botsData/message.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__angular_common_http__ = __webpack_require__("./node_modules/@angular/common/esm5/http.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__botsData_bot_message__ = __webpack_require__("./src/app/botsData/bot-message.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__details_form_details_form_component__ = __webpack_require__("./src/app/details-form/details-form.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__myservice__ = __webpack_require__("./src/app/myservice.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};








var ChatComponent = /** @class */ (function () {
    function ChatComponent(http, service) {
        this.http = http;
        this.service = service;
        /////////////open form/////////////////
        this.formToggle = false;
        //////////////////////////////////
        // Exposes the enum for the template
        this.UserStatus = __WEBPACK_IMPORTED_MODULE_2__botsData_user_status_enum__["a" /* UserStatus */];
        this.bots = __WEBPACK_IMPORTED_MODULE_1__botsData_data__["a" /* botsData */];
        this.profileAratar = "Profile_avatar";
        this.friendsListWidth = 262;
        this.persistWindowsState = true;
        this.windows = [];
        this.isCollapsed = false;
        this.windowSizeFactor = 320;
        this.searchPlaceholder = "Search";
        this.searchInput = '';
        this.show = true;
        this.url = this.service.getConfig();
        this.x = 300;
        this.y = 100;
        this.px = 0;
        this.py = 0;
        this.width = 300;
        this.height = 360;
        this.draggingCorner = false;
        this.dragging = false;
        this.minArea = 20000;
    }
    ChatComponent.prototype.openForm = function (event) {
        this.formToggle = !this.formToggle;
    };
    ChatComponent.prototype.onNotificationChatWindow = function () {
        alert("No new Notification");
    };
    ChatComponent.prototype.onSpeakerChatWindow = function () {
        alert("Speaker is ON");
    };
    Object.defineProperty(ChatComponent.prototype, "localStorageKey", {
        get: function () {
            return "ng-chat-users-" + this.userId; // Appending the user id so the state is unique per user in a computer.
        },
        enumerable: true,
        configurable: true
    });
    ;
    ChatComponent.prototype.prepareChatMessage = function (message) {
        return message;
    };
    // Handles received messages by the adapter
    ChatComponent.prototype.onMessageReceived = function (user, message) {
        if (user && message) {
            var chatWindow = this.openChatWindow(user);
            if (!chatWindow[1]) {
                message = this.prepareChatMessage(message);
                chatWindow[0].messages.push(message);
                this.scrollChatWindowToBottom(chatWindow[0]);
            }
            // this.emitMessageSound(chatWindow[0]);
        }
    };
    ChatComponent.prototype.onChatTitleClicked = function (event) {
        if (!this.dragging) {
            this.isCollapsed = !this.isCollapsed;
        }
    };
    // Toggles a chat window visibility between maximized/minimized
    ChatComponent.prototype.onChatWindowClicked = function (window) {
        if (!this.dragging) {
            window.isCollapsed = !window.isCollapsed;
            this.scrollChatWindowToBottom(window);
        }
    };
    // Scrolls a chat window message flow to the bottom
    ChatComponent.prototype.scrollChatWindowToBottom = function (window) {
        var _this = this;
        if (!window.isCollapsed) {
            var windowIndex_1 = this.windows.indexOf(window);
            setTimeout(function () {
                if (_this.chatMessageClusters)
                    _this.chatMessageClusters.toArray()[windowIndex_1].nativeElement.scrollTop = _this.chatMessageClusters.toArray()[windowIndex_1].nativeElement.scrollHeight;
            });
        }
    };
    ChatComponent.prototype.openChatWindow = function (user, focusOnNewWindow) {
        if (focusOnNewWindow === void 0) { focusOnNewWindow = false; }
        // Is this window opened?
        var openedWindow = this.windows.find(function (x) { return x.chattingTo.id == user.id; });
        if (!openedWindow) {
            var newChatWindow = {
                chattingTo: user,
                messages: [],
                hasFocus: false // This will be triggered when the 'newMessage' input gets the current focus
            };
            this.windows.unshift(newChatWindow);
            // Is there enough space left in the view port ?
            if (this.windows.length * this.windowSizeFactor >= this.viewPortTotalArea - this.friendsListWidth) {
                this.windows.pop();
            }
            this.updateWindowsState(this.windows);
            if (focusOnNewWindow)
                this.focusOnWindow(newChatWindow);
            return [newChatWindow, true];
        }
        else {
            // Returns the existing chat window
            return [openedWindow, false];
        }
    };
    // Saves current windows state into local storage if persistence is enabled
    ChatComponent.prototype.updateWindowsState = function (windows) {
        if (this.persistWindowsState) {
            var usersIds = windows.map(function (w) {
                return w.chattingTo.id;
            });
            localStorage.setItem(this.localStorageKey, JSON.stringify(usersIds));
        }
    };
    // Focus on the input element of the supplied window
    ChatComponent.prototype.focusOnWindow = function (window, callback) {
        var _this = this;
        if (callback === void 0) { callback = function () { }; }
        var windowIndex = this.windows.indexOf(window);
        if (windowIndex >= 0) {
            setTimeout(function () {
                var messageInputToFocus = _this.chatWindowInputs.toArray()[windowIndex];
                messageInputToFocus.nativeElement.focus();
                callback();
            });
        }
    };
    // Gets closest open window if any. Most recent opened has priority (Right)
    ChatComponent.prototype.getClosestWindow = function (window) {
        var index = this.windows.indexOf(window);
        if (index > 0) {
            return this.windows[index - 1];
        }
        else if (index == 0 && this.windows.length > 1) {
            return this.windows[index + 1];
        }
    };
    // Closes a chat window via the close 'X' button
    ChatComponent.prototype.onCloseChatWindow = function (window) {
        var index = this.windows.indexOf(window);
        this.windows.splice(index, 1);
        this.updateWindowsState(this.windows);
    };
    ChatComponent.prototype.onCloseChatWindowpeople = function (window) {
        this.show = !this.show;
    };
    //////////////////////received massages/////////////////////////////////////
    ChatComponent.prototype.sendMessage = function (message) {
        var _this = this;
        setTimeout(function () {
            var replyMessage = new __WEBPACK_IMPORTED_MODULE_3__botsData_message__["a" /* Message */]();
            replyMessage.fromId = message.toId;
            replyMessage.toId = message.fromId;
            var bot = _this.bots.find(function (x) { return x.id == replyMessage.fromId; });
            var body = new __WEBPACK_IMPORTED_MODULE_4__angular_common_http__["d" /* HttpParams */]().set('message', message.message);
            var headers = new __WEBPACK_IMPORTED_MODULE_4__angular_common_http__["c" /* HttpHeaders */]().set('Content-Type', 'application/x-www-form-urlencoded');
            _this.http.post('/chat/message/' + bot.id + "/", body.toString(), { headers: headers }).subscribe(function (data) {
                console.log(data);
                var msg = '';
                if (data['error']) {
                    msg = data['error'];
                }
                else {
                    msg = data['resp'];
                }
                if (typeof (msg) != "string") {
                    var bot_message = Object.create(__WEBPACK_IMPORTED_MODULE_5__botsData_bot_message__["a" /* BotMessage */].prototype);
                    msg = Object.assign(bot_message, msg);
                }
                replyMessage.message = msg;
                _this.onMessageReceived(bot, replyMessage);
            }, function (err) {
                console.log("Error occured");
            });
        }, 1000);
    };
    ChatComponent.prototype.onChatInputTyped = function (event, window) {
        switch (event.keyCode) {
            case 13:
                if (window.newMessage && window.newMessage.trim() != "") {
                    var message = new __WEBPACK_IMPORTED_MODULE_3__botsData_message__["a" /* Message */]();
                    message.fromId = this.userId;
                    message.toId = window.chattingTo.id;
                    message.message = window.newMessage;
                    window.messages.push(message);
                    this.sendMessage(message);
                    window.newMessage = ""; // Resets the new message input
                    this.scrollChatWindowToBottom(window);
                }
                break;
        }
    };
    // [Localized] Returns the status descriptive title
    // Returns the total unread messages from a chat window. TODO: Could use some Angular pipes in the future
    ChatComponent.prototype.unreadMessagesTotal = function (window) {
        var _this = this;
        if (window) {
            if (window.hasFocus) {
                this.markMessagesAsRead(window.messages);
            }
            else {
                var totalUnreadMessages = window.messages.filter(function (x) { return x.fromId != _this.userId && !x.seenOn; }).length;
                if (totalUnreadMessages > 0) {
                    if (totalUnreadMessages > 99)
                        return "99+";
                    else
                        return String(totalUnreadMessages);
                }
            }
        }
        // Empty fallback.
        return "";
    };
    // Toggles a window focus on the focus/blur of a 'newMessage' input
    ChatComponent.prototype.toggleWindowFocus = function (window) {
        window.hasFocus = !window.hasFocus;
    };
    // Marks all messages provided as read with the current time.
    ChatComponent.prototype.markMessagesAsRead = function (messages) {
        var currentDate = new Date();
        messages.forEach(function (msg) {
            msg.seenOn = currentDate;
        });
    };
    // Asserts if a user avatar is visible in a chat cluster
    ChatComponent.prototype.isAvatarVisible = function (window, message, index) {
        if (message.fromId != this.userId) {
            if (index == 0) {
                return true; // First message, good to show the thumbnail
            }
            else {
                // Check if the previous message belongs to the same user, if it belongs there is no need to show the avatar again to form the message cluster
                if (window.messages[index - 1].fromId != message.fromId) {
                    return true;
                }
            }
        }
        return false;
    };
    ChatComponent.prototype.area = function () {
        return this.width * this.height;
    };
    ChatComponent.prototype.topLeftResize = function (offsetX, offsetY) {
        this.x += offsetX;
        this.y += offsetY;
        this.width -= offsetX;
        this.height -= offsetY;
    };
    ChatComponent.prototype.onCornerClick = function (event, resizer) {
        this.draggingCorner = true;
        this.dragging = false;
        this.px = event.clientX;
        this.py = event.clientY;
        this.resizer = resizer;
        event.preventDefault();
        event.stopPropagation();
    };
    ChatComponent.prototype.onCornerMove = function (event) {
        if (!this.draggingCorner) {
            return;
        }
        this.dragging = true;
        var offsetX = event.clientX - this.px;
        var offsetY = event.clientY - this.py;
        var lastX = this.x;
        var lastY = this.y;
        var pWidth = this.width;
        var pHeight = this.height;
        this.resizer(offsetX, offsetY);
        if (this.area() < this.minArea) {
            this.x = lastX;
            this.y = lastY;
            this.width = pWidth;
            this.height = pHeight;
        }
        this.px = event.clientX;
        this.py = event.clientY;
    };
    ChatComponent.prototype.onCornerRelease = function (event) {
        this.draggingWindow = false;
        this.draggingCorner = false;
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_9" /* ViewChild */])(__WEBPACK_IMPORTED_MODULE_6__details_form_details_form_component__["a" /* DetailsFormComponent */]),
        __metadata("design:type", Object)
    ], ChatComponent.prototype, "detailurl", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["D" /* Input */])(),
        __metadata("design:type", Array)
    ], ChatComponent.prototype, "bots", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["D" /* Input */])(),
        __metadata("design:type", String)
    ], ChatComponent.prototype, "user_image", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["D" /* Input */])(),
        __metadata("design:type", Object)
    ], ChatComponent.prototype, "localization", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["D" /* Input */])(),
        __metadata("design:type", Boolean)
    ], ChatComponent.prototype, "persistWindowsState", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_10" /* ViewChildren */])('chatMessages'),
        __metadata("design:type", Object)
    ], ChatComponent.prototype, "chatMessageClusters", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_10" /* ViewChildren */])('chatWindowInput'),
        __metadata("design:type", Object)
    ], ChatComponent.prototype, "chatWindowInputs", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["D" /* Input */])(),
        __metadata("design:type", Boolean)
    ], ChatComponent.prototype, "isCollapsed", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["D" /* Input */])(),
        __metadata("design:type", String)
    ], ChatComponent.prototype, "searchPlaceholder", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["D" /* Input */])(),
        __metadata("design:type", Object)
    ], ChatComponent.prototype, "userId", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["y" /* HostListener */])('document:mousemove', ['$event']),
        __metadata("design:type", Function),
        __metadata("design:paramtypes", [MouseEvent]),
        __metadata("design:returntype", void 0)
    ], ChatComponent.prototype, "onCornerMove", null);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["y" /* HostListener */])('document:mouseup', ['$event']),
        __metadata("design:type", Function),
        __metadata("design:paramtypes", [MouseEvent]),
        __metadata("design:returntype", void 0)
    ], ChatComponent.prototype, "onCornerRelease", null);
    ChatComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
            selector: 'app-chat',
            template: __webpack_require__("./src/app/chat/chat.component.html"),
            styles: [__webpack_require__("./src/app/chat/chat.component.css")]
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_4__angular_common_http__["a" /* HttpClient */], __WEBPACK_IMPORTED_MODULE_7__myservice__["a" /* ConfigService */]])
    ], ChatComponent);
    return ChatComponent;
}());



/***/ }),

/***/ "./src/app/details-form/details-form.component.css":
/***/ (function(module, exports) {

module.exports = "p {\n\tfont-family: Montserrat-Regular;\n\tfont-size: 14px;\n\tline-height: 1.7;\n\tcolor: #666666;\n\tmargin: 0px;\n}\n\nul, li {\n\tmargin: 0px;\n\tlist-style-type: none;\n}\n\n/*---------------------------------------------*/\n\ninput {\n\toutline: none;\n\tborder: none;\n}\n\ntextarea {\n  outline: none;\n  border: none;\n}\n\ntextarea:focus, input:focus {\n  border-color: transparent !important;\n}\n\ninput::-webkit-input-placeholder { color: #999999; }\n\ninput:-moz-placeholder { color: #999999; }\n\ninput::-moz-placeholder { color: #999999; }\n\ninput:-ms-input-placeholder { color: #999999; }\n\ntextarea::-webkit-input-placeholder { color: #999999; }\n\ntextarea:-moz-placeholder { color: #999999; }\n\ntextarea::-moz-placeholder { color: #999999; }\n\ntextarea:-ms-input-placeholder { color: #999999; }\n\n/*---------------------------------------------*/\n\nbutton {\n\toutline: none !important;\n\tborder: none;\n\tbackground: transparent;\n}\n\nbutton:hover {\n\tcursor: pointer;\n}\n\niframe {\n\tborder: none !important;\n}\n\n/*//////////////////////////////////////////////////////////////////\n[ Contact 1 ]*/\n\n.contact1 {\n  width: 100%;\n  min-height: 100%;\n  padding: 15px;\n\n  background: #009bff;\n  background: -webkit-gradient(linear, right top, left top, from(#0072ff), to(#00c6ff));\n  background: linear-gradient(to left, #0072ff, #00c6ff);\n\n  display: -webkit-box;\n  display: -ms-flexbox;\n  display: flex;\n  -ms-flex-wrap: wrap;\n      flex-wrap: wrap;\n  -webkit-box-pack: center;\n      -ms-flex-pack: center;\n          justify-content: center;\n  -webkit-box-align: center;\n      -ms-flex-align: center;\n          align-items: center;\n}\n\n.container-contact1 {\n  width: 1163px;\n  background: #fff;\n  border-radius: 10px;\n  overflow: hidden;\n\n  display: -webkit-box;\n  display: -ms-flexbox;\n  display: flex;\n  -ms-flex-wrap: wrap;\n      flex-wrap: wrap;\n  -webkit-box-pack: justify;\n      -ms-flex-pack: justify;\n          justify-content: space-between;\n  -webkit-box-align: center;\n      -ms-flex-align: center;\n          align-items: center;\n\n  padding: 90px 130px 88px 148px;\n}\n\n/*------------------------------------------------------------------\n[  ]*/\n\n.contact1-pic {\n  width: 296px;\n}\n\n.contact1-pic img {\n  max-width: 100%;\n}\n\n/*------------------------------------------------------------------\n[  ]*/\n\n.contact1-form {\n  width: 390px;\n}\n\n.contact1-form-title {\n  display: block;\n  font-family: Montserrat-ExtraBold;\n  font-size: 24px;\n  color: #333333;\n  line-height: 1.2;\n  text-align: center;\n  padding-bottom: 44px;\n}\n\ninput.input1 {\n  height: 50px;\n  border-radius: 25px;\n  padding: 0 30px;\n}\n\ninput.input1 + .shadow-input1 {\n  border-radius: 25px;\n}\n\ntextarea.input1 {\n  min-height: 150px;\n  border-radius: 25px;\n  padding: 12px 30px;\n}\n\ntextarea.input1 + .shadow-input1 {\n  border-radius: 25px;\n}\n\n/*---------------------------------------------*/\n\n.wrap-input1 {\n  position: relative;\n  width: 100%;\n  z-index: 1;\n  margin-bottom: 20px;\n}\n\n.input1 {\n  display: block;\n  width: 100%;\n  background: #e6e6e6;\n  font-family: Montserrat-Bold;\n  font-size: 15px;\n  line-height: 1.5;\n  color: #666666;\n}\n\n.shadow-input1 {\n  content: '';\n  display: block;\n  position: absolute;\n  bottom: 0;\n  left: 0;\n  z-index: -1;\n  width: 100%;\n  height: 100%;\n  -webkit-box-shadow: 0px 0px 0px 0px;\n          box-shadow: 0px 0px 0px 0px;\n  color: rgba(87,184,70, 0.5);\n}\n\n.input1:focus + .shadow-input1 {\n  -webkit-animation: anim-shadow 0.5s ease-in-out forwards;\n  animation: anim-shadow 0.5s ease-in-out forwards;\n}\n\n/*---------------------------------------------*/\n\n.container-contact1-form-btn {\n  display: -webkit-box;\n  display: -ms-flexbox;\n  display: flex;\n  -ms-flex-wrap: wrap;\n      flex-wrap: wrap;\n  -webkit-box-pack: center;\n      -ms-flex-pack: center;\n          justify-content: center;\n}\n\n.contact1-form-btn {\n  min-width: 193px;\n  height: 50px;\n  border-radius: 25px;\n  background: #57b846;\n  font-family: Montserrat-Bold;\n  font-size: 15px;\n  line-height: 1.5;\n  color: #fff;\n  display: -webkit-box;\n  display: -ms-flexbox;\n  display: flex;\n  -webkit-box-pack: center;\n      -ms-flex-pack: center;\n          justify-content: center;\n  -webkit-box-align: center;\n      -ms-flex-align: center;\n          align-items: center;\n  padding: 0 25px;\n\n  -webkit-transition: all 0.4s;\n  transition: all 0.4s;\n}\n\n.contact1-form-btn i {\n  margin-left: 7px;\n\n  -webkit-transition: all 0.4s;\n  transition: all 0.4s;\n}\n\n.contact1-form-btn:hover {\n  background: #333333;\n}\n\n.contact1-form-btn:hover i {\n  -webkit-transform: translateX(10px);\n  transform: translateX(10px);\n}\n\n/*\n///////////////////////////\n */\n\n#chat-top-left-resize { top: 0px; left: 0px; }\n"

/***/ }),

/***/ "./src/app/details-form/details-form.component.html":
/***/ (function(module, exports) {

module.exports = "<div class=\"contact1\">\n\t<div class=\"container-contact1\">\n\t\t<div class=\"contact1-pic js-tilt\" data-tilt>\n\t\t\t<!-- <img src=\"assets/img-01.png\" alt=\"IMG\"> -->\n\t\t</div>\n\n\t\t<form class=\"contact1-form validate-form\">\n\t\t\t<span class=\"contact1-form-title\">\n\t\t\t\t\tFill these Details\n\t\t\t\t</span>\n\n\t\t\t<div class=\"wrap-input1 validate-input\" data-validate=\"Name is required\">\n\t\t\t\t<input class=\"input1\" type=\"text\" [(ngModel)]=\"uname\" placeholder=\"User Name\" [ngModelOptions]=\"{standalone: true}\">\n\t\t\t</div>\n\n\t\t\t<div class=\"wrap-input1 validate-input\" data-validate=\"Valid email is required: ex@abc.xyz\">\n\t\t\t\t<input class=\"input1\" type=\"password\" [(ngModel)]=\"password\" placeholder=\"Password\" [ngModelOptions]=\"{standalone: true}\">\n\t\t\t</div>\n\n\t\t\t<div class=\"wrap-input1 validate-input\" data-validate=\"Subject is required\">\n\t\t\t\t<input class=\"input1\" type=\"url\" placeholder=\"URL\" [(ngModel)]=\"url\" [ngModelOptions]=\"{standalone: true}\">\n\t\t\t</div>\n\t\t\t<a  [routerLink]=\"['/chat']\">\n\t\t\t<button class=\"contact1-form-btn\" (click)=\"buttonClick()\">\n\t\t\t\t\t\t<span >\n\t\t\t\t\t\t\tSend\n\t\t\t\t\t\t</span>\n\t\t\t\t\t</button></a>\n\t\t</form>\n\t</div>\n</div>\n"

/***/ }),

/***/ "./src/app/details-form/details-form.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return DetailsFormComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_router__ = __webpack_require__("./node_modules/@angular/router/esm5/router.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__myservice__ = __webpack_require__("./src/app/myservice.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var DetailsFormComponent = /** @class */ (function () {
    function DetailsFormComponent(router, service) {
        this.router = router;
        this.service = service;
        console.log("detailsform");
    }
    DetailsFormComponent.prototype.buttonClick = function () {
        this.key = this.url;
        this.value = JSON.stringify([{ 'UserName': this.uname, 'password': this.password }]);
        sessionStorage.setItem(this.key, this.value);
        // this.router.navigateByUrl('/chat');
        alert(sessionStorage.getItem(this.url));
        this.service.setOption(this.url);
    };
    DetailsFormComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
            selector: 'app-details-form',
            template: __webpack_require__("./src/app/details-form/details-form.component.html"),
            styles: [__webpack_require__("./src/app/details-form/details-form.component.css")]
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__angular_router__["a" /* Router */], __WEBPACK_IMPORTED_MODULE_2__myservice__["a" /* ConfigService */]])
    ], DetailsFormComponent);
    return DetailsFormComponent;
}());



/***/ }),

/***/ "./src/app/full-chat/full-chat.component.css":
/***/ (function(module, exports) {

module.exports = "#ng-chat {\n  position: fixed;\n  z-index: 999;\n  right: 0;\n  bottom: 0;\n  color: #5C5C5C;\n  font-family: Arial, Helvetica, sans-serif;\n  -webkit-box-sizing: initial;\n          box-sizing: initial;\n  font-size: 11pt;\n  text-align: left;\n}\n\n.ng-chat-loading-wrapper {\n  height: 30px;\n  text-align: center;\n  font-size: 0.9em;\n}\n\n#ng-chat-people {\n    position: absolute;\n    width: 240px;\n    float:right;\n    height: 360px;\n    bottom: 0px;\n    right: 0px;\n    border: 2px solid #38414c;\n    margin-right: 20px;\n    border-top-right-radius: 25px;\n\n    border-top-left-radius: 12px;\n    background-color: #B0BEC5;\n    -webkit-box-shadow: 0 4px 8px rgba(0,0,0,.25);\n    box-shadow: 0 4px 8px rgba(0,0,0,.25);\n    border-bottom: 0;\n    border-bottom-width: 0px;\n    border-bottom-style: initial;\n    border-bottom-color: initial;\n    margin-bottom: auto;\n}\n\n#ng-chat-people-content{\n  position: absolute;\nbottom: 0;\nleft: 0;\n}\n\n#ng-chat-people.ng-chat-people-collapsed {\n  height: 30px;\n\n}\n\n.ng-chat-close {\n  color: #5C5C5C;\n  text-decoration: none;\n  float: right;\n}\n\n.ng-chat-title, .ng-chat-title:hover {\n  position: relative;\n  z-index: 2;\n  height: 30px;\n  line-height: 30px;\n  font-size: 1em;\n  padding: 0 10px;\n  border-radius:5px;\n  border-bottom-left-radius: 20px;\n  border-top-right-radius: 20px;\n  background-color: #38414c;\n  display: block;\n  text-decoration: none;\n  color: inherit;\n  font-weight: 400;\n  cursor: pointer;\n  /* background-color:#FFF; */\n}\n\n.ng-chat-title.shadowed {\n  -webkit-box-shadow: 0 4px 8px rgba(0, 0, 0, .25);\n          box-shadow: 0 4px 8px rgba(0, 0, 0, .25);\n}\n\n.ng-chat-title>strong {\n  font-weight: 600;\n  display: block;\n  overflow: hidden;\n  height: 30px;\n  text-overflow: ellipsis;\n  white-space: nowrap;\n  max-width: 85%;\n  float: left;\n}\n\n.ng-chat-title>.ng-chat-user-status {\n  float: left;\n  margin-left: 5px;\n}\n\n#ng-chat-search_friend {\n  display: block;\n  padding: 7px 10px;\n  margin: 0 auto;\n  width: calc(100% - 20px);\n  margin-top: 10px;\n  font-size: 0.9em;\n  -webkit-appearance: searchfield;\n}\n\n#ng-chat-users {\n  padding: 0 10px;\n  list-style: none;\n  margin: 0;\n  overflow: auto;\n  position: absolute;\n  top: 84px;\n  bottom: 0;\n  width: 100%;\n  -webkit-box-sizing: border-box;\n          box-sizing: border-box;\n}\n\n#ng-chat-users li {\n  clear: both;\n  margin-bottom: 10px;\n  overflow: hidden;\n  cursor: pointer;\n  max-height: 30px;\n}\n\n#ng-chat-users li>img, #ng-chat-users li>.icon-wrapper {\n  float: left;\n  margin-right: 5px;\n  border-radius: 25px;\n}\n\n#ng-chat-users li>.icon-wrapper {\n  background-color: #BABABA;\n  overflow: hidden;\n  width: 30px;\n  height: 30px;\n}\n\n#ng-chat-users li>.icon-wrapper>i {\n  color: #FFF;\n  -webkit-transform: scale(0.7);\n          transform: scale(0.7);\n}\n\n#ng-chat-users li>strong {\n  float: left;\n  line-height: 30px;\n  font-size: 0.8em;\n  max-width: 57%;\n  max-height: 30px;\n  overflow: hidden;\n  white-space: nowrap;\n  text-overflow: ellipsis;\n}\n\n#ng-chat-users li>.ng-chat-user-status {\n  float: right;\n}\n\n.ng-chat-user-status {\n  border-radius: 25px;\n  width: 8px;\n  height: 8px;\n  margin-top: 10px;\n}\n\n.ng-chat-user-status.online {\n  background-color: #92A400;\n}\n\n.ng-chat-user-status.busy {\n  background-color: #F91C1E;\n}\n\n.ng-chat-user-status.away {\n  background-color: #F7D21B;\n}\n\n.ng-chat-user-status.offline {\n  background-color: #BABABA;\n}\n\n.ng-chat-unread-messages-count {\n  background-color: #E3E3E3;\n  margin-left: 5px;\n  padding: 0 5px;\n  border-radius: 25px;\n  font-size: 0.9em;\n  color: #5C5C5C;\n  line-height: 30px;\n}\n\n.ng-chat-window {\n  bottom: 0;\n  border: 1.5px solid #38414c;\n  border-bottom: 0;\n  border-top-right-radius: 25px;\n  position: fixed;\n  width: 100%;\n  height: 100%;\n  left: 0;\n  top: 0;\n  background: rgba(51, 51, 51, 0.7);\n  z-index: 10;\n  border-top-left-radius: 12px;\n  -webkit-box-shadow: 0 4px 8px rgba(0, 0, 0, .25);\n          box-shadow: 0 4px 8px rgba(0, 0, 0, .25);\n}\n\n/* .ng-chat-window {\n  right: 260px;\n  height: 360px;\n  z-index: 999;\n  bottom: 0;\n  position: fixed;\n  width: 300px;\n  border: 1.5px solid #38414c;\n  border-bottom: 0;\n  border-top-right-radius: 25px;\n\n  border-top-left-radius: 12px;\n  box-shadow: 0 4px 8px rgba(0, 0, 0, .25);\n} */\n\n.ng-chat-window-collapsed {\n  height: 30px !important;\n}\n\n.ng-chat-window>input {\n  font-size: 1.5em;\n  -webkit-box-sizing: border-box;\n          box-sizing: border-box;\n  padding: 0 5px;\n  display: block;\n\n  height: calc(10%);\n  width: 100%;\n  border: none;\n  border-top: 1px solid #A3A3A3;\n}\n\n.ng-chat-window .ng-chat-messages {\n  padding: 10px;\n  height: calc(90% - 30px);\n  -webkit-box-sizing: border-box;\n          box-sizing: border-box;\n  position: relative;\n  overflow: auto;\n  background-color: #FFF;\n}\n\n.ng-chat-window .ng-chat-messages .ng-chat-message {\n  clear: both;\n}\n\n.ng-chat-window .ng-chat-messages .ng-chat-message>img, .ng-chat-window .ng-chat-messages .ng-chat-message>.icon-wrapper {\n  position: absolute;\n  left: 10px;\n\n  border-radius: 25px;\n}\n\n.ng-chat-window .ng-chat-messages .ng-chat-message>.icon-wrapper {\n  background-color: #80CBC4;\n  overflow: hidden;\n  width: 30px;\n  height: 30px;\n\n}\n\n.ng-chat-window .ng-chat-messages .ng-chat-message>.icon-wrapper>i {\n  color: #FFF;\n  -webkit-transform: scale(0.7);\n          transform: scale(0.7);\n}\n\n.ng-chat-window .ng-chat-messages .ng-chat-message>span {\n  float: right;\n  width: 40%;\n  padding: 0;\n  margin-top: 0;\n  background-color: #81D4FA;\n  border-radius: 5px;\n  padding: 10px;\n  border-top-left-radius: 20px;\n  border-bottom-right-radius: 20px;\n  margin-top: 0;\n  margin-bottom: 5px;\n  font-size: 1.5em;\n  word-wrap: break-word;\n}\n\n.ng-chat-window .ng-chat-messages .ng-chat-message.ng-chat-message-received>span {\n  float: left;\n  margin-left: 40px;\n  padding-top: 7px;\n  padding-bottom: 7px;\n  border-top-left-radius: 20px;\n  border-bottom-right-radius: 20px;\n  background-color: #A7BBEC;\n  border: 3px solid #E3E3E3;\n  margin-top: 0;\n  margin-bottom: 5px;\n}\n\n/*\n/////////////cards/////////// */\n\n.card {\n    /* Add shadows to create the \"card\" effect */\n    -webkit-box-shadow: 0 4px 8px 0 rgba(0,0,0,0.2);\n            box-shadow: 0 4px 8px 0 rgba(0,0,0,0.2);\n    -webkit-transition: 0.3s;\n    transition: 0.3s;\n    position:absolute;\n\n}\n\n/* On mouse-over, add a deeper shadow */\n\n.card:hover {\n    -webkit-box-shadow: 0 8px 16px 0 rgba(0,0,0,0.2);\n            box-shadow: 0 8px 16px 0 rgba(0,0,0,0.2);\n}\n\n/* Add some padding inside the card container */\n\n.container {\n    padding: 2px 16px;\n    width: auto;\n}\n\n.center {\n    margin: auto;\n    border: 3px solid green;\n    padding: 10px;\n}\n\n.titleIcon{\n  height: \"15px\";\n  float: right;\n  width: auto ;\n}\n\n.titleIconSpeaker{\n  height: \"15px\";\n  width: 15px ;\n  margin-right: 10px;\n\n}\n\n.titleIconCancle{\n  font-size:15;\n  margin-right: 15px;\n  color: #EDF4ED;\n}\n"

/***/ }),

/***/ "./src/app/full-chat/full-chat.component.html":
/***/ (function(module, exports) {

module.exports = "<button *ngFor=\"let user of botsData\" (click)=\"openChatWindow(user, true)\"\n class=\"btn btn-primary\" style=\"margin-left:20px;\">Open chat window</button>\n\n<div *ngFor=\"let window of windows; let i = index\" [ngClass]=\"{'ng-chat-window': true, 'ng-chat-window-collapsed': window.isCollapsed}\" [ngStyle]=\"{'right': friendsListWidth + 20 + windowSizeFactor * i + 'px'}\">\n  <ng-container *ngIf=\"window.isCollapsed\">\n    <div class=\"ng-chat-title\" (click)=\"onChatWindowClicked(window)\">\n      <strong style=\"color: white;\" title=\"{{window.chattingTo.displayName}}\">\n                {{window.chattingTo.displayName}}\n            </strong>\n      <span [ngClass]=\"{'ng-chat-user-status': true, 'online': window.chattingTo.status == UserStatus.Online, 'busy': window.chattingTo.status == UserStatus.Busy, 'away': window.chattingTo.status == UserStatus.Away, 'offline': window.chattingTo.status == UserStatus.Offline}\"\n        title=\"{{getStatusTitle(UserStatus[window.chattingTo.status])}}\"></span>\n      <span *ngIf=\"unreadMessagesTotal(window).length > 0\" class=\"ng-chat-unread-messages-count\">{{unreadMessagesTotal(window)}}</span>\n      <div class=\"titleIcon\">\n        <img src=\"../assets/notification.png\" (click)=\"onNotificationChatWindow()\" alt=\"\" class=\"titleIconSpeaker\">\n        <img src=\"../assets/speaker.png\" (click)=\"onSpeakerChatWindow()\" alt=\"\" class=\"titleIconSpeaker\">\n        <a href=\"javascript:void(0);\" class=\"ng-chat-close\" (click)=\"onCloseChatWindow(window)\" class=\"titleIconCancle\">X</a>\n      </div>\n    </div>\n  </ng-container>\n  <ng-container *ngIf=\"!window.isCollapsed\">\n    <div  class=\"ng-chat-title\" (click)=\"onChatWindowClicked(window)\">\n      <strong style=\"color: white;\" title=\"{{window.chattingTo.displayName}}\">\n                {{window.chattingTo.displayName}}\n            </strong>\n      <!-- <span [ngClass]=\"{'ng-chat-user-status': true, 'online': window.chattingTo.status == UserStatus.Online, 'busy': window.chattingTo.status == UserStatus.Busy, 'away': window.chattingTo.status == UserStatus.Away, 'offline': window.chattingTo.status == UserStatus.Offline}\"\n        title=\"{{getStatusTitle(UserStatus[window.chattingTo.status])}}\"></span> -->\n      <span *ngIf=\"unreadMessagesTotal(window).length > 0\" class=\"ng-chat-unread-messages-count\">{{unreadMessagesTotal(window)}}</span>\n      <div class=\"titleIcon\">\n\n        <img src=\"../assets/notification.png\" alt=\"\" class=\"titleIconSpeaker\" (click)=\"onNotificationChatWindow()\">\n        <img src=\"../assets/speaker.png\" alt=\"\" class=\"titleIconSpeaker\" (click)=\"onSpeakerChatWindow()\">\n        <a href=\"javascript:void(0);\" class=\"ng-chat-close\" (click)=\"onCloseChatWindow(window)\" class=\"titleIconCancle\">X</a>\n      </div>\n    </div>\n    <div #chatMessages class=\"ng-chat-messages\">\n      <div *ngIf=\"window.isLoadingHistory\" class=\"ng-chat-loading-wrapper\">\n        <div class=\"loader\">Loading history...</div>\n      </div>\n\n      <div *ngFor=\"let message of window.messages; let i = index\" [ngClass]=\"{'ng-chat-message': true, 'ng-chat-message-received': message.fromId != userId}\">\n        <div *ngIf=\"!window.chattingTo.avatar && isAvatarVisible(window, message, i)\" class=\"icon-wrapper\">\n          <i class=\"user-icon\"></i>\n        </div>\n        <img *ngIf=\"window.chattingTo.avatar && isAvatarVisible(window, message, i)\" alt=\"\" class=\"avatar\" height=\"30\" width=\"30\" [src]=\"window.chattingTo.avatar\" />\n        <span [innerHtml]=\"message.message \"></span>\n      </div>\n    </div>\n\n    <input #chatWindowInput [ngModel]=\"window.newMessage \" (ngModelChange)=\"window.newMessage=$event\" type=\"text\" (keydown)=\"onChatInputTyped($event, window)\" (blur)=\"toggleWindowFocus(window)\" (focus)=\"toggleWindowFocus(window)\" />\n  </ng-container>\n</div>\n"

/***/ }),

/***/ "./src/app/full-chat/full-chat.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return FullChatComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__botsData_user_status_enum__ = __webpack_require__("./src/app/botsData/user-status.enum.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__botsData_message__ = __webpack_require__("./src/app/botsData/message.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__angular_common_http__ = __webpack_require__("./node_modules/@angular/common/esm5/http.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var FullChatComponent = /** @class */ (function () {
    function FullChatComponent(http) {
        this.http = http;
        /////////////open form/////////////////
        this.formToggle = false;
        //////////////////////////////////
        // Exposes the enum for the template
        this.UserStatus = __WEBPACK_IMPORTED_MODULE_1__botsData_user_status_enum__["a" /* UserStatus */];
        this.profileAratar = "Profile_avatar";
        this.friendsListWidth = 262;
        this.persistWindowsState = true;
        this.windows = [];
        this.isCollapsed = true;
        this.windowSizeFactor = 320;
        this.searchPlaceholder = "Search";
        this.searchInput = '';
        this.pattern = new RegExp(/(http|https):\/\/(\w+:{0,1}\w*)?(\S+)(:[0-9]+)?(\/|\/([\w#!:.?+=&%!\-\/]))?/); // fragment locater
    }
    FullChatComponent.prototype.openForm = function (event) {
        this.formToggle = !this.formToggle;
    };
    FullChatComponent.prototype.onNotificationChatWindow = function () {
        alert("No new Notification");
    };
    FullChatComponent.prototype.onSpeakerChatWindow = function () {
        alert("Speaker is ON");
    };
    Object.defineProperty(FullChatComponent.prototype, "localStorageKey", {
        get: function () {
            return "ng-chat-users-" + this.userId; // Appending the user id so the state is unique per user in a computer.
        },
        enumerable: true,
        configurable: true
    });
    ;
    // Handles received messages by the adapter
    FullChatComponent.prototype.onMessageReceived = function (user, message) {
        if (user && message) {
            var chatWindow = this.openChatWindow(user);
            if (!chatWindow[1]) {
                chatWindow[0].messages.push(message);
                this.scrollChatWindowToBottom(chatWindow[0]);
            }
            // this.emitMessageSound(chatWindow[0]);
        }
    };
    Object.defineProperty(FullChatComponent.prototype, "filteredUsers", {
        get: function () {
            var _this = this;
            if (this.searchInput.length > 0) {
                // Searches in the friend list by the inputted search string
                return this.users.filter(function (x) { return x.displayName.toUpperCase().includes(_this.searchInput.toUpperCase()); });
            }
            return this.users;
        },
        enumerable: true,
        configurable: true
    });
    FullChatComponent.prototype.onChatTitleClicked = function (event) {
        this.isCollapsed = !this.isCollapsed;
    };
    // Toggles a chat window visibility between maximized/minimized
    FullChatComponent.prototype.onChatWindowClicked = function (window) {
        window.isCollapsed = !window.isCollapsed;
        this.scrollChatWindowToBottom(window);
    };
    // Scrolls a chat window message flow to the bottom
    FullChatComponent.prototype.scrollChatWindowToBottom = function (window) {
        var _this = this;
        if (!window.isCollapsed) {
            var windowIndex_1 = this.windows.indexOf(window);
            setTimeout(function () {
                if (_this.chatMessageClusters)
                    _this.chatMessageClusters.toArray()[windowIndex_1].nativeElement.scrollTop = _this.chatMessageClusters.toArray()[windowIndex_1].nativeElement.scrollHeight;
            });
        }
    };
    FullChatComponent.prototype.openChatWindow = function (user, focusOnNewWindow) {
        if (focusOnNewWindow === void 0) { focusOnNewWindow = false; }
        // Is this window opened?
        var openedWindow = this.windows.find(function (x) { return x.chattingTo.id == user.id; });
        if (!openedWindow) {
            var newChatWindow = {
                chattingTo: user,
                messages: [],
                hasFocus: false // This will be triggered when the 'newMessage' input gets the current focus
            };
            this.windows.unshift(newChatWindow);
            // Is there enough space left in the view port ?
            if (this.windows.length * this.windowSizeFactor >= this.viewPortTotalArea - this.friendsListWidth) {
                this.windows.pop();
            }
            this.updateWindowsState(this.windows);
            if (focusOnNewWindow)
                this.focusOnWindow(newChatWindow);
            return [newChatWindow, true];
        }
        else {
            // Returns the existing chat window
            return [openedWindow, false];
        }
    };
    // Saves current windows state into local storage if persistence is enabled
    FullChatComponent.prototype.updateWindowsState = function (windows) {
        if (this.persistWindowsState) {
            var usersIds = windows.map(function (w) {
                return w.chattingTo.id;
            });
            localStorage.setItem(this.localStorageKey, JSON.stringify(usersIds));
        }
    };
    // Focus on the input element of the supplied window
    FullChatComponent.prototype.focusOnWindow = function (window, callback) {
        var _this = this;
        if (callback === void 0) { callback = function () { }; }
        var windowIndex = this.windows.indexOf(window);
        if (windowIndex >= 0) {
            setTimeout(function () {
                var messageInputToFocus = _this.chatWindowInputs.toArray()[windowIndex];
                messageInputToFocus.nativeElement.focus();
                callback();
            });
        }
    };
    // Gets closest open window if any. Most recent opened has priority (Right)
    FullChatComponent.prototype.getClosestWindow = function (window) {
        var index = this.windows.indexOf(window);
        if (index > 0) {
            return this.windows[index - 1];
        }
        else if (index == 0 && this.windows.length > 1) {
            return this.windows[index + 1];
        }
    };
    // Closes a chat window via the close 'X' button
    FullChatComponent.prototype.onCloseChatWindow = function (window) {
        var index = this.windows.indexOf(window);
        this.windows.splice(index, 1);
        this.updateWindowsState(this.windows);
    };
    FullChatComponent.prototype.sendMessage = function (message) {
        var _this = this;
        setTimeout(function () {
            var replyMessage = new __WEBPACK_IMPORTED_MODULE_2__botsData_message__["a" /* Message */]();
            replyMessage.fromId = message.toId;
            replyMessage.toId = message.fromId;
            // replyMessage.message = "You have typed '" + message.message + "'";
            // replyMessage.message = JSON.stringify(data);
            if (_this.pattern.test(message.message)) {
                _this.http.get(message.message).subscribe(function (data) {
                    console.log(data); // using the HttpClient instance, http to call the API then subscribe to the data and display to console
                    replyMessage.message = JSON.stringify(data);
                });
            }
            else if (message.message != null) {
                replyMessage.message = "You have typed '" + message.message + "'";
            }
            var user = _this.botsData.find(function (x) { return x.id == replyMessage.fromId; });
            _this.onMessageReceived(user, replyMessage);
        }, 1000);
    };
    FullChatComponent.prototype.onChatInputTyped = function (event, window) {
        switch (event.keyCode) {
            case 13:
                if (window.newMessage && window.newMessage.trim() != "") {
                    var message = new __WEBPACK_IMPORTED_MODULE_2__botsData_message__["a" /* Message */]();
                    message.fromId = this.userId;
                    message.toId = window.chattingTo.id;
                    message.message = window.newMessage;
                    window.messages.push(message);
                    this.sendMessage(message);
                    window.newMessage = ""; // Resets the new message input
                    this.scrollChatWindowToBottom(window);
                }
                break;
        }
    };
    // [Localized] Returns the status descriptive title
    FullChatComponent.prototype.getStatusTitle = function (status) {
        var currentStatus = status.toString().toLowerCase();
        return this.localization.statusDescription[currentStatus];
    };
    // Returns the total unread messages from a chat window. TODO: Could use some Angular pipes in the future
    FullChatComponent.prototype.unreadMessagesTotal = function (window) {
        var _this = this;
        if (window) {
            if (window.hasFocus) {
                this.markMessagesAsRead(window.messages);
            }
            else {
                var totalUnreadMessages = window.messages.filter(function (x) { return x.fromId != _this.userId && !x.seenOn; }).length;
                if (totalUnreadMessages > 0) {
                    if (totalUnreadMessages > 99)
                        return "99+";
                    else
                        return String(totalUnreadMessages);
                }
            }
        }
        // Empty fallback.
        return "";
    };
    // Toggles a window focus on the focus/blur of a 'newMessage' input
    FullChatComponent.prototype.toggleWindowFocus = function (window) {
        window.hasFocus = !window.hasFocus;
    };
    // Marks all messages provided as read with the current time.
    FullChatComponent.prototype.markMessagesAsRead = function (messages) {
        var currentDate = new Date();
        messages.forEach(function (msg) {
            msg.seenOn = currentDate;
        });
    };
    // Asserts if a user avatar is visible in a chat cluster
    FullChatComponent.prototype.isAvatarVisible = function (window, message, index) {
        if (message.fromId != this.userId) {
            if (index == 0) {
                return true; // First message, good to show the thumbnail
            }
            else {
                // Check if the previous message belongs to the same user, if it belongs there is no need to show the avatar again to form the message cluster
                if (window.messages[index - 1].fromId != message.fromId) {
                    return true;
                }
            }
        }
        return false;
    };
    FullChatComponent.prototype.ngOnInit = function () {
    };
    FullChatComponent.prototype.sendURL = function (urldata) {
        console.log(this.urldata);
        var message = new __WEBPACK_IMPORTED_MODULE_2__botsData_message__["a" /* Message */]();
        message.message = this.window.newMessage;
        this.window.messages.push(urldata);
        this.scrollChatWindowToBottom(this.window);
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["D" /* Input */])(),
        __metadata("design:type", Array)
    ], FullChatComponent.prototype, "botsData", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["D" /* Input */])(),
        __metadata("design:type", Object)
    ], FullChatComponent.prototype, "localization", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["D" /* Input */])(),
        __metadata("design:type", Boolean)
    ], FullChatComponent.prototype, "persistWindowsState", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_10" /* ViewChildren */])('chatMessages'),
        __metadata("design:type", Object)
    ], FullChatComponent.prototype, "chatMessageClusters", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_10" /* ViewChildren */])('chatWindowInput'),
        __metadata("design:type", Object)
    ], FullChatComponent.prototype, "chatWindowInputs", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["D" /* Input */])(),
        __metadata("design:type", Boolean)
    ], FullChatComponent.prototype, "isCollapsed", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["D" /* Input */])(),
        __metadata("design:type", String)
    ], FullChatComponent.prototype, "searchPlaceholder", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["D" /* Input */])(),
        __metadata("design:type", Object)
    ], FullChatComponent.prototype, "userId", void 0);
    FullChatComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
            selector: 'app-full-chat',
            template: __webpack_require__("./src/app/full-chat/full-chat.component.html"),
            styles: [__webpack_require__("./src/app/full-chat/full-chat.component.css")]
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_3__angular_common_http__["a" /* HttpClient */]])
    ], FullChatComponent);
    return FullChatComponent;
}());



/***/ }),

/***/ "./src/app/html-message/html-message.component.css":
/***/ (function(module, exports) {

module.exports = ".botmessage {\n  float: left;\n  width: 65%;\n  padding: 0;\n  margin-top: 0;\n  background-color: #A7BBEC;\n  border-radius: 5px;\n  padding: 10px;\n  border-top-left-radius: 20px;\n  border-bottom-right-radius: 20px;\n  margin-top: 0;\n  margin-bottom: 5px;\n  font-size: 0.9em;\n  word-wrap: break-word;\n  margin-left: 40px;\n}\n"

/***/ }),

/***/ "./src/app/html-message/html-message.component.html":
/***/ (function(module, exports) {

module.exports = "<span class=\"botmessage\" [innerHtml]=\"message\"></span>\n<!-- <ng-template #USER>\n    <div class='chat_message_wrapper chat_message_right'>\n        <div class='chat_user_avatar'>\n            <img src='{{base_url}}/static/images/avatar.png' class='md-user-image'>\n        </div>\n        <ul class='chat_message'>\n            <li>\n                <p>\n                    {{message}}\n                </p>\n            </li>\n        </ul>\n    </div>\n</ng-template>\n\n<ng-template #BOT>\n    <div class='chat_message_wrapper'>\n        <div class='chat_user_avatar'>\n            <img src=\"{{bot_img}}{{^bot_img}}{{base_url}}/static/images/entillio.png{{/bot_img}}\"\n                class='md-user-image'>\n        </div>\n        <ul class='chat_message'>\n            <li>\n                <p>\n                   {{{message}}}\n                </p>\n            </li>\n        </ul>\n    </div>\n</ng-template>\n\n<ng-template #spinner>\n    <div class='spn_hol'>\n        <div class='spinner' style='display:block'>\n            <div class='bounce1'></div>\n            <div class='bounce2'></div>\n            <div class='bounce3'></div>\n        </div>\n    </div>\n</ng-template>\n\n<ng-template #thumbs>\n<div align=\"right\" id=\"{{chat_id}}\">\n    <a class=\"thumbs\" data-chatid={{chat_id}} data-botid={{bot_id}} data-baseurl={{base_url}}\n        data-value=\"like\">\n        <img src=\"{{base_url}}/static/images/thumbs_up.png\" alt=\"Like\" title=\"Like\">\n    </a>\n    <a class=\"thumbs\" data-chatid={{chat_id}} data-botid={{bot_id}} data-baseurl={{base_url}}\n        data-value=\"dislike\">\n        <img src=\"{{base_url}}/static/images/thumbs_down.png\" alt=\"Dislike\" title=\"Dislike\">\n    </a>\n</div>\n</ng-template>\n\n<ng-template #star>\n<div align=\"right\" id=\"{{chat_id}}\">\n    <input type=\"image\" src=\"\" class=\"rating rating-loading\"  value=\"0\" data-size=\"xs\" title=\"\"\n    data-chatid={{chat_id}} data-botid={{bot_id}} data-baseurl={{base_url}}\n    >\n</div>\n</ng-template>\n\n<ng-template #suggestions>\n<br><br>\n<b>Some of the other questions of interest could be as follows:</b><br>\n<ul>\n    {{#suggestions}}\n    <li>\n        <a href=\"#\" data-inp-val=\"{{question}}\">{{question}}</a>: <b><i>({{score}}% confidence)</i></b>\n    </li>\n     {{/suggestions}}\n</ul>\n<br>\n<i>\n<p>If none of the above matches your requirement, please rephrase to include more keywords</p>\n</i>\n<br>\n</ng-template>\n\n<ng-template #Text>\n<p style=\"{{css}}\">{{value}}</p>\n</ng-template>\n\n<ng-template #List_with_Checkboxes>\n<ul id=\"{{id}}\" style=\"list-style-type: none;{{css}}\">\n    {{#options}}\n    <li><input type=\"checkbox\" style=\"vertical-align:middle\" value=\"{{displayvalue}}\">{{displayname}}</li>\n    {{/options}}\n    <br>\n    <button type=\"button\" onclick=\"changeValue(returnValues(this.parentNode.id),'cards')\">Proceed</button>\n</ul>\n</ng-template>\n\n<ng-template #Carousel>\n<div id=\"carousel-example-generic{{id}}\" class=\"carousel slide\" data-ride=\"carousel\">\n    <ol id=\"carousel-indicators{{id}}\" class=\"carousel-indicators\">\n        {{#options.0}}\n            <li data-target=\"#carousel-example-generic{{id}}\" data-slide-to=\"{{@index}}\" class=\"active\"></li>\n        {{/options.0}}\n        {{#options}}\n            <li data-target=\"#carousel-example-generic{{id}}\" data-slide-to=\"{{@index}}\"></li>\n        {{/options}}\n    </ol>\n    <div id=\"carousel-inner{{id}}\" class=\"carousel-inner\">\n        {{#options.0}}\n            <div id=\"item{{id}}\" class=\"item active\"><img src=\"{{displayvalue}}\">\n                <div class=\"carousel-caption\"></div>\n            </div>\n        {{/options.0}}\n        {{#options}}\n            <div id=\"item{{id}}\" class=\"item\"><img src=\"{{displayvalue}}\">\n                <div class=\"carousel-caption\"></div>\n            </div>\n        {{/options}}\n    </div>\n    <a href=\"#carousel-example-generic{{id}}\" class=\"left carousel-control\" data-slide=\"prev\">\n        <span class=\"glyphicon glyphicon-chevron-left\"></span>\n    </a>\n    <a href=\"#carousel-example-generic{{id}}\" class=\"right carousel-control\" data-slide=\"next\">\n        <span class=\"glyphicon glyphicon-chevron-right\"></span>\n    </a>\n</div>\n</ng-template>\n\n<ng-template #DropDown>\n<select onchange=\"changeValue($(this).val(),'cards')\" style=\"{{css}}\">\n    {{#options}}\n    <option value=\"{{displayvalue}}\">{{displayname}}</option>\n    {{/options}}\n</select>\n</ng-template>\n\n<ng-template #List>\n<ul style=\"{{css}}\">\n    {{#options}}\n    <li>{{displayname}}</li>\n    {{/options}}\n\n</ul>\n</ng-template>\n\n<ng-template #AnchorLink>\n<a data-inp-val=\"{{inp_val}}\" style=\"{{css}}\">{{value}}</a>\n</ng-template>\n\n<ng-template #Image>\n<img style=\"{{css}}\" src=\"{{value}}\">\n</ng-template>\n\n<ng-template #LineBreak>\n{{#value}}\n<br>\n{{/value}}\n</ng-template>\n\n<ng-template #InputBox>\n<input type=\"{{type}}{{^type}}text{{/type}}\" style=\"{{css}}\" value=\"{{value}}\">\n</ng-template>\n\n<ng-template #TextArea>\n<textarea style=\"{{css}}\">{{value}}</textarea>\n</ng-template>\n\n<ng-template #Button>\n<button type=\"button\" data-inp-val=\"{{inp_val}}\" style=\"{{css}}\">{{value}}</button>\n</ng-template>\n\n<ng-template #ListwithAnchors>\n<ul style=\"{{css}}\">\n    {{#options}}\n    <li><a data-inp-val=\"{{displayvalue}}\" style=\"{{css}}\">{{displayname}}</a></li>\n    {{/options}}\n</ul>\n</ng-template>\n\n<ng-template #Form>\n<form id=\"api_form{{id}}\">\n    {{#val}}\n        <p style=\"{{css}}\">{{label}}:</p>\n        {{#input_box}}\n            <input name={{name}} type=\"{{type}}{{^type}}text{{/type}}\" style=\"{{css}}\" value=\"{{value}}\" required>\n        {{/input_box}}\n\n        {{#text_area}}\n            <textarea name={{name}} style=\"{{css}}\" required>{{value}}</textarea>\n        {{/text_area}}\n        <br><br>\n    {{/val}}\n    <button type=\"button\" id=\"form_submit{{id}}\" form-inp-val=\"{{form_action}}\">Submit</button>\n    <br>\n</form>\n</ng-template>\n\n\n<ng-template #Table>\n<table>\n    <thead>\n        <tr>\n            {{#header}}\n            <td>\n                {{value}}\n            </td>\n            {{/header}}\n        </tr>\n    </thead>\n    <tbody>\n        {{#rows}}\n        <tr>\n            {{#row}}\n            <td>\n                {{value}}\n            </td>\n            {{/row}}\n        </tr>\n        {{/rows}}\n    </tbody>\n</table>\n</ng-template>\n\n\n<ng-template #Spell>\n    <p>\n        <b>Did you mean</b>\n    </p>\n    <p>\n        <i style=\"color:red\">{{value}}</i>\n    </p>\n    <br>\n    <a href=\"#\" data-inp-val=\"Yes\">Yes</a>\n    <a href=\"#\" data-inp-val=\"No\">No</a>\n</ng-template>\n\n<ng-template #LiveAgent>\n<p>It seems you are not getting satisfactory response.\n    <br>\n    Click on the image to talk to a Live Agent.\n</p>\n<a target=\"_blank\" href=\"{{value}}\">\n    <img height=\"60\" width=\"60\" src=\"{{base_url}}static/images/live_agent.jpg\" />\n</a>\n</ng-template>\n\n\n<ng-template #Rank5>\n<p>It seems you are not getting satisfactory response.\n    <br>\n    Please provide feedback at below\n</p>\n<a target=\"_blank\" href=\"{{value}}\">\n    <img height=\"60\" width=\"60\" src=\"{{base_url}}static/images/live_agent.jpg\" />\n</a>\n</ng-template>\n\n<ng-template #Video>\n<video width=\"320\" height=\"240\" controls><source style=\"{{css}}\" src=\"{{value}}\" type=\"video/mp4\"></video>\n</ng-template>\n\n<ng-template #notice>\n{{#notices}}\n    <div id=\"notice_{{notice_id}}\" class=\"notice\">\n        <h4> {{notice}} </h4>\n        {{#detail}}\n        <form>\n            {{{detail}}}\n            <br>\n            <a class=\"notice btn btn-info\" data-baseurl=\"{{base_url}}\"\n             data-noticeid=\"{{notice_id}}\"> Submit </a>\n            <div class=\"msg\">\n            </div>\n        </form>\n        {{/detail}}\n        <hr>\n    </div>\n{{/notices}}\n</ng-template> -->\n"

/***/ }),

/***/ "./src/app/html-message/html-message.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return HtmlMessageComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__botsData_bot_message__ = __webpack_require__("./src/app/botsData/bot-message.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var HtmlMessageComponent = /** @class */ (function () {
    function HtmlMessageComponent() {
        this.message = '';
    }
    HtmlMessageComponent.prototype.ngOnInit = function () {
        this.message = this.bot_message.answer;
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["D" /* Input */])(),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_1__botsData_bot_message__["a" /* BotMessage */])
    ], HtmlMessageComponent.prototype, "bot_message", void 0);
    HtmlMessageComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
            selector: 'app-html-message',
            template: __webpack_require__("./src/app/html-message/html-message.component.html"),
            styles: [__webpack_require__("./src/app/html-message/html-message.component.css")]
        }),
        __metadata("design:paramtypes", [])
    ], HtmlMessageComponent);
    return HtmlMessageComponent;
}());



/***/ }),

/***/ "./src/app/myservice.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ConfigService; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};

var ConfigService = /** @class */ (function () {
    function ConfigService() {
    }
    ConfigService.prototype.setOption = function (url) {
        this.config = url;
    };
    ConfigService.prototype.getConfig = function () {
        return this.config;
    };
    ConfigService = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["A" /* Injectable */])()
    ], ConfigService);
    return ConfigService;
}());



/***/ }),

/***/ "./src/environments/environment.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return environment; });
// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.
var environment = {
    production: false
};


/***/ }),

/***/ "./src/main.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_platform_browser_dynamic__ = __webpack_require__("./node_modules/@angular/platform-browser-dynamic/esm5/platform-browser-dynamic.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__app_app_module__ = __webpack_require__("./src/app/app.module.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__environments_environment__ = __webpack_require__("./src/environments/environment.ts");




if (__WEBPACK_IMPORTED_MODULE_3__environments_environment__["a" /* environment */].production) {
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_15" /* enableProdMode */])();
}
Object(__WEBPACK_IMPORTED_MODULE_1__angular_platform_browser_dynamic__["a" /* platformBrowserDynamic */])().bootstrapModule(__WEBPACK_IMPORTED_MODULE_2__app_app_module__["a" /* AppModule */])
    .catch(function (err) { return console.log(err); });


/***/ }),

/***/ 0:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__("./src/main.ts");


/***/ })

},[0]);
//# sourceMappingURL=main.bundle.js.map