from django.conf.urls import url, include
from .views import home

app_name="brain"

urlpatterns = [
    url(r'^$', home, name='brain-home')
]
