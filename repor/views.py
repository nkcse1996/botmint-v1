from django.shortcuts import render
from chat.models import ChatHistory
from django.contrib.auth.decorators import login_required
from config.utils import get_bot_db
from django.conf import settings
from django.http import JsonResponse
from registration.models import Bot


@login_required
def home(request, bot_id):
    template = 'index.html'
    return render(request, template, {'bot_id': bot_id,'fields': ['_id',
    'botid', 'u_nm', 'scr', 'chats', 'u_email', 'sn_id', 'fb_type', 'dt',
    'u_id', 'fb_msg']})


@login_required
def chat_data(request, bot_id):
    try:
        bot = Bot.objects.get(pk=bot_id)
    except Exception as e:
        return JsonResponse({"msg": str(e), "data": {}})
    db_name = get_bot_db(bot)
    chat_history = settings.CON[db_name].ChatHistory
    draw = request.GET['draw']
    start = int(request.GET.get('start', 0))
    limit = int(request.GET.get('length', 10))
    objects = []
    result_set = chat_history.find()
    for row in result_set:
        objects.append((str(row['_id']), row['botid'], row['u_nm'], row['scr']
        , row['chats'], row['u_email'], row['sn_id'], row['fb_type'], row['dt'],
         row['u_id'],row['fb_msg']))

    filtered_count = result_set.count()
    total_count = chat_history.find().count()
    return JsonResponse({
        "sEcho": draw,
        "iTotalRecords": total_count,
        "iTotalDisplayRecords": filtered_count,
        "aaData": objects,
    })
