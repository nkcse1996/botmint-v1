from django.conf.urls import url
from . views import home, chat_data
app_name = "repor"
urlpatterns = [
    url(r'^(?P<bot_id>\d+)/$', home, name='home'),
    url(r'^(?P<bot_id>\d+)/chatdata/$',chat_data, name='chat_data'),

]
