export class BotMessage {
      public feedback_type: string;
      public suggestions: Array<any>;
      public counter: number;
      public error: string;
      public context: any;
      public answer: any;
      public score:number;
}
