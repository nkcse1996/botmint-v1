export const botsData = [
{
    id: 1,
    displayName: "Arya Stark",
    avatar: "https://pbs.twimg.com/profile_images/894833370299084800/dXWuVSIb.jpg",
    status: "Online"
},
{
    id: 2,
    displayName: "Daenerys Targaryen",
    avatar: "https://68.media.tumblr.com/avatar_d28d7149f567_128.png",
    status: "Busy"
},
{
    id: 3,
    displayName: "Eddard Stark",
    avatar: "https://pbs.twimg.com/profile_images/600707945911844864/MNogF757_400x400.jpg",
    status: "Offline"
},
{
    id: 4,
    displayName: "Hodor",
    avatar: "https://pbs.twimg.com/profile_images/378800000449071678/27f2e27edd119a7133110f8635f2c130.jpeg",
    status: "Offline"
},
{
    id: 5,
    displayName: "Jaime Lannister",
    avatar: "https://pbs.twimg.com/profile_images/378800000243930208/4fa8efadb63777ead29046d822606a57.jpeg",
    status: "Busy"
},
{
    id: 6,
    displayName: "John Snow",
    avatar: "https://pbs.twimg.com/profile_images/3456602315/aad436e6fab77ef4098c7a5b86cac8e3.jpeg",
    status:" Busy"
},
{
    id: 7,
    displayName: "Lorde Petyr 'Littlefinger' Baelish",
    avatar: "http://68.media.tumblr.com/avatar_ba75cbb26da7_128.png",
    status: "Offline"
},
{
    id: 8,
    displayName: "Sansa Stark",
    avatar: "http://pm1.narvii.com/6201/dfe7ad75cd32130a5c844d58315cbca02fe5b804_128.jpg",
    status: "Online"
},
{
    id: 9,
    displayName: "Theon Greyjoy",
    avatar: "https://thumbnail.myheritageimages.com/502/323/78502323/000/000114_884889c3n33qfe004v5024_C_64x64C.jpg",
    status: "Away"
}
];
