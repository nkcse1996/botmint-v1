import {Injectable} from '@angular/core';

@Injectable()
export class ConfigService {

    private config:string;

    setOption(url:string) {
        this.config = url;
    }

    getConfig() {
        return this.config;
    }
}
