import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  bots: Array<any>;
  user_image : string;
  
  ngOnInit(){
    this.bots = window['bot_info'] ? window['bot_info']['data'] : [];
    this.user_image = window['user_image'];
  }

}
