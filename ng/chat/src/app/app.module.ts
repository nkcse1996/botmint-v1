import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppComponent } from './app.component';
import { DetailsFormComponent } from './details-form/details-form.component';
import { ChatComponent } from './chat/chat.component';
import { RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { ConfigService } from './myservice';
import { FullChatComponent } from './full-chat/full-chat.component';
import { HtmlMessageComponent } from './html-message/html-message.component';


@NgModule({
  declarations: [
    AppComponent,
    DetailsFormComponent,
    ChatComponent,
    FullChatComponent,
    HtmlMessageComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpClientModule,
    RouterModule.forRoot([
      {
        path: 'detailsform',
        component: DetailsFormComponent
      },
      {
        path: 'fullchat',
        component: FullChatComponent
      },
      {
        path: '',
        component: ChatComponent
      }
    ])
  ],
  providers: [ConfigService],
  bootstrap: [AppComponent]
})
export class AppModule { }
