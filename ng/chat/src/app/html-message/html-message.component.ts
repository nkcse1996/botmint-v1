import { Component, OnInit, Input} from '@angular/core';
import { BotMessage } from "../botsData/bot-message";
@Component({
  selector: 'app-html-message',
  templateUrl: './html-message.component.html',
  styleUrls: ['./html-message.component.css']
})
export class HtmlMessageComponent implements OnInit {

  message: string;
  constructor(){
    this.message = '';
  }

  @Input()
  public bot_message: BotMessage;


  ngOnInit() {
        this.message = this.bot_message.answer;
  }
}
