import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HtmlMessageComponent } from './html-message.component';

describe('HtmlMessageComponent', () => {
  let component: HtmlMessageComponent;
  let fixture: ComponentFixture<HtmlMessageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HtmlMessageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HtmlMessageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
