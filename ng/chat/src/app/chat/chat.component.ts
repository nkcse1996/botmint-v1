import { Component, OnInit, Input, ViewChildren, ViewChild, HostListener } from '@angular/core';

import { botsData } from "../botsData/data";
import { UserStatus } from "../botsData/user-status.enum";
import { User } from "../botsData/user";
import { Window } from '../botsData/window';
import { Message } from "../botsData/message";
// import { ChatAdapter } from "./botsData/chat-adapter";
import { Localization } from "../botsData/localization";
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { BotMessage } from "../botsData/bot-message";
import { DetailsFormComponent } from '../details-form/details-form.component';
import { ConfigService } from './../myservice';


@Component({
  selector: 'app-chat',
  templateUrl: './chat.component.html',
  styleUrls: ['./chat.component.css']
})

export class ChatComponent {

  url: string;

  @ViewChild(DetailsFormComponent) detailurl;

  constructor(private http: HttpClient, private service: ConfigService) {
    this.url = this.service.getConfig();
    this.x = 300;
    this.y = 100;
    this.px = 0;
    this.py = 0;
    this.width = 300;
    this.height = 360;
    this.draggingCorner = false;
    this.dragging = false;
    this.minArea = 20000
  }

  /////////////open form/////////////////

  formToggle: boolean = false;

  openForm(event) {
    this.formToggle = !this.formToggle;
  }

  onNotificationChatWindow() {
    alert("No new Notification");
  }
  onSpeakerChatWindow() {
    alert("Speaker is ON");
  }


  //////////////////////////////////

  // Exposes the enum for the template
  UserStatus = UserStatus;
  private users: User[];
  public message: Message;
  @Input()
  public bots: Array<any> = botsData;

  @Input()
  public user_image: string;

  profileAratar: string = "Profile_avatar";

  private friendsListWidth: number = 262;

  private viewPortTotalArea: number;

  @Input()
  public localization: Localization;

  @Input()
  public persistWindowsState: boolean = true;

  @ViewChildren('chatMessages') chatMessageClusters: any;

  @ViewChildren('chatWindowInput') chatWindowInputs: any;

  windows: Window[] = [];

  @Input()
  public isCollapsed: boolean = false;
  private windowSizeFactor: number = 320;
  @Input()
  public searchPlaceholder: string = "Search";

  @Input()
  public userId: any;

  public searchInput: string = '';

  private get localStorageKey(): string {
    return `ng-chat-users-${this.userId}`; // Appending the user id so the state is unique per user in a computer.
  };

  private prepareChatMessage(message: Message){

    return message;
  }

  // Handles received messages by the adapter
  private onMessageReceived(user: User, message: Message) {
    if (user && message) {
      let chatWindow = this.openChatWindow(user);

      if (!chatWindow[1]) {
        message = this.prepareChatMessage(message);
        chatWindow[0].messages.push(message);

        this.scrollChatWindowToBottom(chatWindow[0]);
      }

      // this.emitMessageSound(chatWindow[0]);
    }
  }


  onChatTitleClicked(event: any): void {
    if (!this.dragging) {
      this.isCollapsed = !this.isCollapsed;
    }
  }

  // Toggles a chat window visibility between maximized/minimized
  onChatWindowClicked(window: Window): void {
    if (!this.dragging) {
      window.isCollapsed = !window.isCollapsed;
      this.scrollChatWindowToBottom(window);
    }

  }

  // Scrolls a chat window message flow to the bottom
  private scrollChatWindowToBottom(window: Window): void {
    if (!window.isCollapsed) {
      let windowIndex = this.windows.indexOf(window);

      setTimeout(() => {
        if (this.chatMessageClusters)
          this.chatMessageClusters.toArray()[windowIndex].nativeElement.scrollTop = this.chatMessageClusters.toArray()[windowIndex].nativeElement.scrollHeight;
      });
    }
  }


  private openChatWindow(user: User, focusOnNewWindow: boolean = false): [Window, boolean] {
    // Is this window opened?
    let openedWindow = this.windows.find(x => x.chattingTo.id == user.id);

    if (!openedWindow) {
      let newChatWindow: Window = {
        chattingTo: user,
        messages: [],
        hasFocus: false // This will be triggered when the 'newMessage' input gets the current focus
      };

      this.windows.unshift(newChatWindow);

      // Is there enough space left in the view port ?
      if (this.windows.length * this.windowSizeFactor >= this.viewPortTotalArea - this.friendsListWidth) {
        this.windows.pop();
      }

      this.updateWindowsState(this.windows);

      if (focusOnNewWindow)
        this.focusOnWindow(newChatWindow);

      return [newChatWindow, true];
    }
    else {
      // Returns the existing chat window
      return [openedWindow, false];
    }


  }

  // Saves current windows state into local storage if persistence is enabled
  private updateWindowsState(windows: Window[]): void {
    if (this.persistWindowsState) {
      let usersIds = windows.map((w) => {
        return w.chattingTo.id;
      });

      localStorage.setItem(this.localStorageKey, JSON.stringify(usersIds));
    }
  }


  // Focus on the input element of the supplied window
  private focusOnWindow(window: Window, callback: Function = () => { }): void {
    let windowIndex = this.windows.indexOf(window);

    if (windowIndex >= 0) {
      setTimeout(() => {
        let messageInputToFocus = this.chatWindowInputs.toArray()[windowIndex];

        messageInputToFocus.nativeElement.focus();

        callback();
      });
    }
  }

  // Gets closest open window if any. Most recent opened has priority (Right)
  private getClosestWindow(window: Window): Window | undefined {
    let index = this.windows.indexOf(window);

    if (index > 0) {
      return this.windows[index - 1];
    }
    else if (index == 0 && this.windows.length > 1) {
      return this.windows[index + 1];
    }
  }

  // Closes a chat window via the close 'X' button
  onCloseChatWindow(window: Window): void {
    let index = this.windows.indexOf(window);

    this.windows.splice(index, 1);

    this.updateWindowsState(this.windows);
  }
  public show:boolean = true;

  onCloseChatWindowpeople(window: Window): void {
    this.show = !this.show;

  }
  //////////////////////received massages/////////////////////////////////////

  sendMessage(message: Message): void {
    setTimeout(() => {
      let replyMessage = new Message();

      replyMessage.fromId = message.toId;
      replyMessage.toId = message.fromId;
      let bot = this.bots.find(x => x.id == replyMessage.fromId);
      const body = new HttpParams().set('message', message.message);
      const headers = new HttpHeaders().set('Content-Type', 'application/x-www-form-urlencoded');


      this.http.post('/chat/message/'+ bot.id + "/", body.toString(), {headers}).subscribe(
                data => {
                  console.log(data);
                  var msg = '';
                  if(data['error']){
                    msg  = data['error'];
                  }
                  else{
                    msg = data['resp'];
                  }

                  if(typeof(msg) != "string"){
                    let bot_message = Object.create(BotMessage.prototype);
                    msg = Object.assign(bot_message, msg);
                  }
                  replyMessage.message = msg;
                  this.onMessageReceived(bot, replyMessage);
                },
                err => {
                  console.log("Error occured");
                });

    }, 1000);
  }

  onChatInputTyped(event: any, window: Window): void {
    switch (event.keyCode) {
      case 13:
        if (window.newMessage && window.newMessage.trim() != "") {
          let message = new Message();

          message.fromId = this.userId;
          message.toId = window.chattingTo.id;
          message.message = window.newMessage;

          window.messages.push(message);

          this.sendMessage(message);

          window.newMessage = ""; // Resets the new message input

          this.scrollChatWindowToBottom(window);


        }
        break;

    }
  }

  // [Localized] Returns the status descriptive title

  // Returns the total unread messages from a chat window. TODO: Could use some Angular pipes in the future
  unreadMessagesTotal(window: Window): string {
    if (window) {
      if (window.hasFocus) {
        this.markMessagesAsRead(window.messages);
      }
      else {
        let totalUnreadMessages = window.messages.filter(x => x.fromId != this.userId && !x.seenOn).length;

        if (totalUnreadMessages > 0) {

          if (totalUnreadMessages > 99)
            return "99+";
          else
            return String(totalUnreadMessages);
        }
      }
    }

    // Empty fallback.
    return "";
  }


  // Toggles a window focus on the focus/blur of a 'newMessage' input
  toggleWindowFocus(window: Window): void {
    window.hasFocus = !window.hasFocus;
  }

  // Marks all messages provided as read with the current time.
  private markMessagesAsRead(messages: Message[]): void {
    let currentDate = new Date();

    messages.forEach((msg) => {
      msg.seenOn = currentDate;
    });
  }

  // Asserts if a user avatar is visible in a chat cluster
  isAvatarVisible(window: Window, message: Message, index: number): boolean {
    if (message.fromId != this.userId) {
      if (index == 0) {
        return true; // First message, good to show the thumbnail
      }
      else {
        // Check if the previous message belongs to the same user, if it belongs there is no need to show the avatar again to form the message cluster
        if (window.messages[index - 1].fromId != message.fromId) {
          return true;
        }
      }
    }

    return false;
  }

  /////////////////for extendiblity of div//////////////////////////

  x: number;
  y: number;
  px: number;
  py: number;
  width: number;
  height: number;
  minArea: number;
  draggingCorner: boolean;
  dragging: boolean;
  draggingWindow: boolean;
  resizer: Function;



  area() {
    return this.width * this.height;
  }


  topLeftResize(offsetX: number, offsetY: number) {
    this.x += offsetX;
    this.y += offsetY;
    this.width -= offsetX;
    this.height -= offsetY;
  }


  onCornerClick(event: MouseEvent, resizer?: Function) {
    this.draggingCorner = true;
    this.dragging = false;
    this.px = event.clientX;
    this.py = event.clientY;
    this.resizer = resizer;
    event.preventDefault();
    event.stopPropagation();
  }

  @HostListener('document:mousemove', ['$event'])
  onCornerMove(event: MouseEvent) {
    if (!this.draggingCorner) {
      return;
    }
    this.dragging = true;
    let offsetX = event.clientX - this.px;
    let offsetY = event.clientY - this.py;

    let lastX = this.x;
    let lastY = this.y;
    let pWidth = this.width;
    let pHeight = this.height;

    this.resizer(offsetX, offsetY);
    if (this.area() < this.minArea) {
      this.x = lastX;
      this.y = lastY;
      this.width = pWidth;
      this.height = pHeight;
    }

    this.px = event.clientX;
    this.py = event.clientY;
  }

  @HostListener('document:mouseup', ['$event'])
  onCornerRelease(event: MouseEvent) {
    this.draggingWindow = false;
    this.draggingCorner = false;
  }

}
