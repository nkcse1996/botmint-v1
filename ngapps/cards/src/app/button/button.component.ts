import { Component } from '@angular/core';
import { Card } from '../extras/card';

@Component({
  selector: 'app-button',
  templateUrl: './button.component.html',
  styleUrls: ['./button.component.css']
})
export class ButtonComponent extends Card {
  data = {
    name: '',
    disabled: '',
    btnType: '',
    styles: ''
  };
}
