import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppComponent } from './app.component';
import { CardSelectorComponent } from './card-selector/card-selector.component';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { InputTextComponent } from './input-text/input-text.component';
import { Card } from './extras/card';
import { FormModel } from './extras/model';
import { WindowModel } from './extras/window-model';
import { TextAreaComponent } from './text-area/text-area.component';
import { SharedServiceService } from './shared-service.service';
import { ImageComponent } from './image/image.component';
import { VideoComponent } from './video/video.component';
import { AnchorComponent } from './anchor/anchor.component';
import { TextComponent } from './text/text.component';
import { ButtonComponent } from './button/button.component';
import { AnchorListComponent } from './anchor-list/anchor-list.component';
import { ChecklistComponent } from './checklist/checklist.component';
import { DropdownComponent } from './dropdown/dropdown.component';
import { DynamicAnchorListComponent } from './dynamic-anchor-list/dynamic-anchor-list.component';
import { DynamicChecklistComponent } from './dynamic-checklist/dynamic-checklist.component';
import { DynamicDropdownComponent } from './dynamic-dropdown/dynamic-dropdown.component';
import { RadioComponent } from './radio/radio.component';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {MatDialogModule} from '@angular/material/dialog';
import { CardsComponent } from './cards/cards.component';



@NgModule({
  declarations: [
    AppComponent,
    CardSelectorComponent,
    TextComponent,
    InputTextComponent,
    TextAreaComponent,
    ImageComponent,
    VideoComponent,
    AnchorComponent,
    ButtonComponent,
    AnchorListComponent,
    ChecklistComponent,
    DropdownComponent,
    DynamicAnchorListComponent,
    DynamicChecklistComponent,
    DynamicDropdownComponent,
    RadioComponent,
    CardsComponent,

  ],
  imports: [
    FormsModule,
    CommonModule,
    BrowserModule,
    BrowserAnimationsModule,
    MatDialogModule
  ],
  providers: [
        FormModel,
        WindowModel,
        SharedServiceService
              ],
  bootstrap: [AppComponent],
  entryComponents: [AppComponent,
  CardSelectorComponent,
  TextComponent,
  InputTextComponent,
  TextAreaComponent,
  ImageComponent,
  VideoComponent,
  AnchorComponent,
  ButtonComponent,
  AnchorListComponent,
  ChecklistComponent,
  DropdownComponent,
  DynamicAnchorListComponent,
  DynamicChecklistComponent,
  DynamicDropdownComponent,
  RadioComponent,
CardsComponent]
})

export class AppModule { }
