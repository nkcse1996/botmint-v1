import { Component } from '@angular/core';
import { Card } from '../extras/card';

@Component({
  selector: 'app-text',
  templateUrl: './text.component.html',
  styleUrls: ['./text.component.css']
})
export class TextComponent extends Card {
  data = {
    text: '',
    styles: ''
  };
}
