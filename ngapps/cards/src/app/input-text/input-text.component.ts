import { Component, ViewChild, OnInit, Input, EventEmitter } from '@angular/core';
import { Card } from '../extras/card';
import { CardSelectorComponent } from '../card-selector/card-selector.component';
import { WindowModel } from '../extras/window-model';
import { SharedServiceService } from '../shared-service.service';
@Component({
  selector: 'app-input-text',
  templateUrl: './input-text.component.html',
  styleUrls: ['./input-text.component.css']
})

export class InputTextComponent extends Card implements OnInit {

  showData: boolean;
  constructor(public windowModel: WindowModel, public service: SharedServiceService) {
    super();
  }
  id: string;
  label: string;
  name: string;
  type: string;
  placeholder: string;

  data = {
    label: '',
    name: '',
    placeholder: '',
    styles: ''
  };

  createElement(id: string) {
    debugger;
    this.type = this.windowModel.data[id]["type"];
    this.label = this.windowModel.data[id]["label"];
    this.name = this.windowModel.data[id]["name"];
    this.placeholder = this.windowModel.data[id]["placeholder"];
    console.log(this.name);

  }
  ngOnInit() {
    this.service.change.subscribe(showData => {
      this.showData = showData;
    });

  }
}
