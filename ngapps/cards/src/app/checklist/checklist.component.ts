import { Component } from '@angular/core';
import { Card } from '../extras/card';

@Component({
  selector: 'app-checklist',
  templateUrl: './checklist.component.html',
  styleUrls: ['./checklist.component.css']
})
export class ChecklistComponent extends Card {

  public data: any[] = [];

  addComponent() {
    this.data.push({
      name: '',
      value: '',
      content: '',
      checked: '',
      styles: ''
    });
  }

  ngOnInit(){
    this.addComponent();
    if(!this.data.length){
      delete this.window_model.data[this._id];
      this._ref.destroy();
    }
  }

  closeItemForm(i:number){
    this.data.splice(i,1);
  }
}
