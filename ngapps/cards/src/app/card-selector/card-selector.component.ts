import { Component, OnInit, ViewChild, ComponentFactoryResolver, ViewContainerRef, Output, EventEmitter }
  from '@angular/core';
import { WindowModel } from '../extras/window-model';
import { Cardlist } from './card-selector-data';
import { SharedServiceService } from '../shared-service.service';
import { TextComponent } from '../text/text.component';
import { InputTextComponent } from '../input-text/input-text.component';
import { TextAreaComponent } from '../text-area/text-area.component';
import { ImageComponent } from '../image/image.component';
import { VideoComponent } from '../video/video.component';
import { AnchorComponent } from '../anchor/anchor.component';
import { ButtonComponent } from '../button/button.component';
import { AnchorListComponent } from '../anchor-list/anchor-list.component';
import { ChecklistComponent } from '../checklist/checklist.component';
import { DropdownComponent } from '../dropdown/dropdown.component';
import { RadioComponent } from '../radio/radio.component';
import { DynamicAnchorListComponent } from '../dynamic-anchor-list/dynamic-anchor-list.component';
import { DynamicChecklistComponent } from '../dynamic-checklist/dynamic-checklist.component';
import { DynamicDropdownComponent } from '../dynamic-dropdown/dynamic-dropdown.component';

@Component({
  selector: 'app-card-selector',
  templateUrl: './card-selector.component.html',
  styleUrls: ['./card-selector.component.css']
})
export class CardSelectorComponent {
  modelid: any;
  cards:any[] = [];

  @ViewChild('parent', { read: ViewContainerRef })
  container: ViewContainerRef;
  showData: boolean = true;
  btn: boolean;
  optionSelected: string = 'Select Option';
  options = [
    new Cardlist(0, 'Select Option'),
    new Cardlist(1, 'Text'),
    new Cardlist(1, 'Input'),
    new Cardlist(2, 'TextArea'),
    new Cardlist(3, 'Image'),
    new Cardlist(4, 'Video'),
    new Cardlist(5, 'Anchor'),
    new Cardlist(6, 'Button'),
    new Cardlist(7, 'AnchorList'),
    new Cardlist(8, 'CheckList'),
    new Cardlist(9, 'Dropdown'),
    new Cardlist(10, 'RadioButton'),
    new Cardlist(11, 'DynamicAnchorList'),
    new Cardlist(12, 'DynamicCheckList'),
    new Cardlist(13, 'DynamicDropdown'),
  ];

  components = {
    Text: TextComponent,
    Input: InputTextComponent,
    TextArea: TextAreaComponent,
    Image: ImageComponent,
    Video: VideoComponent,
    Anchor: AnchorComponent,
    Button: ButtonComponent,
    AnchorList: AnchorListComponent,
    CheckList: ChecklistComponent,
    Dropdown: DropdownComponent,
    RadioButton: RadioComponent,
    DynamicAnchorList: DynamicAnchorListComponent,
    DynamicCheckList: DynamicChecklistComponent,
    DynamicDropdown: DynamicDropdownComponent,
  }
  constructor(private _cfr: ComponentFactoryResolver,
    public windowModel: WindowModel,
    public service: SharedServiceService,
  ) {
  }

  id: string;
  event: string;
  onOptionsSelected(event) {
    this.btn = true;
    if (event != 'Select Option') {
      var _comp = this._cfr.resolveComponentFactory(this.components[event]);
      var _component = this.container.createComponent(_comp);
      _component.instance['_ref'] = _component;
      _component.instance['_id'] = this.getId();
      _component.instance['window_model'] = this.windowModel;
      _component.instance['data']['type'] = event;
      this.windowModel.data[_component.instance['_id']] = {
        type:event, details: _component.instance['data']
      }
    }
  }

  saveData() {
    console.log(this.windowModel.data);
    this.cards = this.sortedValues(this.windowModel.data);
  }



  getId() {
    var date = new Date();
    var components = [
      date.getFullYear(),
      date.getMonth(),
      date.getDate(),
      date.getHours(),
      date.getMinutes(),
      date.getSeconds(),
      date.getMilliseconds()
    ];
    return components.join("");
  }

  sortedValues(jsObj){
    var keys = Object.keys(jsObj);
    keys.sort();
    var arrayValues = [];
    for (var i = 0; i < keys.length; i+= 1) {
        arrayValues.push(jsObj[keys[i]]);
    }
    return arrayValues;
  }

}
