import { Component } from '@angular/core';
import { Card } from '../extras/card';

@Component({
  selector: 'app-dynamic-dropdown',
  templateUrl: './dynamic-dropdown.component.html',
  styleUrls: ['./dynamic-dropdown.component.css']
})

export class DynamicDropdownComponent extends Card {
  data = {
    url: '',
    key: '',
    value: '',
    styles: ''
  };
}
