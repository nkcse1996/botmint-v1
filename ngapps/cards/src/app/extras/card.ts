import { WindowModel } from './window-model';
import { OnInit } from '@angular/core';


export abstract class Card implements OnInit{
  _ref: any;
  _id: string;
  window_model: WindowModel;
  constructor() { }
  data: any;
  closeForm() {
    delete this.window_model.data[this._id];
    this._ref.destroy();
  }
  ngOnInit(){

  }
  closeItemForm(i:number){
    this.data.splice(i,1);
    if(!this.data.length){
      delete this.window_model.data[this._id];
      this._ref.destroy();
    }
  }
}
