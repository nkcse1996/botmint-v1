import { Injectable, Output, EventEmitter } from '@angular/core';

@Injectable()
export class SharedServiceService {
  @Output() change: EventEmitter<boolean> = new EventEmitter();

  constructor() { }
  textData: {};
  sendTextData(textData: {}) {
    this.textData = textData;
  }

  getTextData() {
    return this.textData;
  }

  showData: boolean;
  showdata() {
    debugger;
    this.showData = !this.showData;
    this.change.emit(this.showData);
  }
}
