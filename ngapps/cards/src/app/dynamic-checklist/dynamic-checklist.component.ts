import { Component } from '@angular/core';
import { Card } from '../extras/card';

@Component({
  selector: 'app-dynamic-checklist',
  templateUrl: './dynamic-checklist.component.html',
  styleUrls: ['./dynamic-checklist.component.css']
})

export class DynamicChecklistComponent extends Card {
    data = {
      checked: '',
      url: '',
      key: '',
      value: '',
      styles: ''
    };
}
