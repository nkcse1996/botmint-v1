import { Component } from '@angular/core';
import { Card } from '../extras/card';

@Component({
  selector: 'app-text-area',
  templateUrl: './text-area.component.html',
  styleUrls: ['./text-area.component.css']
})
export class TextAreaComponent extends Card {
  data = {
      name: '',
      rows: '',
      columns: '',
      discription: '',
      styles: ''
    }
}
