import { Component, OnInit } from '@angular/core';
import { Card } from '../extras/card';

@Component({
  selector: 'app-anchor-list',
  templateUrl: './anchor-list.component.html',
  styleUrls: ['./anchor-list.component.css']
})
export class AnchorListComponent extends Card {
  public data: any[] = [];
  addComponent() {
    this.data.push({
      href: '',
      content: '',
      download: '',
      styles: ''
    });
  }

  ngOnInit(){
    this.addComponent();
  }

}
