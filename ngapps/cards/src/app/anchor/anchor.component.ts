import { Component } from '@angular/core';
import { Card } from '../extras/card';

@Component({
  selector: 'app-anchor',
  templateUrl: './anchor.component.html',
  styleUrls: ['./anchor.component.css']
})
export class AnchorComponent extends Card {
  data = {
    href: '',
    content: '',
    download: '',
    styles: ''
  };
}
