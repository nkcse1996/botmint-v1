import { Component } from '@angular/core';
import { Card } from '../extras/card';

@Component({
  selector: 'app-dropdown',
  templateUrl: './dropdown.component.html',
  styleUrls: ['./dropdown.component.css']
})

export class DropdownComponent extends Card {

  public data: any[] = [];

  addComponent() {
    this.data.push({
      value: '',
      content: '',
      styles: ''
    });
  }

  ngOnInit(){
    this.addComponent();
  }

}
