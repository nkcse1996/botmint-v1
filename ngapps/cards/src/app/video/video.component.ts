import { Component } from '@angular/core';
import { Card } from '../extras/card';

@Component({
  selector: 'app-video',
  templateUrl: './video.component.html',
  styleUrls: ['./video.component.css']
})
export class VideoComponent extends Card {
  data = {
    src: '',
    type: '',
    height: '',
    width: '',
    controls : '',
    autoplay : '',
    loop : '',
    styles: ''
  };
}
