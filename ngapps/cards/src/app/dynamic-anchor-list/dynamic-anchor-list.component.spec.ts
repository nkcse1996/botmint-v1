import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DynamicAnchorListComponent } from './dynamic-anchor-list.component';

describe('DynamicAnchorListComponent', () => {
  let component: DynamicAnchorListComponent;
  let fixture: ComponentFixture<DynamicAnchorListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DynamicAnchorListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DynamicAnchorListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
