import { Component } from '@angular/core';
import { Card } from '../extras/card';

@Component({
  selector: 'app-dynamic-anchor-list',
  templateUrl: './dynamic-anchor-list.component.html',
  styleUrls: ['./dynamic-anchor-list.component.css']
})
export class DynamicAnchorListComponent extends Card {
   data = {
      url: '',
      key: '',
      value: '',
      download: '',
      styles:''
    };
}
