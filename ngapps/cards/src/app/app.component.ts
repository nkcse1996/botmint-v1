import { Component, Inject } from '@angular/core';
import { CardSelectorComponent } from './card-selector/card-selector.component';
import { MatDialog } from '@angular/material';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'app';
  isdropdownshow: boolean = false;
  constructor(public dialog: MatDialog) { }
  openCardSelectorComponent() {
    this.isdropdownshow = true;
    if (this.isdropdownshow) {
      let dialogRef = this.dialog.open(CardSelectorComponent, {
        width: 'auto',
        height: 'auto',
        position: {
          top: '20px',
          bottom: '20px',
          left: '10%',
        }
      });
    }

  }

}
