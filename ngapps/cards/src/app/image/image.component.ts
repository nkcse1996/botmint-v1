import { Component, OnInit } from '@angular/core';
import { Card } from '../extras/card';

@Component({
  selector: 'app-image',
  templateUrl: './image.component.html',
  styleUrls: ['./image.component.css']
})
export class ImageComponent extends Card {

  data = {
    src: '',
    alt: '',
    styles: ''
  };

}
