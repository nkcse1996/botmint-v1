import { Component } from '@angular/core';
import { Card } from '../extras/card';

@Component({
  selector: 'app-radio',
  templateUrl: './radio.component.html',
  styleUrls: ['./radio.component.css']
})
export class RadioComponent extends Card {
    data = {
      name: '',
      value: '',
      content: '',
      checked: '',
      styles: ''
    };
  }
