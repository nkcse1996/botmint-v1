import { Component, TemplateRef, OnInit, AfterViewInit,
  ViewChild, ChangeDetectorRef, Input } from '@angular/core';

interface INamedTmplExample {
  name: string;
  template: TemplateRef<object>;
}

@Component({
  selector: 'app-cards',
  templateUrl: './cards.component.html',
  styleUrls: ['./cards.component.css']
})
export class CardsComponent implements OnInit {


  @ViewChild('Text')
  private Text: TemplateRef<object>;

  @ViewChild('Input')
  private Input: TemplateRef<object>;

  @ViewChild('TextArea')
  private TextArea: TemplateRef<object>;

  @ViewChild('Image')
  private Image: TemplateRef<object>;

  @ViewChild('Video')
  private Video: TemplateRef<object>;

  @ViewChild('Anchor')
  private Anchor: TemplateRef<object>;

  @ViewChild('Button')
  private Button: TemplateRef<object>;

  @ViewChild('AnchorList')
  private AnchorList: TemplateRef<object>;

  @ViewChild('CheckList')
  private CheckList: TemplateRef<object>;

  @ViewChild('Dropdown')
  private Dropdown: TemplateRef<object>;

  @ViewChild('RadioButton')
  private RadioButton: TemplateRef<object>;

  @ViewChild('DynamicAnchorList')
  private DynamicAnchorList: TemplateRef<object>;

  @ViewChild('DynamicCheckList')
  private DynamicCheckList: TemplateRef<object>;

  @ViewChild('DynamicDropdown')
  private DynamicDropdown: TemplateRef<object>;

  @Input()
  cards: any[]=[];

  tmpls: any = {};
  constructor(private cdRef:ChangeDetectorRef) {}

  ngOnInit() {

  }

  ngAfterViewInit() {
      this.tmpls = {
          'Text' : this.Text,
          'Input' : this.Input,
          'TextArea' : this.TextArea,
          'Image' : this.Image,
          'Video' : this.Video,
          'Anchor' : this.Anchor,
          'Button' : this.Button,
          'AnchorList' : this.AnchorList,
          'CheckList' : this.CheckList,
          'Dropdown' : this.Dropdown,
          'RadioButton' : this.RadioButton,
          'DynamicAnchorList' : this.DynamicAnchorList,
          'DynamicCheckList' : this.DynamicCheckList,
          'DynamicDropdown' : this.DynamicDropdown,
      }
      this.cdRef.detectChanges();
  }
}
