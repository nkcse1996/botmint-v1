from django.contrib import admin

# Register your models here.

from .models import Template, Bot

admin.site.register(Template)
admin.site.register(Bot)