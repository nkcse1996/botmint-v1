from django.conf.urls import url, include
# from .api import TemplateList, TemplateDetail
from .api import BotList, BotDetail, AdminBotList, TemplateViewSet
from .views import bots_index
from rest_framework import routers
router = routers.DefaultRouter()
router.register(r'', TemplateViewSet)

app_name = "registration"

# template_api_urls = [
#     url(r'^(?P<name>[0-9a-zA-Z_-]+)$', TemplateDetail.as_view(), name='template-detail'),
#     url(r'^$', TemplateList.as_view(), name='template-list')
# ]

bot_api_urls = [
    url(r'^(?P<pk>\d+)$', BotDetail.as_view(), name='bot-detail'),
    url(r'^$', BotList.as_view(), name='bot-list'),
    url(r'^(?P<username>[0-9a-zA-Z_-]+)$',
        AdminBotList.as_view(), name='adminbot-list')
]

api_urls = [
    url(r'^bots/', include(bot_api_urls)),
    url(r'^templates/', include(router.urls), name='templates'),
]

urlpatterns = [
    url(r'^api/', include(api_urls)),
    url(r'^$', bots_index, name='bots-registration')
]
