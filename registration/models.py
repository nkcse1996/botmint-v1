from django.db import models
from botmint.users.models import User
# Create your models here.
from django.conf import settings
from datetime import datetime, timedelta


def expiry():
    return datetime.now() + timedelta(days=settings.BOT_EXPIRY)


class Template(models.Model):
    name = models.CharField(max_length=20, blank=True, null=True)
    desc = models.TextField(blank=True, null=True)
    data = models.TextField()

    def __str__(self):
        return self.name


class Bot(models.Model):
    admin = models.ForeignKey(User, on_delete=models.CASCADE)
    name = models.CharField(max_length=20)
    desc = models.TextField()
    image = models.ImageField(blank=True, null=True,
                              upload_to='bot_pics/',
                              default='bot_pics/avatar.png')
    expiry = models.DateField(blank=True, null=True, default=expiry())
    status = models.BooleanField(default=True)
    domain = models.CharField(max_length=20)
    template = models.ForeignKey(Template, on_delete=models.CASCADE)
    attrs = models.TextField(default="{}")

    class Meta:
        unique_together = ('admin', 'name',)

    def __str__(self):
        return "Admin: {} , Name:{} ".format(self.admin.username, self.name)
