# Generated by Django 2.0.3 on 2018-05-31 09:56

import datetime
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('registration', '0006_auto_20180530_0623'),
    ]

    operations = [
        migrations.AlterField(
            model_name='bot',
            name='expiry',
            field=models.DateField(blank=True, default=datetime.datetime(2018, 6, 30, 9, 55, 52, 337889), null=True),
        ),
    ]
