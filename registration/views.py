from django.shortcuts import render
from .models import Bot
from django.contrib.auth.decorators import login_required
from .forms import AddBot
from django.db import transaction
from django.db import IntegrityError
# from django.db import IntegrityError
# Create your views here.


@login_required()
def bots_index(request):
    if request.is_ajax():
        template = 'bot_form.html'
    else:
        template = 'bots_index.html'
    bots = Bot.objects.filter(admin=request.user)
    dummy_cells = (3 - (bots.count()+1)%3)%3
    error = ''
    if request.POST:
        form = AddBot(request.POST)
        if form.is_valid():
            new_bot = form.save(commit=False)
            new_bot.admin = request.user
            try:
                with transaction.atomic():
                    new_bot.save()
                    form = AddBot()
            except IntegrityError:
                error = "Bot name can't be duplicated"
            except Exception as e:
                print(str(e))
                error = str(e)

    else:
        form = AddBot()

    return render(request, template, {'bots': bots, 'bot_form': form,
                'dummy_cells': range(dummy_cells), 'error': error})
