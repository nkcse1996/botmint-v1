from django.forms import ModelForm
from .models import Bot

class AddBot(ModelForm):
    class Meta:
        model = Bot
        fields = ['name', 'desc', 'image', 'domain', 'template']
    def __init__(self, *args, **kwargs):
        super(AddBot, self).__init__(*args, **kwargs) # Call to ModelForm constructor
        self.fields['desc'].widget.attrs['rows'] = 4
