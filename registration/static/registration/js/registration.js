$(function() {
  var dialog, form;

  dialog = $("#dialog-form").dialog({
    autoOpen: false,
    // height: 400,
    // width: 500,
    modal: true,
    minWidth: 400,
    position: {
      my: "center",
      at: "center",
      of: window
    },
  });

  alert_dialog = $("#error").alert({
    autoOpen: true,
    minWidth: 300,
    position: {
      my: "center",
      at: "center",
      of: window
    },
  });

  $("#cancel").button().on("click", function() {
    dialog.dialog("close");
  });

  $("#create-bot").button().on("click", function() {
    dialog.dialog("open");
  });
  $(".delete").button().on("click", function() {
    var delete_link = this.getAttribute('data-href')
    $("#dialog-confirm").dialog({
      resizable: false,
      height: "auto",
      width: 400,
      modal: true,
      buttons: {
        "Delete BOT": function() {
          $.ajax({
            type: "post",
            url: delete_link,
            data: '',
            beforeSend: function(xhr) {
              xhr.setRequestHeader('X-CSRFToken', $('input[name="csrfmiddlewaretoken"]').attr('value'))
            },
            success: function(data) {
              alert(data.msg);
              location.replace(location.href);
            },
            error: function(xhr, ajaxOptions, thrownError) {
              alert(xhr.status + " " + thrownError);
            }
          });

          $(this).dialog("close");
        },
        Cancel: function() {
          $(this).dialog("close");
        }
      }
    });
  });

  $(".copy").button().on("click", function() {
    debugger;
    var copy_link = this.getAttribute('data-href');
    $("#dialog-copy-confirm").dialog({
      resizable: false,
      height: "auto",
      width: 400,
      modal: true,
      buttons: {
        "COPY BOT": function() {
          $.ajax({
            type: "post",
            url: copy_link,
            data: '',
            beforeSend: function(xhr) {
              xhr.setRequestHeader('X-CSRFToken', $('input[name="csrfmiddlewaretoken"]').attr('value'))
            },
            success: function(data) {
              alert(data.msg);
              location.replace(location.href);
            },
            error: function(xhr, ajaxOptions, thrownError) {
              alert(xhr.status + " " + thrownError);
            }
          });

          $(this).dialog("close");
        },
        Cancel: function() {
          $(this).dialog("close");
        }
      }
    });
  });

});

$(document).on('submit', 'form.form-horizontal', function(form) {
  var $form = $(form);
  $.ajax({
    type: form.method,
    url: form.action,
    data: $form.serialize(),
    success: function(data) {
      $form.replace(data);
    }
  });



});
