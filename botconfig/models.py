from django.db import models
from registration.models import Bot

from botmint.users.models import User


class FAQ(models.Model):
    bot = models.ForeignKey(Bot, on_delete=models.CASCADE)
    admin = models.ForeignKey(User, on_delete=models.CASCADE)
    faq_file = models.FileField(blank=True, null=True, upload_to='bot_faq/')
    date = models.DateField(auto_now_add=True, blank=True)
    publish_date = models.DateField(null=True, blank=True)

    def __str__(self):
        return "{} - {}".format(self.bot.name, self.date)
