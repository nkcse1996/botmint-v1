$(document).ready(function() {
  var qatable, dataset={};

  qatable = $('#botdata').DataTable({
    "processing": true,
    "serverSide": true,
    "ajax": {
      url: url,
      data: function(d) {},
      dataSrc: function(json) {
          $.each(json.aaData, function(i, v){
            var qid = v[0];
            dataset[qid] = {
                qid: v[0],
                question: v[1],
                answer: v[2],
                entity_mn: v[4]
            };
          });
          return json.aaData;
      }
    }
  });

  $("#botdata").on("mousedown", "td .fa.fa-minus-square", function(e) {
    var ques_id = $(this).closest("tr").find("td").html();
    if(!ques_id){
        $(this).closest("tr").remove();
        return;
    }
    delete_question(ques_id, ajax=false);
  });


  $("#botdata").on('mousedown.edit', "i.fa.fa-pencil-square", function(e) {
    var id = $(this.closest("tr")).find("td:first").text();
    var data = dataset[id];
    qaForm.qid.value = data.qid;
    qaForm.question.value = data.question;
    qaForm.answer.value = data.answer;
    qaForm.entity_mn.value = data.entity_mn;
    dialog.dialog('option', 'title', 'Edit Question');
    dialog.dialog("open");
  });

  $("#botdata").on('mousedown', "input", function(e) {
    e.stopPropagation();
  });

  $('#qaForm').on('submit', function( event ) {
      //$(this).removeClass().addClass("fa fa-pencil-square");
      var data = {
        'qid':  this.qid.value,
        'ques': this.question.value,
        'ans': this.answer.value,
        'ent_mn': this.entity_mn.value
      };
      save_question(data);
      event.preventDefault();
    });


  $("#botdata").on('mousedown', "#selectbasic", function(e) {
      e.stopPropagation();
  });

  $('#botdata').css('border-bottom', 'none');
  $('<div class="addRow btn btn-primary"><button id="addQA">Add New QA</button></div>').insertBefore('#botdata');
  $('<div class="deleteAll btn btn-danger"><button id="deleteAll">Delete all</button></div>').insertBefore('#botdata');
  $('<div class="trainBot btn btn-success"><button id="trainBot">Train Bot</button></div>').insertBefore('#botdata');



  $("#deleteAll").button().on("click",  function(e) {
      var ques_id = '';
      delete_question(ques_id);
  });

  $("#trainBot").button().on("click",  function(e) {
    $("#dialog-train-confirm").dialog({
      resizable: false,
      height: "auto",
      width: 400,
      modal: true,
      buttons: {
        "Train Bot": function() {
          $.ajax({
            type: "POST",
            url: train_url,
            data: '',
            beforeSend: function(xhr) {
              xhr.setRequestHeader('X-CSRFToken', $('input[name="csrfmiddlewaretoken"]').attr('value'))
            },
            success: function(data) {
              alert(data.msg);
              location.replace(location.href);
            },
            error: function(xhr, ajaxOptions, thrownError) {
              alert(xhr.status + " " + thrownError);
            }
          });

          $(this).dialog("close");
        },
        Cancel: function() {
          $(this).dialog("close");
        }
      }
    });
  });

  function delete_question(ques_id){
    $("#dialog-confirm").dialog({
      resizable: false,
      height: "auto",
      width: 400,
      modal: true,
      buttons: {
        "Delete Question": function() {
          $.ajax({
            type: "post",
            url: delete_question_url,
            data: {question: ques_id},
            beforeSend: function(xhr) {
              xhr.setRequestHeader('X-CSRFToken', $('input[name="csrfmiddlewaretoken"]').attr('value'))
            },
            success: function(data) {
              alert(data.msg);
              location.replace(location.href);
            },
            error: function(xhr, ajaxOptions, thrownError) {
              alert(xhr.status + " " + thrownError);
            }
          });

          $(this).dialog("close");
        },
        Cancel: function() {
          $(this).dialog("close");
        }
      }
    });
  }
  function save_question(data){
    $("#dialog-save-confirm").dialog({
      resizable: false,
      height: "auto",
      width: 400,
      modal: true,
      buttons: {
        "Save Question": function() {
          $.ajax({
            type: "post",
            url: save_question_url,
            data: data,
            beforeSend: function(xhr) {
              xhr.setRequestHeader('X-CSRFToken', $('input[name="csrfmiddlewaretoken"]').attr('value'))
            },
            success: function(data) {
              alert(data.msg);
              location.replace(location.href);
            },
            error: function(xhr, ajaxOptions, thrownError) {
              alert(xhr.status + " " + thrownError);
            }
          });

          $(this).dialog("close");
        },
        Cancel: function() {
          $(this).dialog("close");
        }
      }
    });
  }

  dialog = $("#dialog-form").dialog({
    autoOpen: false,
    // height: 400,
    // width: 500,
    modal: true,
    minWidth: 400,
    position: {
      my: "center",
      at: "center",
      of: window
    },
  });

  alert_dialog = $("#error").alert({
    autoOpen: true,
    minWidth: 300,
    position: {
      my: "center",
      at: "center",
      of: window
    },
  });

  $("#cancel").button().on("click", function() {
    dialog.dialog("close");
  });


  $('#addQA').click(function() {
      qaForm.qid.value = '';
      qaForm.question.value = '';
      qaForm.answer.value = '';
      qaForm.entity_mn.value = '';
      dialog.dialog('option', 'title', 'Save New Question');
      dialog.dialog("open");
    });


});
