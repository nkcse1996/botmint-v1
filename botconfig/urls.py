from django.conf.urls import url

from .api import add_edit_question, bot_data, delete_question
from .views import copy, delete, faq_config, home, report, train

app_name = "botconfig"
urlpatterns = [
    url(r'^(?P<bot_id>\d+)/$', home, name='home'),
    url(r'^(?P<bot_id>\d+)/faq/$', faq_config, name='faq_config'),
    url(r'^(?P<bot_id>\d+)/train/$', train, name='train'),
    url(r'^(?P<bot_id>\d+)/copy/$', copy, name='copy'),
    url(r'^(?P<bot_id>\d+)/report/$', report, name='report'),
    url(r'^(?P<bot_id>\d+)/delete/$', delete, name='delete'),
    url(r'^(?P<bot_id>\d+)/data/$', bot_data, name='bot_data'),
    url(r'^(?P<bot_id>\d+)/delete_question/$',
        delete_question, name='delete_question'),
    url(r'^(?P<bot_id>\d+)/add_edit_question/$',
        add_edit_question, name='add_edit_question'),
]
