from config.utils import get_bot_db
from django.contrib.auth.decorators import login_required
from django.conf import settings
from django.http import JsonResponse
from registration.models import Bot
from django.shortcuts import get_object_or_404
from bson import ObjectId
from nlp.pos_en import pos


@login_required
def bot_data(request, bot_id):
    try:
        bot = Bot.objects.get(pk=bot_id)
    except Exception as e:
        return JsonResponse({"msg": str(e), "data": {}})
    db_name = get_bot_db(bot)
    question = settings.CON[db_name].Question
    draw = request.GET['draw']
    start = int(request.GET.get('start', 0))
    limit = int(request.GET.get('length', 10))
    # global_search = request.GET['search[value]']
    # all_objects = question.find()
    # if global_search:
    #     all_objects = question.find({'ques':global_search}).find({'ans': global_search})

    objects = []
    # sort = [(column, order_direction)]

    result_set = question.find(skip=start, limit=limit)
    for row in result_set:
        objects.append((str(row['_id']), row['ques'], row['ans'],
                        """<td><i class="fa fa-pencil-square" aria-hidden="true"></i>
                          <i class="fa fa-minus-square" aria-hidden="true"></i>
                          </td>""", row.get('ent_mn', '')))

    filtered_count = result_set.count()
    total_count = question.find().count()
    return JsonResponse({
        "sEcho": draw,
        "iTotalRecords": total_count,
        "iTotalDisplayRecords": filtered_count,
        "aaData": objects,

    })


@login_required
def delete_question(request, bot_id):
    if request.GET:
        return JsonResponse({'status': False, 'msg': "Bad Request"})
    else:
        question_id = request.POST.get('question')
        bot = get_object_or_404(Bot, pk=bot_id, admin=request.user)
        try:
            bot = Bot.objects.get(pk=bot_id)
        except Exception as e:
            return JsonResponse({"msg": str(e), "data": {}})
        db_name = get_bot_db(bot)
        question = settings.CON[db_name].Question
        try:
            if question_id:
                records = question.find({'_id': ObjectId(question_id)})
                msg = "{} question deleted".format(question_id)
            else:
                records = question.find()
                msg = "All questions deleted"
            for r in records:
                r.delete()
            resp = {'status': True, 'msg': msg}
        except Exception as e:
            resp = {'status': False, 'msg': str(e)}
        return JsonResponse(resp)


@login_required
def add_edit_question(request, bot_id):
    if request.GET:
        return JsonResponse({'status': False, 'msg': "Bad Request"})
    else:
        data = {}
        msg = "New Question Saved successfully"
        if request.POST.get('qid'):
            data['_id'] = ObjectId(request.POST.get('qid'))
            msg = "Question edited successfully"

        data['ques'] = request.POST.get('ques')
        data['ans'] = request.POST.get('ans')
        data['ans_typ'] = request.POST.get('ans_typ', 'QA')
        data['ent_mn'] = request.POST.get('ent_mn', '').split(",")
        data["pos"] = pos(data['ques'])
        bot = get_object_or_404(Bot, pk=bot_id)
        db_name = get_bot_db(bot)
        question = settings.CON[db_name].Question
        ques = None
        try:
            ques = question(data)
            ques.save()
            resp = {'status': True, 'msg': msg}
        except Exception as e:
            resp = {'status': False, 'msg': str(e)}
        return JsonResponse(resp)
