from registration.models import Bot
from django.conf import settings
from config.utils import get_bot_db


def get_bot_data(bot_id):
    question = get_bot_Question(bot_id)
    all_quest = question.find()
    ques = []
    for row in all_quest:
        ques.append({'id': str(row['_id']),
                     'question': row['ques'],
                     'answer': row['ans']})
    return ques


def get_bot_Question(bot_id):
    bot = Bot.objects.get(pk=bot_id)
    db_name = get_bot_db(bot)
    question = settings.CON[db_name].Question
    return question
