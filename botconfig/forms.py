from django import forms

from .models import FAQ


class AddFAQ(forms.ModelForm):
    class Meta:
        model = FAQ
        fields = ['faq_file']


class QAForm(forms.Form):
    qid = forms.CharField(widget=forms.HiddenInput())
    question = forms.CharField(
        widget=forms.Textarea(attrs={'rows': 2, 'cols': 15}))
    answer = forms.CharField(
        widget=forms.Textarea(attrs={'rows': 2, 'cols': 15}))
    entity_mn = forms.CharField(
        widget=forms.Textarea(attrs={'rows': 2, 'cols': 15}), required=False)
