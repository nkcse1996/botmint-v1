from django.shortcuts import render
from django.contrib.auth.decorators import login_required
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework.reverse import reverse
from .forms import AddFAQ, QAForm
from .models import FAQ
from registration.models import Bot
import django_excel as excel
from django.http import JsonResponse
from django.shortcuts import get_object_or_404
from uuid import uuid4
from nlp.word2vec import train_bot


@login_required
def faq_config(request, bot_id):
    faqs = FAQ.objects.filter(admin=request.user).order_by("-id")
    template = 'faq_config.html'
    error = ''
    if request.POST:
        form = AddFAQ(request.POST, request.FILES)
        if form.is_valid():
            try:
                filehandle = request.FILES['faq_file']
                excel.make_response(filehandle.get_sheet(), "csv")
                new_faq = form.save(commit=False)
                new_faq.admin = request.user
                new_faq.bot = Bot.objects.get(pk=bot_id)
                new_faq.save()
                form = AddFAQ()
            except Exception as e:
                error = str(e)
    else:
        form = AddFAQ()
    return render(request, template, {'faqs': faqs, 'faq_form': form, 'bot_id': bot_id, 'error': error})


@login_required
def home(request, bot_id):
    template = 'home.html'
    form = QAForm()
    return render(request, template, {'bot_id': bot_id, 'qa_form': form,
                                      'fields': ['ID', 'Question', 'Answer', 'Action']})


@login_required
def train(request, bot_id):
    if request.GET:
        return JsonResponse({'status': False, 'msg': "Bad Request"})
    try:
        train_bot(bot_id)
        resp = {'status': True, 'msg': "Bot successfully trained"}
    except Exception as e:
        resp = {'status': False, 'msg': str(e)}
    return JsonResponse(resp)


@login_required
def copy(request, bot_id):
    if request.GET:
        return JsonResponse({'status': False, 'msg': "Bad Request"})
    bot = get_object_or_404(Bot, pk=bot_id, admin=request.user)
    resp = {'status': True, 'msg': "{} successfully copied".format(bot.name)}
    try:
        bot.id = None
        bot.name = "{}_{}".format(bot.name[:8], str(uuid4())[-8:])
        bot.save()
        # TO DO deep copy FAQ sheets and mongodb replica
    except Exception as e:
        resp = {'status': False, 'msg': str(e)}
    return JsonResponse(resp)


@login_required
def report(request, bot_id):
    return render(request, "report.html")


@login_required
def delete(request, bot_id):
    if request.GET:
        return JsonResponse({'status': False, 'msg': "Bad Request"})
    bot = get_object_or_404(Bot, pk=bot_id, admin=request.user)
    resp = {'status': True, 'msg': "{} successfully deleted".format(bot.name)}
    try:
        bot.delete()
    except Exception as e:
        resp = {'status': False, 'msg': str(e)}
    return JsonResponse(resp)


class APIRoot(APIView):

    def get(self, request):
        # Assuming we have views named 'foo-view' and 'bar-view'
        # in our project's URLconf.
        return Response({
            'Templates-List': reverse('registration:template-list',
                                      request=request),
            'Templates-Detail': reverse('registration:template-detail',
                                        kwargs={'pk': 1}, request=request),
            'Bot-List': reverse('registration:bot-list', request=request),
            'Bot-Detail': reverse('registration:bot-detail', kwargs={'pk': 1},
                                  request=request),
            'AdminBot-List': reverse('registration:adminbot-list',
                                     kwargs={
                                         'username': request.user.username},
                                     request=request),
        })
