from django.conf import settings
from django.conf.urls import include, url
from django.conf.urls.static import static
from django.contrib import admin
from django.views.generic import TemplateView
from django.views import defaults as default_views
from botconfig.views import home, APIRoot
from rest_framework.schemas import get_schema_view
from rest_framework_swagger.renderers import SwaggerUIRenderer, OpenAPIRenderer


schema_view = get_schema_view(title='APIS', renderer_classes=[OpenAPIRenderer, SwaggerUIRenderer])

urlpatterns = [
    url(r'^$', TemplateView.as_view(template_name='pages/index.html'), name='index'),
    url(r'^home/$', home, name='home'),
    url(r'^about/$', TemplateView.as_view(template_name='pages/about.html'), name='about'),
    # Django Admin, use {% url 'admin:index' %}
    url(settings.ADMIN_URL, admin.site.urls),

    # User management
    url(r'^users/', include('botmint.users.urls', namespace='users')),
    url(r'^accounts/', include('allauth.urls')),
    url(r'^avatar/', include('avatar.urls')),
    url(r'^profile/', include('userprofiles.urls')),
    # Your stuff: custom urls includes go here
    # url(r'^dashboard/', include('dashboard.urls')),
    # url(r'^broadcast/', include('broadcast.urls')),
    # url(r'^reports/', include('reports.urls')),
    # url(r'^nlp/', include('nlp.urls')),
    url(r'^notification/', include('notifications.urls')),
    # url(r'^documentation/', include('documentation.urls')),
    # url(r'^help/', include('allauth.accounts.urls')),
    url(r'^dataset/', include('dataset.urls')),
    url(r'^registration/', include('registration.urls')),
    url(r'^chat/', include('chat.urls')),
    url(r'^botconfig/', include('botconfig.urls')),
    url(r'^api-auth/', include('rest_framework.urls')),
    url(r'^api/$', APIRoot.as_view(), name='api-root'),
    url(r'^ngapp/', include('ngapp.urls')),
    # url(r'^cards/', include('cards.urls')),
    url(r'^spell/', include('spellcheck.urls')),
    url(r'^apis/', include('apis.urls')),


    # Your stuf


] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT) + \
    static(settings.STATIC_URL, document_root=settings.STATIC_ROOT) + \
    static(settings.APPS_URL, document_root=settings.APPS_ROOT)


if settings.DEBUG:
    # This allows the error pages to be debugged during development, just visit
    # these url in browser to see how these error pages look like.
    urlpatterns += [
        url(r'^400/$', default_views.bad_request, kwargs={'exception': Exception('Bad Request!')}),
        url(r'^403/$', default_views.permission_denied, kwargs={'exception': Exception('Permission Denied')}),
        url(r'^404/$', default_views.page_not_found, kwargs={'exception': Exception('Page not Found')}),
        url(r'^500/$', default_views.server_error),
        #url(r'^', schema_view, name="docs"),
    ]
    if 'debug_toolbar' in settings.INSTALLED_APPS:
        import debug_toolbar
        urlpatterns = [
            url(r'^__debug__/', include(debug_toolbar.urls)),
        ] + urlpatterns
