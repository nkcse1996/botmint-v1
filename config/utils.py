from django.conf import settings
import logging


# Bot is registeration models instance of bot
def get_bot_db(bot):
    try:
        return bot.name + "_" + str(bot.id)
    except Exception as e:
        print(str(e))
        return 'botmint'


def get_bot_db_from_session(bot):
    try:
        return bot['botname'] + "_" + str(bot['botid'])
    except Exception as e:
        print(str(e))
        return 'botmint'


def log_error_msg(func_name, error_msg):
    """

    :param fun_name: error occurred in function
    :param error_msg: error msg
    :return:
    """
    error_msg = "Error in function : ({}) Error Details:\n {}".format(
        func_name, error_msg)
    if settings.DEBUG:
        print(error_msg)
    else:
        logging.error(error_msg)
