from django.conf.urls import include, url
from django.conf.urls.static import static
# Uncomment the next two lines to enable the admin:
from  . import views

app_label = 'notice'

urlpatterns = [
    url(r'^$', views.get_notices, name='notices'),
    url(r'^seen/$', views.seen_notices, name='seen_notices'),
    url(r'^update/(?P<id>\d+)/$', views.update_notice, name='update_notice'),
]
