from django.db import models
from django.core.exceptions import ValidationError
from bot_registration.models import bot_details
from datetime import datetime
from dateutil.relativedelta import relativedelta
from bot_home.models import BotUser
from fwk.utils import log_error_msg


class Notice(models.Model):
    NOTICE_EXPIRY = 15
    name = models.CharField(max_length=400)
    description = models.TextField()
    bot_id = models.ForeignKey(bot_details, on_delete=models.CASCADE)
    created = models.DateTimeField(auto_now_add=True)
    expiry = models.DateTimeField(null=True, blank=True)

    def __unicode__(self):
        return (self.name)

    def __str__(self):
        return (self.name)

    def questions(self):
        if self.pk:
            return Question.objects.filter(notice=self.pk)
        else:
            return None

    def save(self, *args, **kwargs):
        if not self.expiry:
            self.expiry = datetime.now() + relativedelta(
                days=self.NOTICE_EXPIRY)
        # Create Notice State Table:
        super(Notice, self).save(*args, **kwargs)

        Response.objects.filter(notice=self).delete()
        for user in BotUser.objects(
                bot_id=self.bot_id.bot_apikey).only("user_id"):
            try:
                Response(notice=self, user_id=user.user_id).save()
            except Exception as e:
                log_error_msg("Notice save", str(e))


class Question(models.Model):
    TEXT = 'text'
    RADIO = 'radio'
    SELECT = 'select'
    SELECT_MULTIPLE = 'select-multiple'
    INTEGER = 'integer'

    QUESTION_TYPES = (
        (TEXT, 'text'),
        (RADIO, 'radio'),
        (SELECT, 'select'),
        (SELECT_MULTIPLE, 'Select Multiple'),
        (INTEGER, 'integer'),
    )

    text = models.TextField()
    notice = models.ForeignKey(Notice)
    question_type = models.CharField(
        max_length=200, choices=QUESTION_TYPES, default=TEXT)
    # the choices field is only used if the question type
    choices = models.TextField(blank=True, null=True,
                               help_text='if the question type is "radio," \
                               "select," or "select multiple" provide a \
                               comma-separated list of options for this \
                               question .')

    def validate_choice(self):
        '''takes a text value and verifies that there is at least one comma '''
        if len(self.choices.split(',')) < 2:
            raise ValidationError(
                "The selected field requires an associated list of choices.\
                 Choices must contain more than one item.")

    def save(self, *args, **kwargs):
        if (self.question_type == Question.RADIO or
                self.question_type == Question.SELECT or
                self.question_type == Question.SELECT_MULTIPLE):
            self.validate_choice()
        super(Question, self).save(*args, **kwargs)

    def get_choices(self):
        ''' parse the choices field and return a tuple formatted appropriately
        for the 'choices' argument of a form widget.'''
        choices = self.choices.split(',')
        choices_list = []
        for c in choices:
            c = c.strip()
            choices_list.append((c, c))
        choices_tuple = tuple(choices_list)
        return choices_tuple

    def __unicode__(self):
        return (self.text)

    def __str__(self):
        return (self.text)


class Response(models.Model):
    UNDELIVERED = 'undelivered'
    DELIVERED = 'delivered'
    SEEN = 'seen'
    RESPONDED = 'responded'

    STATUS_TYPES = (
        (UNDELIVERED, 'undelivered'),
        (DELIVERED, 'delivered'),
        (SEEN, 'seen'),
        (RESPONDED, 'responded')
    )

    created = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)
    notice = models.ForeignKey(Notice, on_delete=models.CASCADE)
    user_id = models.CharField(
        "User unique identifier", max_length=50)

    status = models.CharField(
        max_length=200, choices=STATUS_TYPES, default=UNDELIVERED)

    def __unicode__(self):
        return ("%s | %s | %s" % (
            self.notice.name,  self.user_id, self.status))

    def __str__(self):
        return ("%s | %s | %s" % (
            self.notice.name,  self.user_id, self.status))


class Answer(models.Model):
    question = models.ForeignKey(Question)
    body = models.TextField(blank=True, null=True)
    response = models.ForeignKey(Response)
    created = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)
