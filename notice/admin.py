from notice.models import Question, Notice, Response, Answer
from django.contrib import admin


class QuestionInline(admin.TabularInline):
	model = Question
	extra = 0

class NoticeAdmin(admin.ModelAdmin):
	inlines = [QuestionInline]


class AnswerInline(admin.StackedInline):
	fields = ('question', 'body')
	readonly_fields = ('question',)
	extra = 0
	model = Answer


class ResponseAdmin(admin.ModelAdmin):
	inlines = [AnswerInline]
	# specifies the order as well as which fields to act on
	readonly_fields = ('notice', 'created', 'updated', 'user_id')

#admin.site.register(Question, QuestionInline)
admin.site.register(Notice, NoticeAdmin)
admin.site.register(Response, ResponseAdmin)
