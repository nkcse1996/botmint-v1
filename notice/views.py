from django.http import JsonResponse
from .models import Response, Notice
from .forms import ResponseForm
from django.views.decorators.csrf import csrf_exempt
from datetime import datetime


def get_notices(request):
    bot_id = request.session['bot_details']['bot_id']
    user_id = request.session['userinfo']['user_id']
    notices = Response.objects.filter(notice__bot_id=bot_id,
                                      notice__expiry__gte=datetime.now(),
                                      user_id=user_id,
                                      status=Response.UNDELIVERED)
    resp = []
    for notice in notices:
        resp.append({'notice': notice.notice.description,
                     'detail': str(ResponseForm(notice=notice.notice,
                                                user_id=user_id))})
    notices.update(status=Response.DELIVERED)
    return JsonResponse(resp, safe=False)


def seen_notices(request):
    bot_id = request.session['bot_details']['bot_id']
    user_id = request.session['userinfo']['user_id']
    error = ''
    status = True
    notice_list = request.GET.get('notices').split(",")
    try:
        notices = Response.objects.filter(notice__bot_id=bot_id,
                                          user_id=user_id,
                                          notice__pk__in=notice_list)
        notices.update(status=Response.SEEN)
    except Exception as e:
        error = str(e)
        status = False
    return JsonResponse({'status': status, 'error': error})


@csrf_exempt
def update_notice(request, id):
    notice = Notice.objects.get(id=id)
    user_id = request.session['userinfo']['user_id']
    status = False
    error = ''
    if request.method == 'POST':
        form = ResponseForm(request.POST, notice=notice, user_id=user_id)
        if form.is_valid():
            try:
                form.save()
                status = True
            except Exception as e:
                error = str(e)
    else:
        error = "Only Post request allowed"

    return JsonResponse({'status': status, 'error': error})
