from django.conf import settings
from mongokit import Document


@settings.CON.register
class BotUser(Document):
    __database__ = 'botmint'
    __collection__ = 'users'
    use_schemaless = True
    structure = {
        'user_id': str,
        'userinfo': dict
    }
    required_fields = ['user_id']
    default_values = {'userinfo': {}}
