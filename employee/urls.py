from django.conf.urls import url, include
from .views import EmployeeList, EmployeeDetail

app_name='employee'

from cruds_adminlte.urls import crud_for_app

urlpatterns = [
    #url(r'^$',EmployeeList.as_view(), name='emp-list'),
    #url(r'^(?P<pk>\d+)$',EmployeeDetail.as_view(), name='emp-detail')
]
urlpatterns += crud_for_app('employee')
