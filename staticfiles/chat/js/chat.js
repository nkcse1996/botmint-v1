var chatApp=angular.module("chatApp",['ngAnimate'])
      .controller("chatController",function($scope){

          $scope.chathead = function(){
            $(function(){
              $('.chat_body').slideToggle('slow');
            });
          }

          $scope.msghead = function(){
            $(function(){
              $('.msg_wrap').slideToggle('slow');

            });
          }

          $scope.close = function(){
              $scope.isclicked=true;
          }

          $scope.user = function(){
              $scope.isclickeduser=true;
          }


          $scope.user_msg = function(msg){
              $('<span><div class="container"><div class="inline"><div class="msg_b">'+msg+'</div></div><div class="inline"><img height="36" width="36" class="circular img-circle user_image"></img></div></div></span>').insertBefore('.msg_push');
                $('.msg_body').scrollTop($('.msg_body')[0].scrollHeight);
                 $('.user_image').attr('src', user_image );
          }

          $scope.bot_msg = function(msg){
               $.ajax({
                        type: 'POST',
                        url: '/chat/message/' + bot_id + "/",
                        data: {message: msg},
                        success: function(data) {
                            var msg = '';
                            if(data.error){
                              msg  = data.error;
                            }
                            else{
                              msg = data.resp;
                            }

                            if(typeof(msg) != "string"){
                               msg = JSON.stringify(msg);
                            }
                            $('<span><div class="inline2"><img height="36" width="36" class="circular2 img-circle bot_image"></img></div><div class="inline2"><div class="msg_a">'+ msg + '</div></div></span>').insertBefore('.msg_push');
                            $('.msg_body').scrollTop($('.msg_body')[0].scrollHeight);
                            $('.bot_image').attr('src',  bot_image);
                        }
                      });
          }

          $scope.chatfunction = function(){
            var text = $scope.chatbox;
            if (text!=null) {
            $(function(){
              var msg = $('textarea').val();
          			$('textarea').val('');
          			if(msg!=''){
                  $scope.user_msg(msg);
                  $scope.bot_msg(msg);
          			}
            });


            }
          }
});

chatApp.directive('enterPress', function () {
return function (scope, element, attrs) {
    element.bind("keydown keypress", function (event) {
        if(event.which === 13) {
            scope.$apply(function (){
                scope.$eval(attrs.enterPress);
            });

            event.preventDefault();
        }
    });
};
});


// $("body").bind("ajaxSend", function(elm, xhr, s){
//    if (s.type == "POST") {
//       xhr.setRequestHeader('X-CSRF-Token', getCSRFTokenValue());
//    }
// });
