$(document).ready(function() {
    $('#search').on('click',function(){
        if($('#input').val() == ''){
           alert('Input can not be left blank');
        }
        else
        value = $("#input").val()
        $.ajax({
          type: "POST",
          contentType: 'application/json',
          dataType: 'json',
          data: JSON.stringify({word: value , action: "search"}),
          url: url,
          beforeSend: function(xhr) {
            xhr.setRequestHeader('X-CSRFToken',
             $('input[name="csrfmiddlewaretoken"]').attr('value'))
          },
          success: function(data) {
            alert(data);
            console.log(data);
          },
          error: function(xhr, ajaxOptions, thrownError) {
            alert(xhr.status + " " + thrownError);
          }
        });
    });



    $('#add').on('click',function(){
        if($('#input').val() == ''){
           alert('Input can not be left blank');
        }
        else
        value = $("#input").val()
        $.ajax({
            type: "POST",
            contentType: 'application/json',
            dataType: 'json',
            data: JSON.stringify({word: value , action: "add"}),
            url: url,
          beforeSend: function(xhr) {
            xhr.setRequestHeader('X-CSRFToken',
             $('input[name="csrfmiddlewaretoken"]').attr('value'))
          },
          success: function(data) {
            alert(data.msg);
          },
          error: function(xhr, ajaxOptions, thrownError) {
            alert(xhr.status + " " + thrownError);
          }
        });
    });
});
