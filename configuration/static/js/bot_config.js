        var botid = window.botId;
        var get_configDetails = '/configuration/'+botid+'/entellio_getbotconfig_api/';
        var save_configDetails = '/configuration/'+botid+'/add_bot_configuration/';
        var remove_js = '/configuration/'+botid+'/remove_js/';
        var remove_css = '/configuration/'+botid+'/remove_css/';
        var save_images = '/configuration/'+botid+'/upload_bot_images/';
        var save_videos = '/configuration/'+botid+'/upload_bot_videos/';
        $(document).ready(function(){
            $('input[type="file"]').imageuploadify();

            function removeWaterMark(){
                if($('.html5gallery-elem-0 a')!=undefined)
                    $('.html5gallery-elem-0 a').remove()
                if($('.html5gallery-elem-1 a')!=undefined)
                    $('.html5gallery-elem-1 a').remove()
            }
            setInterval(removeWaterMark, 1);
            removeWaterMark();
            getConfigDetails();
        });

        function getConfigDetails(){
            $.ajax({
                url: get_configDetails,
                type: "GET",
                dataType: "html",
                success: function(result){
                    data = JSON.parse(result);
                    $("#id_bot_number_of_questions").val(data['bot_number_of_questions']);
                    $("#id_bot_percent_match").val(data['bot_percent_match']);
                    $("#id_feedback_choice").val(data['feedback_type']);
                    $("#id_is_guest_user_allowed").prop("checked", data['is_guest_user_allowed']);
                    $("#id_is_public").prop("checked", data['is_public']);
                    $("#id_star_rating").prop("checked", data['star_rating']);
                    $("#id_live_agent").prop("checked", data['live_agent']);
                    $("#id_show_bot_api_only").prop("checked", data['show_bot_api_only']);
                    $("#spell_check").prop("checked", data['spell_check']);
                    $("#math_exp").prop("checked", data['math_exp']);
                    $("#wikipedia").prop("checked", data['wikipedia']);
                    $("#matrix_match").prop("checked", data['matrix_match']);
                    $("#exact_match").prop("checked", data['exact_match']);
                    $("#word2vec").prop("checked", data['word2vec']);
                    if(data['bot_js'] == ''){
                        $("#remove_js").hide();
                        $("#change_js").hide();
                        $("#id_bot_js").val('');
                        $("#divForJsLink").hide();
                        $("#divForJsFile").show();
                    }
                    else{
                        $("#remove_js").show();
                        $("#change_js").show();
                        $("#change_js").val('Change');
                        $("#divForJsLink").show();
                        $("#divForJsFile").hide();
                    }
                    if(data['bot_css'] == ''){
                        $("#remove_css").hide();
                        $("#change_css").hide();
                        $("#id_bot_css").val('');
                        $("#divForCssLink").hide();
                        $("#divForCssFile").show();
                    }
                    else{
                        $("#remove_css").show();
                        $("#change_css").show();
                        $("#change_css").val('Change');
                        $("#divForCssLink").show();
                        $("#divForCssFile").hide();
                    }
                    document.getElementById("js_file").innerHTML = "Existing JS File";
                    $("#js_file").attr("href", data['bot_js']);
                    document.getElementById("css_file").innerHTML = "Existing CSS File";
                    $("#css_file").attr("href", data['bot_css']);

                    var images = data['images'];
                    var imgList= "";
                    $.each(images, function (index,value) {
                        imgList += '<a href="'+ value+'"><img src= "' + value + '" alt="<a id=\'copy_image\' class=\'btn btn-primary\' onclick=copyToClipboard(\'' + value + '\') href=\'javascript:;\'>Copy Image URL<\/a>"></a>'
                    });
                    $("#image_id").prepend(imgList);

                    var videos = data['videos'];
                    var vidList= "";
                    $.each(videos, function (index,value) {
                        vidList += '<a href="'+ value+'"><img src= "' + value + '" alt="<a id=\'copy_image\' class=\'btn btn-primary\' onclick=copyToClipboard(\'' + value + '\') href=\'javascript:;\'>Copy Video URL<\/a>"></a>'
                    });
                    $("#video_id").prepend(vidList);
                    $("#vidgallery").append('<script src="/static/bower_components/support_library/js/html5gallery.js">')
                },
                error: function(error){
                    alert(error);
                }
            });
        }

        function saveConfigDetails() {
            var js = $("#id_bot_js").val();
            var ext=js.substring(js.lastIndexOf('.') + 1).toLowerCase();
            if(js!=''){
                if(ext!='js'){
                    alert('Invalid file format. Only js files are allowed');
                    return false;
                }
            }
            var css = $("#id_bot_css").val();
            if(css!=''){
                var ext=css.substring(css.lastIndexOf('.') + 1).toLowerCase();
                if(ext!='css'){
                    alert('Invalid file format. Only css files are allowed');
                    return false;
                }
            }
            var form = new FormData($("#uploadFiles")[0]);
            $.ajax({
                url: save_configDetails,
                type: "POST",
                data: form,
                processData: false,
                contentType: false,
                success: function(result){
                    if(result.toLowerCase()=='successfully configured')
                        tempAlert(result,1000);
                    else
                        alert(result)
                },
                error: function(error){
                    alert(error);
                }
            });
        };

        function copyToClipboard(val){
            var clipboard = new Clipboard('#copy_image', {
                text: function() {
                    return val;
                }
            });
            clipboard.on('success', function(e) {
                tempAlert("Copied!",1000,'Copy');
                e.clear;
            });
        }

        function removeJs() {
            $("#change_js").hide();
            $.ajax({
                url: remove_js,
                type: "POST",
                success: function(result){
                    if(result.toLowerCase()=='removed successfully')
                        tempAlert(result,1000);
                    else
                        alert(result)
                },
                error: function(error){
                    alert(error);
                }
            });
        }

        function removeCss() {
            $("#change_css").hide();
            $.ajax({
                url: remove_css,
                type: "GET",
                success: function(result){
                    if(result.toLowerCase()=='removed successfully')
                        tempAlert(result,1000);
                    else
                        alert(result)
                },
                error: function(error){
                    alert(error);
                }
            });
        };

        function saveImages() {
            var filelist = document.getElementById("id_bot_image").files || [];
            for (var i = 0; i < filelist.length; i++) {
                filename = filelist[i].name;
                var ext=filename.substring(filename.lastIndexOf('.') + 1).toLowerCase();
                if(ext!='png' && ext!='jpg' && ext!='jpeg' && ext!='gif'){
                    alert('Invalid file format Only png,jpg,jpeg,gif are allowed');
                    return false;
                }
            }
            var form = new FormData($("#uploadImages")[0]);
            $.ajax({
                url: save_images,
                type: "POST",
                data: form,
                processData: false,
                contentType: false,
                success: function(result){
                    if(result.toLowerCase()=='successfully uploaded')
                        tempAlert(result,1000);
                    else
                        alert(result)
                },
                error: function(error){
                    alert(error);
                }
            });
        };

        function saveVideos() {
            var video = $("#id_bot_video").val();
            var ext=video.substring(video.lastIndexOf('.') + 1).toLowerCase();
            if(ext!='mp4' && ext=='mkv' && ext=='wmv'){
                alert('Invalid file format.(Allowed Ext : mp4,mkv,wmv)');
            }
            var form = new FormData($("#uploadVideos")[0]);
            $.ajax({
                url: save_videos,
                type: "POST",
                data: form,
                processData: false,
                contentType: false,
                success: function(result){
                    if(result.toLowerCase()=='successfully uploaded')
                        tempAlert(result,1000);
                    else
                        alert(result)
                },
                error: function(error){
                    alert(error);
                }
            });
        };

        function changeJs(){
            if($("#change_js").val()=='Change'){
                $("#divForJsFile").show();
                $("#divForJsLink").hide();
                $("#remove_js").hide();
                $("#change_js").val('Reset');
            }
            else{
                $("#divForJsFile").hide();
                $("#divForJsLink").show();
                $("#remove_js").show();
                $("#change_js").val('Change');
            }
        }
        function changeCss(){
            if($("#change_css").val()=='Change'){
                $("#divForCssFile").show();
                $("#divForCssLink").hide();
                $("#remove_css").hide();
                $("#change_css").val('Reset');
            }
            else{
                $("#divForCssFile").hide();
                $("#divForCssLink").show();
                $("#remove_css").show();
                $("#change_css").val('Change');
            }
        }

        function tempAlert(msg,duration,from){
            $('#validateCards').html(msg);
            $('#page_body').css('opacity','0.6');
            $('#validateCards').show();
            setTimeout(function(){
                $('#validateCards').hide();
                $('#page_body').css('opacity','unset');
                if(from==undefined)
                    location.reload();
            },duration)
        }

        function loadGallery(){
            $("#image_id").html5gallery();
        }