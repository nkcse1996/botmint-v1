from django.conf.urls import url
from . import views

urlpatterns = [
    url(r'^(?P<botid>\w{0,50})/bot_config/$', views.bot_config, name='bot_config'),
    url(r'^(?P<botid>\w{0,50})/add_bot_configuration/$', views.add_bot_configuration, name='add_bot_configuration'),
    url(r'^(?P<botid>\w{0,50})/entellio_getbotconfig_api/$', views.entellio_getbotconfig_api,
        name='entellio_getbotconfig_api'),
    url(r'^(?P<botid>\w{0,50})/upload_bot_images/$', views.upload_bot_images, name='upload_bot_images'),
    url(r'^(?P<botid>\w{0,50})/upload_bot_videos/$', views.upload_bot_videos, name='upload_bot_videos'),
    url(r'^(?P<botid>\w{0,50})/remove_js/$', views.remove_js, name='remove_js'),
    url(r'^(?P<botid>\w{0,50})/remove_css/$', views.remove_css, name='remove_css'),
    url(r'^(?P<botid>\w{0,50})/get_bot_details/$', views.get_bot_details, name='get_bot_details')
]