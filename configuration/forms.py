from django import forms


class UpdateBotForm(forms.Form):
    bot_number_of_questions = forms.CharField(required=False)
    bot_percent_match = forms.CharField(required=False)
    feedback_type = forms.CharField(required=False)
    is_guest_user_allowed = forms.BooleanField(required=False)
    is_public = forms.BooleanField(required=False)
    # star_rating = forms.BooleanField(required=False)
    live_agent = forms.BooleanField(required=False)
    # show_bot_api_only = forms.BooleanField(required=False)
    bot_js = forms.FileField(required=False)
    bot_css = forms.FileField(required=False)
    spell_check = forms.BooleanField(required=False)
    math_exp = forms.BooleanField(required=False)
    wikipedia = forms.BooleanField(required=False)
    matrix_match =forms.BooleanField(required=False)
    exact_match = forms.BooleanField(required=False)
    word2vec = forms.BooleanField(required=False)


class UploadBotImages(forms.Form):
    bot_images = forms.FileField(widget=forms.ClearableFileInput(attrs={'multiple': True}))


class UploadBotVideos(forms.Form):
    bot_videos = forms.FileField(widget=forms.ClearableFileInput(attrs={'multiple': True}))