import os
import json

from django.views.decorators.csrf import csrf_exempt
from bot_registration.models import bot_details
from django.shortcuts import render
from django.http import HttpResponse
from configuration.forms import (
    UpdateBotForm, UploadBotImages, UploadBotVideos)
from django.core.files.storage import FileSystemStorage
from fwk.utils import log_error_msg
from fwk.settings import BOT_FOLDER
from os.path import isdir, isfile, join


@csrf_exempt
def bot_config(request, botid=None):
    if not botid:
        botid = request.session['bot_details']['botid']
    bot_detail = bot_details.objects.filter(bot_apikey=botid)[0]
    response_data = {}
    if bot_detail:
        response_data['botid'] = botid
        response_data['bot_name'] = bot_detail.botname
        response_data['bot_username'] = bot_detail.bot_username
    # print(response_data)
    return render(request, 'bot_config.html', response_data)


@csrf_exempt
def add_bot_configuration(request, botid=None):
    if not botid:
        botid = request.session['bot_details']['botid']
    try:
        if request.method == 'POST':
            if request.FILES:
                form = UpdateBotForm(request.POST, request.FILES)
            else:
                form = UpdateBotForm(request.POST)
            if form.is_valid():
                rank5_response = {}
                entellio_algorithm = {}
                rank5_response['SpellCheck'] = form.cleaned_data['spell_check']
                rank5_response['MathExpression'] = form.cleaned_data['math_exp']
                rank5_response['Wikipedia'] = form.cleaned_data['wikipedia']
                entellio_algorithm['matrix_match'] = form.cleaned_data['matrix_match']
                entellio_algorithm['exact_match'] = form.cleaned_data['exact_match']
                entellio_algorithm['word2vec'] = form.cleaned_data['word2vec']
                bot_obj1 = bot_details.objects.get(bot_apikey=botid)
                if bot_obj1:
                    bot_obj1.bot_number_of_questions = form.cleaned_data[
                        'bot_number_of_questions']
                    bot_obj1.bot_percent_match = form.cleaned_data[
                        'bot_percent_match']
                    bot_obj1.feedback_type = form.cleaned_data['feedback_type']
                    bot_obj1.is_guest_user_allowed = form.cleaned_data[
                        'is_guest_user_allowed']
                    bot_obj1.is_public = form.cleaned_data['is_public']
                    # bot_obj1.star_rating = form.cleaned_data['star_rating']
                    bot_obj1.live_agent = form.cleaned_data['live_agent']
                    # bot_obj1.show_bot_api_only = form.cleaned_data[
                    #     'show_bot_api_only']
                    bot_obj1.js_file = form.cleaned_data['bot_js']
                    bot_obj1.css_file = form.cleaned_data['bot_css']
                    bot_obj1.rank5_responses = rank5_response
                    bot_obj1.entellio_algorithm = entellio_algorithm
                    # bot_obj1.matrix_match = form.cleaned_data['matrix_match']
                    # bot_obj1.exact_match = form.cleaned_data['exact_match']
                    # bot_obj1.word2vec = form.cleaned_data['word2vec']
                    bot_obj1.save()
                    return HttpResponse("Successfully Configured")
                else:
                    print('no bot details')
            else:
                print('invalid form')
                return HttpResponse("Invalid form")
    except Exception as e:
        log_error_msg("add_bot_configuration", str(e))
        print('in exception')
        print(e)
        return HttpResponse("Failed to save")


@csrf_exempt
def entellio_getbotconfig_api(request, botid=None):
    if not botid:
        botid = request.session['bot_details']['botid']
    path = request.scheme + "://" + request.get_host()

    bot_folder = os.path.join(BOT_FOLDER, botid)

    img_folder = os.path.join(bot_folder, 'images')
    res_img_path = os.path.relpath(img_folder)
    res_img = res_img_path.replace('fwk', '')
    res_img_path = res_img.replace('\\', '/')
    res_img_path = path + res_img_path
    img_list = []
    if isdir(img_folder):
        only_img_files = [f for f in os.listdir(
            img_folder) if isfile(join(img_folder, f))]
        for f in only_img_files:
            image_name = res_img_path + "/" + f
            img_list.append(image_name)

    vid_folder = os.path.join(bot_folder, 'videos')
    res_vid_path = os.path.relpath(vid_folder)
    res_vid = res_vid_path.replace('fwk', '')
    res_vid_path = res_vid.replace('\\', '/')
    res_vid_path = path + res_vid_path
    video_list = []
    if isdir(vid_folder):
        only_vid_files = [f for f in os.listdir(
            vid_folder) if isfile(join(vid_folder, f))]
        for f in only_vid_files:
            video_name = res_vid_path + "/" + f
            video_list.append(video_name)

    data_res = bot_details.objects.filter(bot_apikey=botid)

    response_data = {}
    for bot in data_res:
        if bot.js_file:
            js_path = bot.js_file.url
            js_path = path + js_path
        else:
            js_path = ''
        if bot.css_file:
            css_path = bot.css_file.url
            css_path = path + css_path
        else:
            css_path = ''
        response_data['bot_number_of_questions'] = bot.bot_number_of_questions
        response_data['bot_percent_match'] = bot.bot_percent_match
        response_data['feedback_type'] = bot.feedback_type
        response_data['is_guest_user_allowed'] = bot.is_guest_user_allowed
        response_data['is_public'] = bot.is_public
        response_data['live_agent'] = bot.live_agent
        # response_data['star_rating'] = bot.star_rating
        response_data['show_bot_api_only'] = bot.show_bot_api_only
        response_data['bot_js'] = js_path
        response_data['bot_css'] = css_path
        response_data['images'] = img_list
        response_data['videos'] = video_list
        res = bot.rank5_responses
        if res:
            response_data['spell_check'] = res['SpellCheck']
            response_data['math_exp'] = res['MathExpression']
            response_data['wikipedia'] = res['Wikipedia']

        else:
            response_data['spell_check'] = ''
            response_data['math_exp'] = ''
            response_data['wikipedia'] = ''

        algo = bot.entellio_algorithm
        if algo:
            response_data['matrix_match'] = algo['matrix_match']
            response_data['exact_match'] = algo['exact_match']
            response_data['word2vec'] = algo['word2vec']
        else:
            response_data['matrix_match'] = ''
            response_data['exact_match'] = ''
            response_data['word2vec'] = ''
    return HttpResponse(json.dumps(response_data))


@csrf_exempt
def upload_bot_images(request, botid=None):
    if not botid:
        botid = request.session['bot_details']['botid']
    bot_folder = os.path.join(BOT_FOLDER, botid)
    img_folder = os.path.join(bot_folder, 'images')
    if request.method == 'POST' and request.FILES:
        form = UploadBotImages(request.POST, request.FILES)
        if form.is_valid():
            bot_obj1 = bot_details.objects.get(bot_apikey=botid)
            if bot_obj1:
                images = request.FILES.getlist('bot_images')
                for image in images:
                    fs = FileSystemStorage(location=img_folder)
                    fs.save(image.name, image)
                bot_obj1.no_of_images = len(images)
                bot_obj1.save()
                return HttpResponse("Successfully Uploaded")
        else:
            return HttpResponse("Invalid Form")
    else:
        return HttpResponse("Please Upload Files")


@csrf_exempt
def remove_js(request, botid=None):
    if not botid:
        botid = request.session['bot_details']['botid']
    bot_obj1 = bot_details.objects.get(bot_apikey=botid)
    if bot_obj1:
        response_data = {}
        bot_obj1.js_file = ''
        bot_obj1.save()
        response_data['bot_js'] = ''
        return HttpResponse("Removed Successfully")
    else:
        return HttpResponse("failed to remove")
        #render(request, 'bot_config.html', "failed to remove")


@csrf_exempt
def remove_css(request, botid=None):
    if not botid:
        botid = request.session['bot_details']['botid']
    bot_obj1 = bot_details.objects.get(bot_apikey=botid)
    if bot_obj1:
        response_data = {}
        bot_obj1.css_file = ''
        bot_obj1.save()
        response_data['bot_css'] = ''
        return HttpResponse("Removed Successfully")
    else:
        return HttpResponse("failed to remove")


@csrf_exempt
def upload_bot_videos(request, botid=None):
    if not botid:
        botid = request.session['bot_details']['botid']
    bot_folder = os.path.join(BOT_FOLDER, botid)
    video_folder = os.path.join(bot_folder, 'videos')
    if request.method == 'POST' and request.FILES:
        form = UploadBotVideos(request.POST, request.FILES)
        if form.is_valid():
            bot_obj1 = bot_details.objects.get(bot_apikey=botid)
            if bot_obj1:
                videos = request.FILES.getlist('bot_videos')
                for video in videos:
                    fs = FileSystemStorage(location=video_folder)
                    fs.save(video.name, video)
                bot_obj1.no_of_videos = len(videos)
                bot_obj1.save()
                return HttpResponse("Successfully Uploaded")
        else:
            return HttpResponse("Invalid form")
    else:
        return HttpResponse("Please Upload Files")


@csrf_exempt
def get_bot_details(request, botid=None):
    if not botid:
        botid = request.session['bot_details']['botid']
    bot_detail = bot_details.objects.filter(bot_apikey=botid)[0]
    response_data = {}
    session_data = {}
    path = request.scheme + "://" + request.get_host()
    if bot_detail:
        session_data['bot_number_of_questions'] = bot_detail.bot_number_of_questions
        session_data['bot_percent_match'] = bot_detail.bot_percent_match
        session_data['bot_id'] = bot_detail.botid
        if bot_detail.js_file:
            js_path = bot_detail.js_file.url
            js_path = path + js_path
        else:
            js_path = ''
        if bot_detail.css_file:
            css_path = bot_detail.css_file.url
            css_path = path + css_path
        else:
            css_path = ''
        response_data['bot_js'] = js_path
        response_data['bot_css'] = css_path

        session_data['lang'] = bot_detail.language
        session_data['live_agent'] = bot_detail.live_agent
        res = bot_detail.rank5_responses
        if res:
            session_data['spell_check'] = res['SpellCheck']
            session_data['math_exp'] = res['MathExpression']
            session_data['wikipedia'] = res['Wikipedia']
        else:
            session_data['spell_check'] = False
            session_data['math_exp'] = False
            session_data['wikipedia'] = False

        algo = bot_detail.entellio_algorithm
        if algo:
            session_data['matrix_match'] = algo['matrix_match']
            session_data['exact_match'] = algo['exact_match']
            session_data['word2vec'] = algo['word2vec']
        else:
            session_data['matrix_match'] = False
            session_data['exact_match'] = False
            session_data['word2vec'] = False

        request.session['bot_details'] = session_data
    return HttpResponse(json.dumps(response_data))