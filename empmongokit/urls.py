from django.conf.urls import url, include
from .views import EmpmongokitList, EmpmongokitDetail
from django.views.decorators.csrf import csrf_exempt

app_name='empmongokit'

urlpatterns = [
    url(r'^$',csrf_exempt(EmpmongokitList.as_view()), name='empmongokit-list'),
    url(r'^(?P<firstName>\w+)$',EmpmongokitDetail.as_view(), name='empmongokit-detail')
]
