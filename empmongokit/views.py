from django.shortcuts import render
from config.settings.base import CON, DEFAULT_MONGO_DATABSE
from django.conf import settings
from django.views.generic import TemplateView
from django.http import HttpResponse
from bson.json_util import dumps
from django.http import JsonResponse
import json
from bson import ObjectId


# Create your views here.

class Connection:
    db_name = DEFAULT_MONGO_DATABSE
    db = settings.CON[db_name].Employersmongokit

class EmpmongokitList(TemplateView, Connection):

    def get(self, request, *args):
        resp = []
        try:
            for d in self.db.find():
                d = dict(d)
                d['_id'] = str(d['_id'])
                resp.append(d)
            return JsonResponse(resp, safe=False)
        except Exception as e:
            print(str(e))
            return JsonResponse(str(e))

    def post(self,request):
        d = {}
        raw = json.loads(request.body.decode('utf-8'))
        msg = "New Emp added succesfully"
        import pdb; pdb.set_trace()
        if request.POST.get('_id'):
            d['_id'] = ObjectId(raw['_id'])
            msg = "Data edited successfully"
        try:
            d['firstName'] = raw['firstName']
            d['lastName'] = raw['lastName']
            d['mobileNo'] = raw['mobileNo']
            data = self.db(d)
            data.save()
            resp = {'status': True, 'msg': msg}
        except Exception as e:
            resp = {'status': False, 'msg': str(e)}
        return JsonResponse(resp)


    def delete(self, request, *args, **kwargs):
        raw = json.loads(request.body.decode('utf-8'))
        resp = {}
        try:
            id = ObjectId(raw['_id'])
            data = self.db.find({'_id': id})
            if data.count() == 0:
                resp = {'status': False, 'msg': "_id not found"}
            else:
                for r in data:
                    r.delete()
                    print (r.delete())
                resp = {'status': True, 'msg': "successfully deleted"}
        except Exception as e:
            print(str(e))
            resp = {'status': False, 'msg': str(e)}
        return JsonResponse(resp)

class EmpmongokitDetail(TemplateView,Connection):

    def get(self, request, **kwargs):
        try:
            data = self.db.find( {"firstName":kwargs['firstName']} )
            return JsonResponse(data, safe=False)

        except Exception as e:
            print(str(e))
        return HttpResponse(dumps(data))
        #render(request, 'index.html', context=None)
