#!/bin/sh

set -o errexit
set -o pipefail
set -o nounset


celery -A botmint.taskapp worker -l INFO
