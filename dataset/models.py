from django.conf import settings
from mongokit import Document

# Create your models here.


@settings.CON.register
class Question(Document):
    __database__ = 'botmint'
    __collection__ = 'question'
    use_schemaless = True
    structure = {
        'ques': str,
        'ans': str,
        'ans_typ': str,
        'pos': {
            'noun': [str],
            'verb': [str],
            'adject': [str],
            'pronoun': [str]
        },
        'ent_mn': [str],
        'attrs': dict,
        'dcn': [dict]
    }
    required_fields = ['ques', 'ans']
    default_values = {'attrs': dict, 'ans_typ': 'qa', 'pos': {
        'noun': [],
        'verb': [],
        'adject': [],
        'pronoun': []
    },
        'ent_mn': [], 'dcn': [{}]
    }


@settings.CON.register
class API(Document):
    __database__ = 'botmint'
    __collection__ = 'api'
    use_schemaless = True
    use_dot_notation = True

    structure = {
        'name': str,
        'url': str,
        'type': str,
        'body': str,
        'params': [str],
        'header': [
            {
                'headerName': str,
                'headerValue': str
            }
        ],
        'auth': {
            'type': str,
            'values': str,
            'userName': str,
            'password': str
        }
    }
    required_fields = ['name', 'url']
