from rest_framework import generics, permissions


from .serializers import TemplateSerializer, BotSerializer
from .models import Template, Bot
from .permissions import BotAdminCanEditPermission


class TemplateMixin(object):
    model = Template
    queryset = Template.objects.all()
    serializer_class = TemplateSerializer


class TemplateList(TemplateMixin, generics.ListAPIView):
    permission_classes = [
        permissions.AllowAny
    ]


class TemplateDetail(TemplateMixin, generics.RetrieveAPIView):
    lookup_field = 'name'


class BotMixin(object):
    model = Bot
    queryset = Bot.objects.all()
    serializer_class = BotSerializer
    permission_classes = [
        BotAdminCanEditPermission
    ]

    def perform_create(self, serializer):
        """Force author to the current user on save"""
        serializer.save(admin=self.request.user)


class BotList(BotMixin, generics.ListCreateAPIView):
    pass


class BotDetail(BotMixin, generics.RetrieveUpdateDestroyAPIView):
    pass


class AdminBotList(generics.ListAPIView):
    model = Bot
    queryset = Bot.objects.all()
    serializer_class = BotSerializer

    def get_queryset(self):
        queryset = super(AdminBotList, self).get_queryset()
        return queryset.filter(admin__username=self.kwargs.get('username'))
