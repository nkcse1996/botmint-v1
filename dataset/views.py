from config.utils import get_bot_db
from django.views.generic import TemplateView
from bson import ObjectId
import pyexcel as excel
from botconfig.models import FAQ
from django.conf import settings
from django.http import JsonResponse
from django.shortcuts import get_object_or_404, render, redirect
from registration.models import Bot
from datetime import datetime
from nlp.pos_en import pos


# Create your views here.


def faq_upload(request, bot_id, faq_id):
    faq = get_object_or_404(FAQ, pk=faq_id)

    records = excel.iget_records(file_name=faq.faq_file.path)
    data = []
    error = ''
    keys = []
    for i, record in enumerate(records):
        if i == 0:
            keys = list(record.keys())
        d = {}
        try:
            d['ques'] = record['Question'].encode(
                'ascii', 'ignore').decode('utf-8').strip()
            d['ans'] = record['Answer'].encode(
                'ascii', 'ignore').decode('utf-8').strip()
        except KeyError:
            d['ques'] = record[keys[0]].encode(
                'ascii', 'ignore').decode('utf-8').strip()
            d['ans'] = record[keys[1]].encode(
                'ascii', 'ignore').decode('utf-8').strip()
        except Exception as e:
            error = str(e)
            break
        ques = edit_save(bot_id, d)
        data.append(ques)

    if not error:
        faq.publish_date = datetime.now()
        faq.save()
        return redirect('botconfig:home', bot_id=bot_id)
    return render(request, 'faq_dataset.html', {'data': data,
                                                'error': error,
                                                'bot_id': bot_id})


def ques_delete(request, bot_id, ques_id):
    bot = get_object_or_404(Bot, pk=bot_id)
    db_name = get_bot_db(bot)
    question = settings.CON[db_name].Question
    message = "success"
    try:
        question.delete(ques_id)
    except Exception as e:
        print(str(e))
        message = "fail"
    return JsonResponse({'status': message})


def edit_save(bot_id, data, ques_id=None):
    if ques_id:
        data['_id'] = ques_id
    data['pos'] = pos(data['ques'])
    bot = get_object_or_404(Bot, pk=bot_id)
    db_name = get_bot_db(bot)
    question = settings.CON[db_name].Question
    ques = None
    try:
        ques = question(data)
        ques.save()
    except Exception as e:
        print(str(e))
    return ques


class ChabotApi(TemplateView):

    def get(self, request, bot_id):
        try:
            bot = Bot.objects.get(pk=bot_id)
        except Exception as e:
            return JsonResponse({"msg": str(e), "data": {}})
        db_name = get_bot_db(bot)
        api = settings.CON[db_name].API
        resp = []
        try:
            for data in api.find():
                data = dict(data)
                data['_id'] = str(data['_id'])
                resp.append(data)
            return JsonResponse(resp, safe=False)
        except Exception as e:
            print(str(e))
            return JsonResponse(str(e))

    def post(self, request, bot_id):
        try:
            bot = Bot.objects.get(pk=bot_id)
        except Exception as e:
            return JsonResponse({"msg": str(e), "data": {}})
        db_name = get_bot_db(bot)
        api = settings.CON[db_name].API
        data = {
            'name': str,
            'url': str,
            'type': str,
            'body': str,
            'params': [str],
            'header': [
                {
                    'headerName': str,
                    'headerValue': str
                }
            ],
            'auth': {
                'type': str,
                'values': str,
                'userName': str,
                'password': str
            }
        }
        msg = "New Emp added succesfully"
        if request.POST.get('_id'):
            data['_id'] = ObjectId(request.POST.get('_id'))
            msg = "Data edited successfully"
        try:
            data['name'] = request.POST.get('name')
            data['url'] = request.POST.get('url')
            data['type'] = request.POST.get('type')
            data['params'] = request.POST.get('params')
            data['header'] = request.POST.get('header')
            data['auth']['type'] = request.POST.get('auth.type')
            data['auth']['values'] = request.POST.get('auth.values')
            data['auth']['userName'] = request.POST.get('auth.uName')
            data['auth']['password'] = request.POST.get('auth.password')
            saving_data = api(data)
            saving_data.save()
            resp = {'status': True, 'msg': msg}
        except Exception as e:
            resp = {'status': False, 'msg': str(e)}
        return JsonResponse(resp)

        def delete(self, request, bot_id):
            try:
                bot = Bot.objects.get(pk=bot_id)
            except Exception as e:
                return JsonResponse({"msg": str(e), "data": {}})
            db_name = get_bot_db(bot)
            api = settings.CON[db_name].API

            resp = {}
            try:
                id = ObjectId(request.POST.get('_id'))
                data = api.find({'_id': id})
                if data.count() == 0:
                    resp = {'status': False, 'msg': "_id not found"}
                else:
                    for r in data:
                        r.delete()
                        print(r.delete())
                    resp = {'status': True, 'msg': "successfully deleted"}
            except Exception as e:
                print(str(e))
                resp = {'status': False, 'msg': str(e)}
            return JsonResponse(resp)
