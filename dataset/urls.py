from django.conf.urls import url
from .views import faq_upload, ques_delete,ChabotApi

app_name = "dataset"

urlpatterns = [
    url(r'^(?P<bot_id>\d+)/(?P<faq_id>\d+)$', faq_upload, name='faq_upload'),
    url(r'^(?P<bot_id>\d+)/(?P<ques_id>\d+)$', ques_delete, name='ques_delete'),
    url(r'^(?P<bot_id>\d+)/chatbotapi/$', ChabotApi.as_view(), name='chat_bot_api'),


]
