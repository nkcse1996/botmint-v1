from rest_framework import serializers

from .models import Template, Bot


class TemplateSerializer(serializers.ModelSerializer):

    class Meta:
        model = Template
        fields = ('name', 'desc',  )


class BotSerializer(serializers.ModelSerializer):
    class Meta:
        model = Bot
        fields = '__all__'

