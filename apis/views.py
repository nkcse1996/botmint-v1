from django.shortcuts import render
from django.contrib.auth.decorators import login_required


@login_required
def api(request):
    return render(request, "apis.html", {})
