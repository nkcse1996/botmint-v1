from django.conf.urls import url
from .views import api

app_name = "apis"

urlpatterns = [
    url(r'^$', api, name='apis'),
]
