from .pos_en import pos
from .jaccard import jaccard_percent
from botconfig.utils import get_bot_Question


def count_a_in_b(a, b):
    count = 0
    for i in a:
        if i in b:
            count += 1
    return count


def get_max_list(_dict):
    m = max(_dict.values())
    return [k for k in _dict if _dict.get(k) == m]


def get_answer(botid, question):
    response_data = {}
    _pos = pos(question)

    if _pos['noun']:
        ques_list = get_bot_Question(botid).find({
            "$or": [{"pos.noun": {"$in": _pos['noun']}},
                    {"pos.verb": {"$in": _pos['verb']}},
                    {"pos.adject": {"$in": _pos['adject']}},
                    {"pos.pronoun": {"$in": _pos['pronoun']}}
                    ]
        })
    else:
        ques_list = get_bot_Question(botid).find({"pos.noun": []})

    for key in ["noun", "verb", "adject", "pronoun"]:
        if _pos[key]:
            dataset = {}
            for ques in ques_list:
                dataset[ques] = count_a_in_b(_pos[key], ques["pos"][key])
            if dataset:
                ques_list = get_max_list(dataset)

    for ques in ques_list:
        response_data[str(ques['_id'])] = jaccard_percent(question, ques['ques'])
    return response_data
