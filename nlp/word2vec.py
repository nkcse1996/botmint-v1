import pickle

import gensim
import numpy as np
import pandas as pd
from botconfig.utils import get_bot_data
from django.conf import settings
from django.core.files.base import ContentFile
from nlp.models import ModelFiles
from nltk.corpus import stopwords
from nltk.tokenize import RegexpTokenizer
from registration.models import Bot
from scipy import spatial

VEC_SIZE = 64
TOP_RESULTS = 10


def train_bot(botid):
    question_list, question_id = [], []
    for obj in get_bot_data(botid):
        question_list.append(obj['question'])
        question_id.append(obj['id'])
    df = pd.DataFrame({"question": question_list, "question_id": question_id})

    corpus = df['question'].values.tolist()
    tok_corpus = []

    for i in range(len(corpus)):
        tok_corpus.append(apply_text_processing(corpus[i]))
    model = gensim.models.Word2Vec(tok_corpus,
                                   min_count=0,
                                   workers=4,
                                   window=5,
                                   size=VEC_SIZE)

    df['s_vec'] = df['question']
    for i in range(df['question'].values.size):
        df['s_vec'][i] = get_sentance_vector(df["question"][i], model)

    bot = Bot.objects.get(pk=botid)
    files = get_model_files(bot)
    model.save(files["model"])
    with open(files['data'], 'wb') as data_f:
        pickle.dump(df, data_f, protocol=pickle.HIGHEST_PROTOCOL)
    ModelFiles.objects.filter(bot=bot).delete()
    settings.NLP_WORD2VEC_FILES.pop(botid, False)
    mb = ModelFiles(bot=bot)
    mb.model_file.save(files["model"], ContentFile(
        open(files["model"], "rb").read()), save=False)
    mb.df_file.save(files["data"], ContentFile(
        open(files["data"], "rb").read()), save=False)
    mb.save()
    return True


def get_sentance_vector(sentence, model):
    s_vec = np.zeros(VEC_SIZE)
    t_words = apply_text_processing(sentence)
    for word in t_words:
        try:
            vector = model[word]
        except Exception as e:
            print(str(e))
            vector = np.zeros(VEC_SIZE)
        s_vec += vector
    return s_vec / len(s_vec)


def remove_stopwords(tokenized_words):
    tokens_after_removing_stopwords = [
        w for w in tokenized_words if w not in set(stopwords.words("english"))]
    if(len(tokens_after_removing_stopwords) == 0):
        tokens_after_removing_stopwords = tokenized_words
    return tokens_after_removing_stopwords


def apply_text_processing(sentence):
    tokenizer = RegexpTokenizer(r'\w+')
    return remove_stopwords(tokenizer.tokenize(sentence.lower()))


def get_model_files(bot):
    return {'model': "{}-{}-model.bin".format(bot.name, bot.id),
            'data': "{}-{}-data.bin".format(bot.name, bot.id)}


def get_answer(botid, question):
    response = {}
    bot = Bot.objects.get(pk=botid)
    model_files = ModelFiles.objects.get(bot=bot)

    if botid not in settings.NLP_WORD2VEC_FILES:
        settings.NLP_WORD2VEC_FILES[botid] = {}
        with open(model_files.model_file.path, 'rb') as handle:
            settings.NLP_WORD2VEC_FILES[botid]['model'] = pickle.load(handle)
        with open(model_files.df_file.path, 'rb') as handle:
            settings.NLP_WORD2VEC_FILES[botid]['data'] = pickle.load(handle)

    df = settings.NLP_WORD2VEC_FILES[botid]['data']
    model = settings.NLP_WORD2VEC_FILES[botid]['model']

    sent_diff = np.zeros(df['s_vec'].shape)

    question_vector = get_sentance_vector(question, model)
    if not np.any(question_vector):
        return response

    for i in range(df['s_vec'].values.size):
        sent_diff[i] = spatial.distance.cosine(
            df['s_vec'][i], question_vector) - 1

    results = len(sent_diff)
    if results > TOP_RESULTS:
        results = TOP_RESULTS

    argsort_sents = np.argpartition(sent_diff,
                                    results - 1)[:results]
    for sent in argsort_sents:
        if not np.isnan(sent_diff[sent]):
            response[df['question_id'][sent]] = -sent_diff[sent]

    return response


def delete_model(botid):
    bot = Bot.objects.get(pk=botid)
    model_files = ModelFiles.objects.get(bot=bot)
    settings.NLP_WORD2VEC_FILES.pop(botid, True)
    model_files.delete()
