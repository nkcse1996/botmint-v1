from botconfig.utils import get_bot_Question

from .jaccard import jaccard_percent


def get_answer(botid, question):
    response_data = {}
    ques_list = get_bot_Question(botid).find({"ques": question})

    for ques in ques_list:
        response_data[str(ques['_id'])] = jaccard_percent(question, ques['ques'])
    return response_data
