from learning.models import Questionnaire
from learning.views import ques_IE_extractor
from mongoengine import Q
from .utils import jaccard_percent


def matrix_chat(request, input_question):
    weighted_matrix_result = {}
    botid = request.session['bot_details']['botid']
    lang_type = request.session['bot_details']['lang']
    entities, intents, identifier, wh = ques_IE_extractor(
        input_question.lower(), lang_type)
    if entities:
        result = search_pattern_by_entities(entities, botid=botid)
    if result and intents:
        result = search_pattern_by_intents(
            input_data=result, intents=intents, wh=wh, identifier=identifier)

    for matrx_ob in result:
        jv = 0
        for ques in matrx_ob.questions:
            jv = max(jv, jaccard_percent(input_question, ques))
        weighted_matrix_result[str(matrx_ob.id)] = jv
    return weighted_matrix_result


# Entities search function
def search_pattern_by_entities(entities=None, botid=None):
    max_list = []
    if len(entities) != 0:
        ques_list = Questionnaire.objects(
            Q(bot_id=botid) & Q(questions_pos__entities__in=entities))
    else:
        ques_list = Questionnaire.objects(
            Q(bot_id=botid) & Q(questions_pos__entities__size=0))
    if ques_list:
        count_dict_entities = {}
        for i in ques_list:
            count = 0
            object_entities = i.questions_pos.entities
            count_list = [count + 1 for j in entities if j in object_entities]
            count_dict_entities[i] = count_list[0]
        m = max(count_dict_entities.values())
        max_list = [
            k for k in count_dict_entities if count_dict_entities.get(k) == m]
    return max_list


# Intents search function
def search_pattern_by_intents(input_data=None, intents=None, wh=None, identifier=None):
    count_dict_intents = {}
    for i in input_data:
        count = 0
        object_intents = list(i.questions_pos.intents.keys())
        for int_intents in intents:
            if int_intents in object_intents:
                count = count + 1
        count_dict_intents[i] = count
    intents_weight = max(count_dict_intents.values())
    max_list_intents = [
        k for k in count_dict_intents if count_dict_intents.get(k) == intents_weight]
    result_list3 = search_pattern_by_wh(
        input_data=max_list_intents, wh=wh, identifier=identifier)
    return result_list3

# WH search function


def search_pattern_by_wh(input_data=None, wh=None, identifier=None):
    count_dict_wh = {}
    for i in input_data:
        count = 0
        object_intents = i.questions_pos.ablative
        for int_wh in wh:
            if int_wh in object_intents:
                count = count + 1
        count_dict_wh[i] = count
    intents_weight = max(count_dict_wh.values())
    max_list_wh = [
        k for k in count_dict_wh if count_dict_wh.get(k) == intents_weight]
    result_list4 = search_pattern_by_identifier(
        input_data=max_list_wh, identifier=identifier)
    return result_list4


# Identifier search function
def search_pattern_by_identifier(input_data=None, identifier=None):
    count_dict_identifier = {}
    for i in input_data:
        count = 0
        object_identifier = i.questions_pos.identifier
        for int_identifier in identifier:
            if int_identifier in object_identifier:
                count = count + 1
        count_dict_identifier[i] = count
    intents_weight = max(count_dict_identifier.values())
    max_list_intents = [
        k for k in count_dict_identifier if count_dict_identifier.get(k) == intents_weight]
    return max_list_intents
