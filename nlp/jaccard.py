# Imports
import string
from nltk.tokenize import RegexpTokenizer
import nltk.corpus
import nltk.tokenize

# Get default English stopwords and extend with punctuation
stopwords = nltk.corpus.stopwords.words('english')
stopwords.extend(string.punctuation)
stopwords.append('')


def remove_stopwords(tokenized_words):
    tokens_after_removing_stopwords = [
        w for w in tokenized_words if w not in stopwords]
    if(len(tokens_after_removing_stopwords) == 0):
        tokens_after_removing_stopwords = tokenized_words
    return tokens_after_removing_stopwords


def apply_text_processing(sentence):
    tokenizer = RegexpTokenizer(r'\w+')
    return remove_stopwords(tokenizer.tokenize(sentence.lower()))


def jaccard_percent(a, b):
    """Check if a and b are matches."""
    lemmae_a = apply_text_processing(a)
    lemmae_b = apply_text_processing(b)

    # Calculate Jaccard similarity
    ratio = len(set(lemmae_a).intersection(lemmae_b)) / \
        float(len(set(lemmae_a).union(lemmae_b)))
    return ratio
