import re
from datetime import datetime

from bot_registration.models import bot_details
from brain.api_process import process_api
from chat.models import chat_history
from django.http import request
from fwk.utils import log_error_msg
from learning.models import Conversation, Questionnaire, entellio_bot_greeting
from nltk import word_tokenize
from spell_check.spell import SpellCheck

yes_inputs = ['all right', 'hmm', 'alright', 'very well', 'of course',
              'by all means', 'sure', 'certainly', 'absolutely', 'yeah',
              'yah', 'yep', 'yup', 'okay', 'ok', 'yes']
no_inputs = ['no', 'nope']


def chat_history_fn(request, rank, botid, input_question,
                    response_data=None, status=None):
    userinfo = request.session.get("userinfo", {})
    chat_id = ''
    try:
        ch = chat_history(rank=rank,
                          botid=botid,
                          datetime=datetime.now(),
                          request=input_question,
                          username=userinfo.get('username', "user"),
                          emailid=userinfo.get('email', "user@entellio.com"),
                          response=str(response_data),
                          status=status)
        chat_obj = ch.save()
        chat_id = chat_obj.id
    except Exception as e:
        log_error_msg("chat_history_fn", str(e))
    return chat_id


# Greeting Decorater
def precheck_greetings(request, botid, input_question, req_type=None):
    input_request = str(input_question).lower().strip()
    response = ''
    input_response_queryset = entellio_bot_greeting.objects.filter(greeting_status=True,
                                                                   bot_id__bot_apikey=request.session['bot_details']['botid'])
    if input_response_queryset:
        if (input_request in map(str.strip, input_response_queryset[0].bot_greeting_request.split(","))) or (
            re.sub(r'(.)\1+', r'\1', input_request) in map(str.strip, input_response_queryset[
                0].bot_greeting_request.split(","))):
            response = input_response_queryset[0].bot_greeting_response

    return response

# Spell Cheker Decorater


def precheck_spell(request, input_question):
    spellcheck = request.session['user_context'].pop('spellcheck', False)
    if spellcheck:
        try:
            botid = request.session['bot_details']['botid']
            global yes_inputs, no_inputs
            if input_question.lower() in yes_inputs:
                return spellcheck[1]
            else:
                return spellcheck[0]


def post_spellcheck(request, input_question):
    input_list = word_tokenize(input_question)
    splchk = SpellCheck(request.session['bot_details']['botid'])
    output_list = [(index,  str(splchk.correction(item))) for index, item in
                   enumerate(input_list) if item and (str(item) != str(splchk.correction(item)))]
    if output_list:
        request.session['user_context']['spellcheck'] = output_list

    except Exception as e:
        log_error_msg("precheck_spell", str(e))

    return output_list


def math_exp_detect(request, input_question):
    response = False
    try:
        regex = [r'[(){}\[\]]|\d|[+*-\/%]|[\.\d]', r'[+*-\/%]']
        search = re.search(regex[0], input_question)
        if search:
            search = re.search(regex[1], input_question)
            if search:
                math_output = eval(input_question)
                response = str(math_output)

    except Exception as e:
        log_error_msg("math_exp_detect", str(e))
    return response


def get_feedback(botid=None, chat_id=None, base_url=None, feedback_type=None):
    if not botid:
        botid = request.session['bot_details']['botid']

    # we have create feedback card
    response_feedback = {
        "feedbackType": "list",
        "feedbackResponse": {
            "url": base_url + "/chat/" + botid + "/feedback_message/",
            "data": {'botid': botid,
                     'chat_id': chat_id,
                     'feedback_msg': "Please send the feedback rating value",
                     },
        }
    }

    response_feedback["feedbackResponse_widget"] = feedback_type
    return response_feedback


def set_bot_details_in_session(request, bot_id, input_data):
    if 'bot_details' not in request.session:
        bot_detail = bot_details.objects.get(bot_apikey=bot_id)
        session_data = {'botid': bot_id}
        if bot_detail:
            session_data[
                'bot_number_of_questions'] = bot_detail.bot_number_of_questions
            session_data['bot_percent_match'] = bot_detail.bot_percent_match
            session_data['lang_type'] = bot_detail.language
            try:
                if not session_data['lang_type']:
                    session_data['lang_type'] = detect(input_data)
            except Exception as e:
                log_error_msg("search", str(e))
                session_data['lang_type'] = 'en'

            session_data['live_agent'] = bot_detail.live_agent
            session_data['id'] = bot_detail.botadmin_id
            res = bot_detail.rank5_responses
            print(res)
            if res:
                session_data['spell_check'] = res['SpellCheck']
                session_data['math_exp'] = res['MathExpression']
                session_data['wikipedia'] = res['Wikipedia']
            else:
                session_data['spell_check'] = False
                session_data['math_exp'] = False
                session_data['wikipedia'] = False
            request.session['bot_details'] = session_data


def set_user_details_in_session(request, userinfo):
    if 'userinfo' not in request.session:
        request.session['userinfo'] = {}
        request.session['userinfo'][
            'displayname'] = request.user.get_username()
        request.session['userinfo'] = userinfo
