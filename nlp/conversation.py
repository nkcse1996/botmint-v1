from learning.models import Conversation


def jaccard_comparison(userinfo, user_input, json_value):
    for key in userinfo:
        json_value = json_value.replace(key, userinfo[key])
    json_value_list = json_value.split(",")
    ratio_list = []
    for each in json_value_list:
        mainList = set(user_input.split(" "))
        subList = set(each.split(" "))
        ratio_list.append(len(set(mainList).intersection(
            subList)) / float(len(set(mainList).union(subList))))
    return max(ratio_list)


def traverse_json(request, conv_obj, session_path):
    """
    :param request: session object
    :param path: session_path or depth level where conversation is going on
    :param conv_obj: json containing conversation for respective question
    :return: path and sub json till the session path
    """
    status = True
    path = conv_obj[0].conversation_json[0]['path']
    json = conv_obj[0].conversation_json[0]
    try:
        while path != session_path:
            if len(json['next']) == 1:
                json = json['next'][0]
                path = json['path']
            elif len(json['next']) > 1:
                for i in range(len(json['next'])):
                    path_i = json['next'][i]['path']
                    if path_i in session_path:
                        json = json['next'][i]
                        path = path_i
                        break
            elif len(json['next']) == 0:
                # converstation ended
                status = False
                break
    except IndexError as e:
        status = False
    return status, path, json


# need optimization afterwards
def extract_info(request, input_question, value):
    variables = set([word for word in value.replace(
        ",", " ").replace(".", " ").split() if word.startswith('$')])
    variable_list = list(variables)
    userinfo = {}
    if variable_list:
        other_words = [word for word in value.replace(",", " ").replace(
            ".", " ").split() if word not in variable_list]
        extracted_variable = [word for word in input_question.replace(
            ",", " ").replace(".", " ").split() if word not in other_words]
        print(extracted_variable)
        if extracted_variable:
            userinfo[variable_list[0]] = " ".join(extracted_variable)

    # Updating variables in session
    if "userinfo" in request.session:
        session_userinfo = request.session['userinfo']
        for key in userinfo:
            session_userinfo[key] = userinfo[key]
        request.session['userinfo'] = session_userinfo
    else:
        request.session['userinfo'] = userinfo
    return userinfo


def start_conv(request, input_data=None):
    """
    This function is to start and reply for conversation
    for the respective question
    :param input_question: Input sentence from user
    :param request: session request object
    :param conversation_id: id of the database question to
    which input sentence is maximum close to
    :return:
    """
    return_message = []
    conv_id = request.session['user_context'].get('conv', {}).get('id')
    if not conv_id:
        conv_id = input_data
    conv_obj = Conversation.objects.get(id=conv_id)

    if not request.session['user_context'].get('conv'):
        # start conversation
        cur_node = conv_obj.conversation_json[0]
        path = cur_node['path']
        request.session['user_context']['conv'] = {'id': conv_id, 'path': path}
        return_message = cur_node['value']

        if len(cur_node['next']) != 0:
            if (cur_node['next'][0]['type']).lower() != "decision":
                request.session['user_context']['conv']['conv_obj'] = conv_id
    else:
        # proceed conversation
        # Code will come here if conversation is not for the first time
        # and its going on, i.e. middle of conversation.
        # There is history of chat in  conversation_dictionary in session
        # which gives information about conversation,
        # like ['path'] say about the point where conversation has reached.
        # ['question_id'] id of database question for
        # which conversation has started

        conv = request.session['user_context']['conv']
        path = conv["path"]

        status, path, json = traverse_json(request, conv_obj, path)
        if status:
            if len(json['next']) != 0:
                if 'decision' in json['next'][0]['type']:
                    for i in range(len(json['next'])):
                        value = json['next'][i]['value']

                        userinfo = extract_info(
                            request, input_question.lower(), value.lower())
                        if jaccard_comparison(userinfo, input_question.lower(),
                                              value.lower()) > 0.5:
                            return_message = json['next'][
                                i]['next'][0]['value']

                            path = json['next'][i]['next'][0]['path']
                            if len(json['next'][i]['next'][0]['next']) != 0:
                                if (json['next'][i]['next'][0]['next'][0]['type']).lower() != "decision":
                                    request.session[
                                        'conv_obj'] = conv_id
                            break

                    request.session['user_context']['conv']['path'] = path
                else:
                    path = json['next'][0]['path']
                    return_message = json['next'][0]['value']
                    request.session['user_context']['conv']['path'] = path

                    if len(json['next'][0]['next']) != 0:
                        if (json['next'][0]['next'][0]['type']).lower() != "decision":
                            request.session['conv_obj'] = conv_id

            elif len(json['next']) == 0:
                # conversation ended as no next link is available hence
                # deleting conversation dictionary from session
                del request.session['user_context']['conv']
        else:
            del request.session['user_context']['conv']
            return_message =  create_card("Conversation Broken, Please ask something else.")

    return return_message
