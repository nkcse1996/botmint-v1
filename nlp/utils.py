from botconfig.utils import get_bot_Question
from bson import ObjectId
import re
from config.utils import log_error_msg
from textblob import TextBlob

yes_inputs = ['all right', 'hmm', 'alright', 'very well', 'of course',
              'by all means', 'sure', 'certainly', 'absolutely', 'yeah',
              'yah', 'yep', 'yup', 'okay', 'ok', 'yes']
no_inputs = ['no', 'nope']


def get_answer_suggestions(bot, response):
    answer = ''
    suggestions = []
    resp_obj = get_bot_Question(
        bot['botid']).find({"_id": {"$in":
                                    [ObjectId(k) for k in response.keys()]}})
    resp = {}
    for r in resp_obj:
        resp[r['_id']] = {"score": response[str(r['_id'])], "question": r}
    sorted_resp = sorted(resp.values(), key=lambda x: -x.get('score'))

    answer = sorted_resp[0]
    if answer.get("score") < bot.get('sug_percentage', 0.5):
        answer = ''
    else:
        answer = answer.get("question").get("ans")
    all_suggestions = sorted_resp[1:]
    for s in all_suggestions:
        if s.get("score") >= bot.get('sug_percentage', 0.5):
            suggestions.append(
                {"score": s.get("score"), "question": s.get('question').get('ques')})
            if len(suggestions) >= bot.get('suggestions', 5):
                break

    if not suggestions:
        suggestions = ''
    return answer, suggestions


def handle_spell_context(context, message):
    spell = context.pop('spell', False)
    postspell = True
    if spell:
        global no_inputs
        if message.lower() in no_inputs:
            postspell = False
            message = spell['initial']
        else:
            message = spell['corrected']

    return postspell, message


def post_spell(msg):
    status = False
    correction = str(TextBlob(msg).correct())
    ans = ''
    if msg != correction:
        status = True
        ans = "Did You mean ? <br>{} <br>YES/NO".format(correction)
    return status, correction, ans


def math_evaluate(message):
    response = False
    try:
        regex = [r'[(){}\[\]]|\d|[+*-\/%]|[\.\d]', r'[+*-\/%]']
        search = re.search(regex[0], message)
        if search:
            search = re.search(regex[1], message)
            if search:
                math_output = eval(message)
                response = str(math_output)

    except Exception as e:
        log_error_msg("math_exp_detect", str(e))
    return response


def initial_greet(bot):
    greet = bot.get('attr', {}).get('initial_greet')
    if not greet:
        greet = 'Hello $displayname, How may I help you ?'
    return greet


def check_greetings(bot, message):
    # greetings Model will be written
    ans = ''
    if message.lower() == "hi":
        ans = "Hello, How are you?"
    return ans
