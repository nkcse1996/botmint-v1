from . import word2vec
from . import exact_match
from . import matrix
from .process_api import handle_api_context, start_api
from .process_conv import handle_conv_context, start_conv
from .utils import get_answer_suggestions, handle_spell_context, \
    initial_greet, math_evaluate, post_spell, check_greetings
import json
import wikipedia
from string import Template
from django.conf import settings


def process(user_msg, bot_info, user_info, context, req_info, counter):
    output = {
        "error": "",
        "feedback_type": '',
        "context": {},
        "answer": "",
        "score": 1,
        "suggestions": '',
        'counter': counter}

    print("Context: ", context)
    print("MSG :", user_msg)
    print("user: ", user_info)
    print("bot :", bot_info)
    postspell = True
    try:
        user_dict = json.loads(user_msg)
        msg = user_dict['value']
    except Exception as e:
        msg = user_msg
        print(e)

    if '$RESET$' == msg:
        output["answer"] = "Context Resetted"

    elif bot_info.get('spell', True):
        postspell, msg = handle_spell_context(context, msg)

    elif '$INITIAL_GREET$' == msg:
        output["answer"] = initial_greet(bot_info)

    elif context.get('api'):
        output["answer"] = handle_api_context(context, msg)
    elif context.get('conv'):
        output["answer"] = handle_conv_context(context, msg)

    if not output["answer"]:
        output["answer"] = check_greetings(bot_info, msg)

    if not output["answer"]:
        output["answer"], output["suggestions"] = process_message(
            bot_info, msg
        )

        if isinstance(output["answer"], dict):
            if output["answer"].get('type') == 'api':
                api, output["answer"] = start_api(output["answer"].get('value'))
                context['api'] = api

            if output["answer"].get('type') == 'conv':
                conv, output["answer"] = start_conv(output["answer"].get('value'))
                context['conv'] = conv

    if not output["answer"]:
        if bot_info.get('math_exp', True):
            output["answer"] = math_evaluate(msg)

        if not output["answer"] and bot_info.get('spell', True) and postspell:
            status, correction, output["answer"] = post_spell(msg)
            if status:
                context['spell'] = {'initial': msg, 'corrected': correction}

        if not output["answer"] and bot_info.get('wiki', True):
            try:
                output["answer"] = wikipedia.summary(msg, sentences=1)
            except wikipedia.exceptions.DisambiguationError:
                pass
            except wikipedia.exceptions.PageError:
                pass

    output["answer"] = replace_variable(user_info, output["answer"])

    if not output["context"] and output["answer"]:
        output["feedback_type"] = bot_info.get('fb_type', 'thumbs')

    if not output["answer"]:
        output["score"] = 0
        output['counter'] += 1
        output["answer"] = live_agent_or_default(req_info, bot_info,
                                                 output['counter'])
    else:
        output['counter'] = 0

    output["context"] = context
    print("CHAT RESPOSNE ", json.dumps(output))
    return output


def process_message(bot, question):
    ans = ''
    suggestions = ''
    response = {}

    resp = word2vec.get_answer(bot['botid'], question)
    print("word2vec: ", resp)
    response.update(resp)

    resp = matrix.get_answer(bot['botid'], question)
    print("matrix: ", resp)
    response.update(resp)

    resp = exact_match.get_answer(bot['botid'], question)
    print("Exact Match: ", resp)
    response.update(resp)

    if response:
        ans, suggestions = get_answer_suggestions(bot, response)
    return ans, suggestions


def replace_variable(userinfo, answer):
    answer = Template(json.dumps(answer)).safe_substitute(
        **userinfo)
    return json.loads(answer)


def live_agent_or_default(req_info, bot_info, counter):
    msg = ''

    if bot_info.get('live_agent', True):
        if counter >= settings.LIVE_AGENT_COUNT:
            msg = settings.LIVE_AGENT_MESSAGE
    else:
        if counter >= settings.DEFAULT_COUNT:
            msg = settings.DEFAULT_FEEDBACK_MESSAGE
    return msg
