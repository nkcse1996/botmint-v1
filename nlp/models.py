from django.db import models
from registration.models import Bot


# Create your models here.
class ModelFiles(models.Model):
    bot = models.ForeignKey(Bot, on_delete=models.CASCADE)
    model_file = models.FileField(blank=True, null=True, upload_to='bot_model/')
    df_file = models.FileField(blank=True, null=True, upload_to='bot_df/')
    date = models.DateField(auto_now_add=True, blank=True)
