import json
# from bs4 import BeautifulSoup
from brain.matrix_algorithm import matrix_chat
from django.conf import settings
from string import Template
from learning.models import Questionnaire
from .cards import create_card
from .api_process import process_api
from .utils import math_exp_detect, precheck_greetings, \
    post_spellcheck, precheck_spell, jaccard_percent
from brain.conversation import start_conv
import requests
import wikipedia
from fwk.utils import log_error_msg

# TO DO
"""
1) Chat History
2) FeedBack response
3) Channels
"""


def prcoess_question_text(request, input_question):
    html_resp = prcoess_question(request, input_question)
    if isinstance(html_resp, list):
        html_resp = " ".join(html_resp)
    soup = BeautifulSoup(html_resp, "html.parser")
    resp = soup.get_text()
    if resp.startswith("Did you mean") and resp.endswith("Yes No"):
        word = resp.strip("Did you mean").rstrip("Yes No").strip()
        resp = "Did You Mean : {} ? Yes No".format(word)
    return resp


def prcoess_question(request, input_data):
    """
        input_data is json object
        input_data: {
            "message": "Message from User",
            "type": ["API", "CONV", "INITIAL_GREET", "MESSAGE", ...]
        }
        request contains three import dicts
        bot_details = {}
        userinfo = {}
        user_context = {spellcheck={}, api={}, conv={}}

        output is json reponse
        output: {
            "error": ["list of error Messages"]
            "type": ["INITIAL_GREET", "API", "CONV",
                      "EXACT_MATCH", "ML", "MATRIX", "WIKIPEDIA",
                      "MATH_EXP", "SPELLCHECK", "LIVE_AGENT", "RANK5"]
            "answer": [list of [list of cards],......]
            "suggestions": [list of tuples tuple("Question", "Confidence")]
            "feedback": "feedback card in case of valid answer"
        }


    """
    # In next phase, we have to back up the session in chat_reponse

    output = {"error": "", "type": input_data["type"],
              "feedback": "", "answer": [], "suggestions": []}
    message = input_data["message"]
    if request.session['bot_details']['spell_check']:
        message = precheck_spell(request, message)

    if input_data['type'] == 'INITIAL_GREET':
        answer = handshake_greet(request, message)

    if input_data['type'] == "API":
        answer = process_api(request, message)

    if input_data['type'] == "CONV":
        answer = start_conv(request, message)

    if input_data['type'] == "MESSAGE":

        answer = precheck_greetings(request, message)

        if not answer:
            response = chat_algos(request, message)
            if response:
                answer, output["suggestions"] = get_answer_suggestions(
                    request, response)

    answer = replace_variable(request, answer)

    if not answer:
        answer = handle_fallback_from_rank5(
            request, message)

    if not answer:
        answer = fallback_to_live_agent(request)

    answer = rank5_response(request, answer)
    output["answer"] = answer
    print("CHAT RESPOSNE ", output)
    return output


def chat_algos(request, input_question=None):
    response = {}
    import pdb
    pdb.set_trace()
    try:
        response = exact_match(request, input_question)

        if not response:
            print("WOrd To VEC")
            response = word_2_vec(request, input_question)
        if not response:
            print("MATRIX")
            response = matrix_chat(request, input_question)

    except Exception as e:
        log_error_msg("chat_algos", str(e))
    return response


def get_answer_suggestions(request, response):
    bot_details = request.session['bot_details']
    answer = []
    suggestions = []
    resp_obj = Questionnaire.objects.filter(bot_id=bot_details['botid'],
                                            id__in=response.keys())
    resp = {}
    for r in resp_obj:
        resp[r.id] = {"score": response[str(r.id)], "question": r}
    sorted_resp = sorted(resp.values(), key=lambda x: x.get('score'))
    answer = sorted_resp[0].get('question').cards
    all_suggestions = sorted_resp[1:]
    for s in all_suggestions:
        if s.get("score") > bot_details["suggestion_percentage"]:
            suggestions.append({"score": s.get("score"), "question": s.get('question').questions[0]})
            if len(suggestions) >= bot_details["no_of_suggestions"]:
                break

    return answer, suggestions





def exact_match(request, input_question):
    response_data = {}
    bot_detail = request.session.get('bot_details')
    input_question = input_question.lower().strip()
    ques_list = Questionnaire.objects.filter(
        bot_id=bot_detail['botid'], questions__in=[input_question])

    for ques in ques_list:
        jv = 0
        for q in ques.questions:
            jv = max(jv, jaccard_percent(input_question, q))
        response_data[str(ques.id)] = jv

    return response_data


def replace_variable(request, output):
    output = Template(json.dumps(output)).safe_substitute(
        **request.session['userinfo'])
    return json.loads(output)


def handshake_greet(request, input_data):
    greeting_obj = Questionnaire.objects.filter(
        category='handshake conversation',
        bot_id=request.session['bot_details']['botid'])
    if not greeting_obj:
        greeting_obj = Questionnaire.objects.filter(
            category='greeting',
            bot_id=request.session['bot_details']['botid'])
    if greeting_obj:
        greet_msg = greeting_obj[0].cards
    else:
        greet_msg = create_card('Hello $displayname, How may I help you ?')
    return greet_msg


def rank5_response(request, output):
    if not output:
        try:
            response = Questionnaire.objects.filter(
                category='error_msg',
                bot_id=request.session['bot_details']['botid'])[0]
            output = response.cards
        except Exception as e:
            # Have to handle Index error
            output = create_card(settings.RANK5_DEFAULT_MESSAGE)
        request.session['count'] = request.session.get('count', 0) + 1
    else:
        request.session['count'] = 0

    return output


def handle_fallback_from_rank5(request, input_question):
    bot_details = request.session['bot_details']
    response_data = []
    try:
        if bot_details['math_exp']:
            response_data = create_card(
                math_exp_detect(request, input_question))
        if not response_data and bot_details['spell_check']:
            response_data = create_card(
                post_spellcheck(request, input_question), 'spell')
        if not response_data and bot_details['wikipedia']:
            response_data = create_card(wikipedia.summary(input_question))
    except Exception as e:
        log_error_msg('handle_fallback_from_rank5', str(e))
    return response_data


def fallback_to_live_agent(request):
    output = []
    if request.session['bot_details']['live_agent']:
        if request.session['count'] >= settings.LIVE_AGENT_COUNT:
            output = create_card(
                settings.LIVE_AGENT_MESSAGE)
            request.session['count'] = 0
    else:
        if request.session['count'] >= settings.RANK5_COUNT:
            output = create_card(
                settings.RANK5_FEEDBACK_MESSAGE)
            request.session['count'] = 0
    return output


def get_feedback(request, chat_id=None, base_url=None):

    botid = request.session['bot_details']['botid']

    # we have create feedback card
    response = {
        "feedbackResponse": {
            "url": base_url + "/chat/" + botid + "/feedback_message/",
            "data": {'botid': botid,
                     'chat_id': chat_id,
                     'feedback_msg': "Please send the feedback rating value",
                     },
        }
    }

    response["feedback_type"] = request.session['bot_details']['feedback_type']
    return response
