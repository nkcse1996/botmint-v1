from nltk import word_tokenize, pos_tag
from nltk.stem.snowball import SnowballStemmer
from config.utils import log_error_msg
stemmer = SnowballStemmer('english')
stop_word = """a about above after again against all almost alone already
                also although always among an and another any anywhere are
                around as at away b back because before behind best better
                between big both but by cannot certainly clearly could
                differently down down during each either through thus
                even evenly ever every everywhere far for from fully generally
                he her mr mrs itself really the their wants we were
                here herself him himself his however i if in into is it its
                just largely later like likely may me might more most mostly
                 much must my myself never no non noone not now nowhere of off
                 often once only or our out over per perhaps quite rather
                 shall she should since so some somewhere still than that
                 them then there therefore these they this those though
                 to too toward under until up upon us very want wanting
                 whether while will with within without would yet you your
                 """


def pos(sentence):
    stop_words_ls = word_tokenize(stop_word)
    _pos = {
        'noun': [],
        'verb': [],
        'adject': [],
        'pronoun': []
    }
    try:
        # Removing special character
        chars_to_remove = ['!', '@', '#', '$', '%', '^', '&', '*', '(', ')',
                           '[', ']', '{', '}', ';', ':', '.', '/', '<', '>',
                           '?', '_', '+', '`', '\\', ","]

        text = word_tokenize(sentence.lower())
        text = [x for x in text if x not in chars_to_remove]
        # print('pos_atg >>>>',pos_tag(text))
        final_text = list(set(text) - set(stop_words_ls))
        print('pos_atg >>>>', pos_tag(final_text))
        _pos['noun'] = [token.lower() for token, pos in pos_tag(
                        final_text) if pos.startswith('N')]
        _pos['verb'] = [stemmer.stem(token.lower()) for token, pos in pos_tag(
                        final_text) if pos in ["VB", "VBD", "VBG", "VBN"]]
        _pos['adject'] = [token.lower() for token, pos in pos_tag(
            final_text) if pos.startswith('J')]
        _pos['pronoun'] = [token.lower() for token, pos in pos_tag(
            final_text) if pos.startswith('W')]
    except Exception as e:
        log_error_msg("pos_english", str(e))

    return _pos
