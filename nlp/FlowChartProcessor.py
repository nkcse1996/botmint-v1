import datetime
import re


class FlowChartProcessor:
    # Returns empty string if validation is success or return error message

    def __init__(self, param_value, data_type):
        self.param_value = param_value
        self.data_type = data_type

    def validate(self):
        self.extractor()
        returnMessage = self.param_value

        dataType = self.data_type.validation
        if dataType == 'notnull':
            if self.param_value == '' or len(self.param_value) == 0:
                returnMessage = False

        elif dataType == 'email':
            if self.param_value == '' or len(self.param_value) == 0:
                returnMessage = False
        elif dataType == 'float':
            try:
                float(self.param_value)
            except ValueError:
                returnMessage = False
        elif dataType == 'string':
            if type(self.param_value) != str or len(self.param_value) == 0:
                returnMessage = False
        elif dataType == 'integer':
            try:
                self.param_value = int(self.param_value)
            except:
                returnMessage = False
        elif dataType == 'int':
            try:
                int(self.param_value)
            except ValueError:
                returnMessage = False
        elif dataType == 'bool':
            try:
                bool(self.param_value)
            except ValueError:
                returnMessage = False
        elif dataType == 'date':
            count = 0
            try:
                datetime.datetime.strptime(self.param_value, '%d-%m-%Y')
            except ValueError:
                count += 1
            try:
                datetime.datetime.strptime(self.param_value, '%d/%m/%Y')
            except ValueError:
                count += 1
            try:
                datetime.datetime.strptime(self.param_value, '%d.%m.%Y')
            except ValueError:
                count += 1
            try:
                datetime.datetime.strptime(self.param_value, '%d %m %Y')
            except ValueError:
                count += 1
            try:
                datetime.datetime.strptime(self.param_value, '%Y-%m-%d')
            except ValueError:
                count += 1

            if count == 5:
                returnMessage = False

        return returnMessage

    def extractor(self, param_value, data_type):
        try:
            regex = data_type.regex
            regex.replace("\\", "\\\\")
            output = re.findall(regex, self.param_value)
            if output:
                self.param_value = output[0]

        except Exception as e:
            print(str(e))
