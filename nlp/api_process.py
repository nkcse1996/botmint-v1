import collections
import json
import requests
from json2html import json2html
from brain.FlowChartProcessor import FlowChartProcessor
from learning.models import tbl_backend_master, tbl_input_params, \
    tbl_output_params
from django.conf import settings
from urllib.parse import unquote
import copy
from fwk.utils import log_error_msg


class DictQuery(dict):

    def get(self, path, default=None):
        keys = path.split(".")
        val = None
        for key in keys:
            if val:
                if isinstance(val, list):
                    val = [v.get(key, default) if v else None for v in val]
                elif isinstance(val, dict):
                    val = val.get(key, default)
            else:
                val = dict.get(self, key, default)

            if not val:
                break
        return val


def filter_api_output(data, params):
    if isinstance(data, str):
        data = json.loads(data)

    # Temp Hack to allow only formatter
    if "nofilter" in params:
        return data

    if isinstance(data, list):
        output = []
        for d in data:
            output.append(filter_api_output_dict(d, params))
        return output
    if isinstance(data, dict):
        return filter_api_output_dict(data, params)


def filter_api_output_dict(data, params):
    output = {}
    if not isinstance(data, dict):
        data = json.loads(data)
    for param in params:
        key = "__".join(param.split('.'))
        output[key] = DictQuery(data).get(param, param + " :NA")
    return output


def output_formatter(data, formatter):
    formatter = unquote(formatter).strip()
    if formatter:
        _data = {'data': json.loads(data)}
        _namespace = copy.deepcopy(_data)
        try:
            if formatter:
                exec(formatter, _namespace)
                if _namespace.get('output'):
                    data = _namespace.get('output')
                else:
                    data = 'formatter should assign value to putput variable'
        except Exception as e:
            return str(e)


def post_call(api_url, params={}):
    message = {"error": "", "response": ""}
    try:
        params = dict(params)
        result1 = requests.post(api_url, params)
        result1.raise_for_status()
        message["answer"] = result1.text
    except requests.exceptions.HTTPError as err1:
        message["error"] = str(err1)
        message["answer"] = settings.API_ERROR_MESSAGE
    except requests.exceptions.RequestException as err2:
        message["error"] = str(err1)
        message["answer"] = settings.API_ERROR_MESSAGE
    except Exception as err3:
        message["error"] = str(err1)
        message["answer"] = settings.API_ERROR_MESSAGE
    return message


def get_call(api_url,  params={}):
    message = {"error": "", "response": ""}
    try:
        result1 = requests.get(api_url)
        message["answer"] = result1.text
    except requests.exceptions.HTTPError as err1:
        message["error"] = str(err1)
        message["answer"] = settings.API_ERROR_MESSAGE

    except requests.exceptions.RequestException as err2:
        message["error"] = str(err1)
        message["answer"] = settings.API_ERROR_MESSAGE
    except Exception as err3:
        message["error"] = str(err1)
        message["answer"] = settings.API_ERROR_MESSAGE
    return message


def execute_url(api_details, params):
    # Call URL
    if api_details['request_flow'] == 'Client_server':
        for key, value in params.items():
            api_details['url'] = api_details['url'].replace(key, value)
        return json.dumps(api_details)
    else:
        request_type = api_details['request_type']
        api_url = api_details['url']
        message = {}

        if request_type.upper() == "GET":
            message = get_call(api_url, params)
        elif request_type.upper() == "POST":
            message = post_call(api_url, params)

        if message.get("error"):
            if settings.DEBUG:
                message["answer"] = message["error"]
            return create_card(message["answer"])

        if isinstance(message, str):
            message["answer"] = json_conversion(
                message["answer"], api_details["message_content"])
        if api_details['out_params']:
            message["answer"] = json.dumps(filter_api_output(
                message["answer"], api_details['out_params'].keys()))
        if api_details.get('out_formatter'):
            message["answer"] = output_formatter(
                message["answer"], api_details['out_formatter'])

        return message["answer"]


def get_api_details(api_id):
    service = tbl_backend_master.objects.get(id=api_id)

    d_service = {"id": service.id, "api_name": service.api_name,
                 "url": service.url,
                 "is_conversation": service.is_conversation,
                 "request_type": service.request_type,
                 "request_flow": service.request_flow,
                 "message_content": service.message_content.message_content}

    inp_params = tbl_input_params.objects.filter(
        backend_api=service.id).order_by('id')

    d_service["inp_params"] = collections.OrderedDict()
    for param in inp_params:
        d_service["inp_params"][param.param_name] = {
            "data_type": param.data_type,
            "data_from": param.data_from,
            "error_message": param.message_prompt,
            "inp_formatter": param.formatter
        }

    out_params = tbl_output_params.objects.filter(
        backend_api=service.id).order_by('id')

    d_service["out_params"] = collections.OrderedDict()
    for param in out_params:
        if param.formatter:
            # Just last formatter will change the UI/Model
            d_service["out_formatter"] = param.formatter

    return d_service


def find_input_parameter(request, api_details):
    inp_params = collections.OrderedDict()
    request.session["user_context"]['api']['params'] = request.session[
        "user_context"]['api'].get('params', {})
    if api_details:
        inp_params = collections.OrderedDict()

        for item in api_details['inp_params']:

            if item["data_from"].lower() == "user":
                inp_params[item['param_name']] = ""

            elif item["data_from"].lower() == "session":
                # see if parameter is directly stored in session variable
                inp_params[item['param_name']] = request.session["user_context"].get(
                    'userinfo', {}).get(item['param_name'], '')
                request.session["user_context"]['api']['params'][
                    item['param_name']] = inp_params[item['param_name']]
            else:
                inp_params[item['param_name']] = request.session["user_context"].get(
                    'api', {}).get('params', {}).get(item['param_name'], '')

    return inp_params


def validate_input_params(request, input_params, api_details):
    message = []

    for param, param_info in api_details['inp_params'].items():
        fcp = FlowChartProcessor(input_params[param], param_info['data_type'])
        param_value = fcp.validate()
        if param_value:
            request.session["user_context"]['api'][
                'params'][param] = param_value

            # introduce formatter specific variables that will be used in API
            inp_formatter = unquote(param_info['inp_formatter']).strip()
            if inp_formatter:
                _namespace = {param: param_value}
                try:
                    exec(inp_formatter, _namespace)
                    for key in _namespace:
                        if key not in [param, '__builtins__']:
                            request.session["user_context"]['api']['params'][
                                key] = _namespace[key]

                except Exception as e:
                    log_error_msg('validate_input_params', str(e))

        else:
            msg = {"param_name": param,
                   "error_message": param_info['error_message'],
                   "counter": 0}
            session_msg = request.session[
                "user_context"]['api'].get('message', msg)

            if session_msg.get('param_name') == msg['param_name']:
                session_msg['counter'] = session_msg['counter'] + 1
                request.session["user_context"]['api']['message'] = session_msg
            else:
                request.session["user_context"]['api']['message'] = msg

            if session_msg['param_name'] in request.session["user_context"]['api']['params']:
                del request.session["user_context"]['api'][
                    'params'][session_msg['param_name']]

            message = create_card(request.session["user_context"]['api']['message']["error_message"])
    return message


def json_conversion(api_response, response_type):
    if response_type == "JSON":
        try:
            response = json.loads(api_response)
        except Exception as e:
            log_error_msg("json_conversion", str(e))
            response = api_response
    if response_type == "XML":
        try:
            response = json.loads(json.dumps(xmltodict.parse(api_response)))
        except Exception as e:
            log_error_msg("json_conversion", str(e))
            response = api_response
    return response


def val_input_para_helper(request, input_params, api_details):
    message = validate_input_params(request, input_params, api_details)

    if not message:
        message = execute_url(api_details, request.session[
                              "user_context"]['api']['params'])
        del request.session["user_context"]['api']

    return message


def process_api(request, input_data=None):
    return_message = []
    api_id = request.session["user_context"].get('api', {}).get('id')
    if not api_id:
        api_id = input_data
    api_details = get_api_details(api_id)

    if not request.session["user_context"].get('api'):
        request.session["user_context"]['api'] = {'id': api_id, 'params': {}}
        input_params = find_input_parameter(request, api_details)
    else:
        message = request.session["user_context"]['api']['message']
        if message['counter'] <= 1:
            param = message['param_name']
            input_params = request.session["user_context"]['api']['params']
            if isinstance(input_data, str):
                input_data = input_data.strip()
            input_params[param] = input_data
        else:
            del request.session["user_context"]['api']
            return create_card("Please ask something else, Context reset")

    # validate input parameters
    return_message = val_input_para_helper(request, input_params, api_details)
    return return_message
